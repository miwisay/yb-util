package com.ybear.ybutils.utils.notification;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({
        DefaultChannelName.PUSH, DefaultChannelName.MESSAGE, DefaultChannelName.NOTIFICATION,
        DefaultChannelName.DOWNLOAD, DefaultChannelName.OTHER
})
@Retention(RetentionPolicy.SOURCE)
public @interface DefaultChannelName {
    String PUSH = "Push";
    String MESSAGE = "Message";
    String NOTIFICATION = "Notification";
    String DOWNLOAD = "Download";
    String OTHER = "Other";
}