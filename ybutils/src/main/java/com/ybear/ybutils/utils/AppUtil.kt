package com.ybear.ybutils.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import com.ybear.ybutils.utils.SysUtil.getPackageInfo

/**
 * App常用工具类
 * @see .getAppName
 * @see .getVerName
 * @see .getVerCode
 * @see .getSdkVer
 */
object AppUtil {
    /**
     * 获取app名称
     * @param context       上下文
     * @return              返回
     */
    @JvmStatic
    fun getAppName(context: Context): String {
        try {
            return ResUtil.getString( context, "app_name" ) ?: ""
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     * 获取版本名称
     * @param context   上下文
     * @return          返回版本名称
     */
    @JvmStatic
    fun getVerName(context: Context): String? {
        try {
            return getPackageInfo( context )?.versionName
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * 获取版本代码
     * @param mContext  上下文
     * @return          返回版本代码
     */
    @JvmStatic
    fun getVerCode(mContext: Context): Long {
        try {
            val pi = getPackageInfo(mContext) ?: return 0L
            return if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.P ) {
                pi.longVersionCode
            } else {
                pi.versionCode.toLong()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return 0L
    }

    /**
     * 获取当前SDK版本
     * @return  ver
     */
    @JvmStatic
    val sdkVer: Int get() = Build.VERSION.SDK_INT

    /**
     * 获取包名
     * @param context   上下文
     * @return          包名
     */
    @JvmStatic
    fun getPackageName(context: Context): String? {
        return getPackageInfo( context )?.packageName
    }

    /**
     * 获取AndroidManifest中指定的meta-data
     * @param context   上下文
     * @param key       meta-data的key
     * @return          对应的val
     */
    @JvmStatic
    fun getMetaDataForString(context: Context, key: String): String? {
        return getMetaDataBundle( context )?.getString( key )
    }

    @JvmStatic
    fun getMetaDataForInt(context: Context, key: String): Int {
        return getMetaDataBundle( context )?.getInt( key ) ?: 0
    }

    @JvmStatic
    fun getMetaDataBundle(context: Context): Bundle? {
        try {
            return context.packageManager?.getApplicationInfo(
                    context.packageName, PackageManager.GET_META_DATA
            )?.metaData
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return null
    }
}