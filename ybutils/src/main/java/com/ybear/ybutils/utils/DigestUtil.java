package com.ybear.ybutils.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 计算校验值工具类
 */
public class DigestUtil {
    /**
     算法类型
     */
    @Retention( RetentionPolicy.SOURCE )
    public @interface AlgorithmType {
        String MD5 = "MD5";
        String SHA1 = "SHA-1";
        String SHA256 = "SHA-256";
        String SHA384 = "SHA-384";
        String SHA512 = "SHA-512";
    }

    /**
     * 计算字节数组的校验值
     * @param data          数据
     * @param algorithm     算法类型
     * @return              返回结果
     */
    @Nullable
    public static String toDigest(@NonNull byte[] data, @NonNull @AlgorithmType String algorithm) {
        try {
            MessageDigest digest = MessageDigest.getInstance( algorithm );
            digest.update( data );
            //转为16进制
            return toHexString( digest.digest() );
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     计算字符串的校验值
     @param data            数据
     @param charset         编码类型
     @param algorithm       算法类型
     @return                返回结果
     */
    public static String toDigest(String data, Charset charset, @NonNull @AlgorithmType String algorithm) {
        return toDigest( data.getBytes( charset ), algorithm );
    }

    /**
     * 计算文件的校验值
     * @param filePath      文件路径
     * @param algorithm     算法类型
     * @return              返回结果
     */
    @Nullable
    public static String toFileDigest(@NonNull String filePath, @NonNull @AlgorithmType String algorithm) {
        InputStream is = null;
        byte[] bsData;
        try {
            is = new FileInputStream( filePath );
            bsData = new byte[ is.available() ];
            if( is.read( bsData ) == bsData.length ) return toDigest( bsData, algorithm );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if ( is != null ) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     计算文件的校验值
     @param file        文件
     @param algorithm   算法类型
     @return            返回结果
     */
    public static String toFileDigest(@NonNull File file, @NonNull @AlgorithmType String algorithm) {
        return toFileDigest( file.getAbsolutePath(), algorithm );
    }

    public static String toMD5(byte[] data) {
        return toDigest( data, AlgorithmType.MD5 );
    }
    public static String toSHA1(byte[] data) {
        return toDigest( data, AlgorithmType.SHA1 );
    }
    public static String toSHA256(byte[] data) {
        return toDigest( data, AlgorithmType.SHA256 );
    }
    public static String toSHA384(byte[] data) {
        return toDigest( data, AlgorithmType.SHA384 );
    }
    public static String toSHA512(byte[] data) {
        return toDigest( data, AlgorithmType.SHA512 );
    }

    public static String toMD5(String data, Charset charset) {
        return toDigest( data, charset, AlgorithmType.MD5 );
    }
    public static String toSHA1(String data, Charset charset) {
        return toDigest( data, charset, AlgorithmType.SHA1 );
    }
    public static String toSHA256(String data, Charset charset) {
        return toDigest( data, charset, AlgorithmType.SHA256 );
    }
    public static String toSHA384(String data, Charset charset) {
        return toDigest( data, charset, AlgorithmType.SHA384 );
    }
    public static String toSHA512(String data, Charset charset) {
        return toDigest( data, charset, AlgorithmType.SHA512 );
    }

    public static String toMD5(String data) {
        return toDigest( data, utf8(), AlgorithmType.MD5 );
    }
    public static String toSHA1(String data) {
        return toDigest( data, utf8(), AlgorithmType.SHA1 );
    }
    public static String toSHA256(String data) {
        return toDigest( data, utf8(), AlgorithmType.SHA256 );
    }
    public static String toSHA384(String data) {
        return toDigest( data, utf8(), AlgorithmType.SHA384);
    }
    public static String toSHA512(String data) {
        return toDigest( data, utf8(), AlgorithmType.SHA512 );
    }

    public static String toFileMD5(String filePath) {
        return toFileDigest( filePath, AlgorithmType.MD5 );
    }
    public static String toFileSHA1(String filePath) {
        return toFileDigest( filePath, AlgorithmType.SHA1 ); }
    public static String toFileSHA256(String filePath) {
        return toFileDigest( filePath, AlgorithmType.SHA256 );
    }
    public static String toFileSHA384(String filePath) {
        return toFileDigest( filePath, AlgorithmType.SHA384 );
    }
    public static String toFileSHA512(String filePath) {
        return toFileDigest( filePath, AlgorithmType.SHA512 );
    }

    public static String toFileMD5(File file) {
        return toFileDigest( file, AlgorithmType.MD5 );
    }
    public static String toFileSHA1(File file) {
        return toFileDigest( file, AlgorithmType.SHA1 );
    }
    public static String toFileSHA256(File file) {
        return toFileDigest( file, AlgorithmType.SHA256 );
    }
    public static String toFileSHA384(File file) {
        return toFileDigest( file, AlgorithmType.SHA384 );
    }
    public static String toFileSHA512(File file) {
        return toFileDigest( file, AlgorithmType.SHA512 );
    }

    /**
     转为16进制
     @param bs  字节数组
     @return    16进制字符串
     */
    private static String toHexString(byte[] bs) {
        StringBuilder sb = new StringBuilder();
        String s;
        for( byte b : bs ) {
            s = Integer.toHexString( b & 0xFF );
            //1得到一位的进行补0操作
            if (s.length() == 1) sb.append("0");
            sb.append(s);
        }
        return sb.toString();
    }

    private static Charset utf8() { return Charset.defaultCharset(); }
}
