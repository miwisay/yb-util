package com.ybear.ybutils.utils.time;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 时间类型
 */
@IntDef({
        TimeType.TYPE_SECOND,
        TimeType.TYPE_MINUTE,
        TimeType.TYPE_HOUR,
        TimeType.TYPE_DAY,
        TimeType.TYPE_MONTH,
        TimeType.TYPE_YEAR
})
@Retention(RetentionPolicy.SOURCE)
public @interface TimeType {
    int TYPE_SECOND = 1;     //秒
    int TYPE_MINUTE = 2;     //分
    int TYPE_HOUR = 3;       //时
    int TYPE_DAY = 4;        //日
    int TYPE_MONTH = 5;      //月
    int TYPE_YEAR = 6;       //年
}
