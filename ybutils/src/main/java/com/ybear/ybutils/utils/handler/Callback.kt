package com.ybear.ybutils.utils.handler

import android.os.Handler
import android.os.Message

/**
 * 我发现他并不实用
 * @see CallbackAdapter 推荐使用它
 */
interface Callback : Handler.Callback {
    override fun handleMessage(msg: Message): Boolean
    fun dispatchMessage(msg: Message)
}