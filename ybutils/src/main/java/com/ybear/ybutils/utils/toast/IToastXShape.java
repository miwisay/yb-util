package com.ybear.ybutils.utils.toast;

import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import java.util.Locale;

interface IToastXShape {
    void setRadius(float r);
    void setRadius(float lt, float rt, float lb, float rb);

    void setRadii(float[] radii);
    float[] getRadii();
    void setBorderSize(int size);
    void setBorderColor(@ColorInt int color);

    void setShadowRadius(int radius);
    void setShadowColor(@ColorInt int color);
    void setShadowOffsetX(int offsetX);
    void setShadowOffsetY(int offsetY);

    void setEnableRtlLayout(Locale locale);
    void setDisableRtlLayout();
    void setEnableLayerTypeHardware(@NonNull View view, boolean enable);
}
