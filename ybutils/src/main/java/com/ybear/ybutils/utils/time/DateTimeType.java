package com.ybear.ybutils.utils.time;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface DateTimeType {
    String YEAR = "yyyy";           //年
    String MONTH = "MM";            //月
    String DAY = "dd";              //日
    String HOUR_SHORT = "hh";       //时（12小时制）
    String HOUR = "HH";             //时（24小时制）
    String MINUTE = "mm";           //分
    String SECOND = "ss";           //秒
    String MILLIS = "SS";           //毫秒
    String WEEK_OF_YEAR = "w";       //一年中的第几个星期
    String WEEK_OF_MONTH = "W";      //一月中的第几星期
    String WEEK_MONTH_7 = "F";      //一月中的第几个星期(会把这个月总共过的天数除以7)
    String WEEK_LONG = "E";         //星期
    String TIME_ZONE = "z";         //时区, eg:CST
    String TIME_ZONE_NUM = "Z";     //时区, eg:+0800
    String A = "a";                 //上下午标识
}