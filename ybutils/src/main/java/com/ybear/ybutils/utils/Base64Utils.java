package com.ybear.ybutils.utils;

import android.annotation.SuppressLint;
import android.util.Base64;

import androidx.annotation.NonNull;

import java.nio.charset.Charset;

public class Base64Utils {
    //Base64解码模式
    private static int mDefaultFlags = Base64.DEFAULT;
    //字符解码模式
    private static Charset mDefaultCharset = Charset.defaultCharset();
    //非安卓环境下建议启用，否则会报错（调试专用）
    private static boolean isJavaMode = false;

    /**
     * Base64解码模式
     */
    public static void setJavaMode(boolean mode) {
        isJavaMode = mode;
    }

    /**
     * 字符解码模式
     */
    public static void setBase64Flags(int flags) {
        mDefaultFlags = flags;
    }

    /**
     * 非安卓环境下建议启用，否则会报错（调试专用）
     */
    public static void setCharset(Charset charset) {
        mDefaultCharset = charset;
    }

    /**
     base64编码（传入字节数组）
     @param data    数据
     @return        结果
     */
    @SuppressLint("NewApi")
    @NonNull
    public static String encodeBase64(byte[] data, Charset charset, int flag) {
        String en;
        if( isJavaMode ) {
            //Java环境
            en = java.util.Base64.getEncoder().encodeToString( data );
            if( en == null ) return "";
        }else {
            //Android环境
            en = new String( Base64.encode( data, flag ), charset );
        }
        return en.replaceAll( String.valueOf( (char) 10 ), "" )
                .trim();
    }
    @NonNull
    public static String encodeBase64(byte[] data, Charset charset) {
        return encodeBase64( data, charset, mDefaultFlags );
    }
    @NonNull
    public static String encodeBase64(byte[] data, int flag) {
        return encodeBase64( data, mDefaultCharset, flag );
    }
    @NonNull
    public static String encodeBase64(byte[] data) {
        return encodeBase64( data, mDefaultCharset, mDefaultFlags );
    }

    /**
     base64编码（传入字符串）
     @param data    数据
     @return        结果
     */
    @NonNull
    public static String encodeBase64(String data, Charset charset, int flag) {
        return data == null ? "" : encodeBase64( data.getBytes( charset ), charset, flag );
    }
    @NonNull
    public static String encodeBase64(String data, Charset charset) {
        return encodeBase64( data, charset, mDefaultFlags );
    }
    @NonNull
    public static String encodeBase64(String data, int flag) {
        return encodeBase64( data, mDefaultCharset, flag );
    }
    @NonNull
    public static String encodeBase64(String data) {
        return encodeBase64( data, mDefaultCharset, mDefaultFlags );
    }

    /**
     base64解码（返回字节数组）
     @param data    数据
     @return        结果
     */
    @SuppressLint("NewApi")
    @NonNull
    public static byte[] decodeBase64(String data, Charset charset, int flag) {
        //Java环境
        if( isJavaMode ) {
            return java.util.Base64.getDecoder().decode( data.getBytes( charset ) );
        }
        //Android环境
        return Base64.decode( data.getBytes( charset ), flag );
    }
    @NonNull
    public static byte[] decodeBase64(String data, Charset charset) {
        return decodeBase64( data, charset, -1 );
    }
    @NonNull
    public static byte[] decodeBase64(String data, int flag) {
        return decodeBase64( data, mDefaultCharset, flag );
    }
    @NonNull
    public static byte[] decodeBase64(String data) {
        return decodeBase64( data, mDefaultCharset, mDefaultFlags );
    }

    /**
     base64解码（返回字符串）
     @param data    数据
     @return        结果
     */
    @NonNull
    public static String decodeBase64String(String data, Charset charset, int flag) {
        return new String( decodeBase64( data, charset, flag ), mDefaultCharset );
    }
    @NonNull
    public static String decodeBase64String(String data, Charset charset) {
        return decodeBase64String( data, charset, -1 );
    }
    @NonNull
    public static String decodeBase64String(String data, int flag) {
        return decodeBase64String( data, mDefaultCharset, flag );
    }
    @NonNull
    public static String decodeBase64String(String data) {
        return decodeBase64String( data, mDefaultCharset, mDefaultFlags );
    }
}
