package com.ybear.ybutils.utils.notification.channel;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.ybear.ybutils.utils.notification.DefaultChannelName;
import com.ybear.ybutils.utils.notification.Importance;

@RequiresApi(api = Build.VERSION_CODES.O)
public class Channel {
    private final NotificationChannel channel;

    @SuppressLint("WrongConstant")
    public Channel(@NonNull String id, CharSequence name, @Importance int importance, boolean isShowBadge) {
        if( TextUtils.isEmpty( name ) ) name = DefaultChannelName.NOTIFICATION;
        if( importance == Importance.UNSPECIFIED ) importance = Importance.DEFAULT;
        channel = new NotificationChannel( id, name, importance );
        channel.setShowBadge( isShowBadge );
    }

    public Channel(@NonNull String id, CharSequence name, boolean isShowBadge) {
        this( id, name, Importance.DEFAULT, isShowBadge );
    }

    public Channel(@NonNull String id, boolean isShowBadge) {
        this( id, DefaultChannelName.NOTIFICATION, isShowBadge );
    }

    public Channel(@NonNull String id) {
        this( id, DefaultChannelName.NOTIFICATION, false );
    }

    public String getChannelId() { return channel.getId(); }
    public NotificationChannel getNotificationChannel() { return channel; }
}
