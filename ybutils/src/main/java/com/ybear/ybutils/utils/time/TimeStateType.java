package com.ybear.ybutils.utils.time;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 时间类型
 */
@IntDef({
        TimeStateType.TYPE_NONE,
        TimeStateType.TYPE_RECENTLY,
        TimeStateType.TYPE_YESTERDAY,
        TimeStateType.TYPE_TOMORROW,
        TimeStateType.TYPE_LAST_MONTH,
        TimeStateType.TYPE_NEXT_MONTH,
        TimeStateType.TYPE_YESTERYEAR,
        TimeStateType.TYPE_NEXT_YEAR
})
@Retention(RetentionPolicy.SOURCE)
public @interface TimeStateType {
    int TYPE_NONE = 0;              //未知
    int TYPE_RECENTLY = 1;          //刚刚
    int TYPE_YESTERDAY = 2;         //昨天
    int TYPE_TOMORROW = 3;          //明天
    int TYPE_LAST_MONTH = 4;        //上个月
    int TYPE_NEXT_MONTH = 5;        //下个月
    int TYPE_YESTERYEAR = 6;        //去年
    int TYPE_NEXT_YEAR = 7;         //明年
}
