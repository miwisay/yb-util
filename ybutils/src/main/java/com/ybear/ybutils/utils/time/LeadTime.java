package com.ybear.ybutils.utils.time;

import androidx.annotation.NonNull;

/**
 * 时间差值实体类
 */
public class LeadTime {
    private long diffTime;              //时间差值。秒
    @TimeType
    private int timeType;               //差值类型
    @TimeStateType
    private int timeState;              //时间状态

    @NonNull
    @Override
    public String toString() {
        String strTimeType = null;
        String strTimeState = null;
        switch( timeType ) {
            case TimeType.TYPE_SECOND:
                strTimeType = "SECOND";
                break;
            case TimeType.TYPE_MINUTE:
                strTimeType = "MINUTE";
                break;
            case TimeType.TYPE_HOUR:
                strTimeType = "HOUR";
                break;
            case TimeType.TYPE_DAY:
                strTimeType = "DAY";
                break;
            case TimeType.TYPE_MONTH:
                strTimeType = "MONTH";
                break;
            case TimeType.TYPE_YEAR:
                strTimeType = "YEAR";
                break;
        }
        switch( timeState ) {
            case TimeStateType.TYPE_NONE:
                strTimeState = "NONE";
                break;
            case TimeStateType.TYPE_RECENTLY:
                strTimeState = "RECENTLY";
                break;
            case TimeStateType.TYPE_YESTERDAY:
                strTimeState = "YESTERDAY";
                break;
            case TimeStateType.TYPE_TOMORROW:
                strTimeState = "TOMORROW";
                break;
            case TimeStateType.TYPE_LAST_MONTH:
                strTimeState = "LAST_MONTH";
                break;
            case TimeStateType.TYPE_NEXT_MONTH:
                strTimeState = "NEXT_MONTH";
                break;
            case TimeStateType.TYPE_YESTERYEAR:
                strTimeState = "YESTERYEAR";
                break;
            case TimeStateType.TYPE_NEXT_YEAR:
                strTimeState = "NEXT_YEAR";
                break;
        }
        return String.format(
                "LeadTime{diffTime=%ss, diffOfMillisecond=%sms, timeType=%s, timeState=%s",
                getDiffTime(), getDiffOfMillisecond(), strTimeType, strTimeState
        );
    }

    public LeadTime setLeadTime(long millisecond, @TimeType int timeType) {
        this.diffTime = millisecond;
        this.timeType = timeType;
        return this;
    }
    public LeadTime setLeadTime(LeadTime leadTime) {
        if( leadTime == null ) return this;
        return setLeadTime( leadTime.diffTime, leadTime.timeType );
    }

    public long getDiffTime() { return diffTime / 1000L; }
    public long getDiffOfMillisecond() { return diffTime; }

    @TimeStateType
    public int getTimeState() { return timeState; }
    public LeadTime setTimeState(@TimeStateType int timeState) {
        this.timeState = timeState;
        return this;
    }

    @TimeType
    public int getTimeType() { return timeType; }
}
