package com.ybear.ybutils.utils.time;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.ybear.ybutils.utils.R;

public class MessageTimeStyle {
    private String recently;        //刚刚
    private String minutesAgo;      //N分钟前
    private String hoursAgo;        //N小时前
    private String yesterday;       //昨天
    private String dayAgo;          //yyyy/MM/dd or MM/dd

    public MessageTimeStyle() { }
    public MessageTimeStyle(String recently, String minutesAgo,
                            String hoursAgo, String yesterday, String dayAgo) {
        this.recently = recently;
        this.minutesAgo = minutesAgo;
        this.hoursAgo = hoursAgo;
        this.yesterday = yesterday;
        this.dayAgo = dayAgo;
    }
    public MessageTimeStyle(@NonNull Resources res,
                            int recentlyResId, int minutesAgoResId,
                            int hoursAgoResId, int yesterdayResId, int dayAgoResId) {
        this( res.getString( recentlyResId ),
                res.getString( minutesAgoResId ), res.getString( hoursAgoResId ),
                res.getString( yesterdayResId ), res.getString( dayAgoResId )
        );
    }

    public static MessageTimeStyle defaultStyle(@NonNull Resources res) {
        return new MessageTimeStyle(
                res.getString( R.string.stringMsgStyleOfRecently ),
                res.getString( R.string.stringMsgStyleOfMinutesAgo ),
                res.getString( R.string.stringMsgStyleOfHoursAgo ),
                res.getString( R.string.stringMsgStyleOfYesterday ),
                res.getString( R.string.stringMsgStyleOfDayAgo )
        );
    }

    public String getRecently() { return recently; }
    public MessageTimeStyle setRecently(String recently) {
        this.recently = recently;
        return this;
    }
    public MessageTimeStyle setRecently(@NonNull Resources res, int recentlyResId) {
        return setRecently( res.getString( recentlyResId ) );
    }

    public String getMinutesAgo() { return minutesAgo; }
    public MessageTimeStyle setMinutesAgo(String minutesAgo) {
        this.minutesAgo = minutesAgo;
        return this;
    }
    public MessageTimeStyle setMinutesAgo(@NonNull Resources res, int minutesAgoResId) {
        return setMinutesAgo( res.getString( minutesAgoResId ) );
    }

    public String getHoursAgo() { return hoursAgo; }
    public MessageTimeStyle setHoursAgo(String hoursAgo) {
        this.hoursAgo = hoursAgo;
        return this;
    }
    public MessageTimeStyle setHoursAgo(@NonNull Resources res, int hoursAgoResId) {
        return setHoursAgo( res.getString( hoursAgoResId ) );
    }

    public String getYesterday() { return yesterday; }
    public MessageTimeStyle setYesterday(String yesterday) {
        this.yesterday = yesterday;
        return this;
    }
    public MessageTimeStyle setYesterday(@NonNull Resources res, int yesterdayResId) {
        return setYesterday( res.getString( yesterdayResId ) );
    }

    public String getDayAgo() { return dayAgo; }
    public MessageTimeStyle setDayAgo(String dayAgo) {
        this.dayAgo = dayAgo;
        return this;
    }
    public MessageTimeStyle setDayAgo(@NonNull Resources res, int dayAgoResId) {
        return setDayAgo( res.getString( dayAgoResId ) );
    }
}