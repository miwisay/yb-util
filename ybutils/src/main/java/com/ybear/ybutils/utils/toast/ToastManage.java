package com.ybear.ybutils.utils.toast;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.ybear.ybutils.utils.handler.Handler;
import com.ybear.ybutils.utils.handler.HandlerManage;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ToastManage {
    private static final Handler mHandler = HandlerManage.create();
    private Build mBuild;

    private ToastManage() { mBuild = new Build(); }
    public static ToastManage get() { return ToastManage.HANDLER.I; }
    private static final class HANDLER { private static final ToastManage I = new ToastManage(); }

    public ToastManage initLifecycle(Application app) {
        app.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity,
                                          @Nullable Bundle savedInstanceState) {}
            @Override
            public void onActivityStarted(@NonNull Activity activity) {}
            @Override
            public void onActivityResumed(@NonNull Activity activity) {}
            @Override
            public void onActivityPaused(@NonNull Activity activity) {}
            @Override
            public void onActivityStopped(@NonNull Activity activity) {}
            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity,
                                                    @NonNull Bundle outState) {}
            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {
                if( mShownList.size() == 0 ) return;
                for( ToastX toastX : mShownList ) {
                    try {
                        if( toastX != null ) toastX.cancel();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mShownList.clear();
            }
        });
        return this;
    }

    public ToastManage setBuild(Build build) {
        mBuild = build;
        return this;
    }

    public Build getBuild() { return mBuild; }

    private final List<ToastX> mShownList = new CopyOnWriteArrayList<>();

    private ToastX newToastX(@NonNull Context context, int duration, @Nullable Build build) {
        build = build == null ? mBuild : build;
        ToastX toastX = new ToastX( context, mHandler, build ).setDuration( duration );
        if( build.isCancelOnDestroy() ) mShownList.add( toastX );
        return toastX;
    }

    public ToastX makeText(@NonNull Context context,
                           @Nullable CharSequence s,
                           int duration,
                           @Nullable Build build) {
        return newToastX( context, duration, build ).setText( s );
    }

    public ToastX makeText(@NonNull Context context, @Nullable CharSequence s, int duration) {
        return makeText( context, s, duration, mBuild );
    }

    public ToastX makeText(@NonNull Context context,
                           @StringRes int resId,
                           int duration,
                           @Nullable Build build) {
        return newToastX( context, duration, build ).setText( resId );
    }

    public ToastX makeText(@NonNull Context context, @StringRes int resId, int duration) {
        return makeText( context, resId, duration, mBuild );
    }

    public ToastX makeText(@NonNull Context context, int duration) {
        return makeText( context, null, duration );
    }

    public ToastX makeText(@NonNull Context context) {
        return makeText( context, null, ToastX.LENGTH_SHORT );
    }

    public ToastX makeTextForLong(@NonNull Context context) {
        return makeText( context, null, ToastX.LENGTH_LONG );
    }


    public void showToast(@NonNull Context context,
                          CharSequence s,
                          int duration,
                          @Nullable Build build) {
        makeText( context, s, duration, build ).show();
    }
    public void showToast(@NonNull Context context, CharSequence s, int duration) {
        showToast( context, s, duration, mBuild );
    }

    public void showToast(@NonNull Context context,
                          CharSequence s,
                          @Nullable Build build) {
        showToast( context, s, Toast.LENGTH_SHORT, build );
    }
    public void showToast(@NonNull Context context, CharSequence s) {
        showToast( context, s, Toast.LENGTH_SHORT );
    }

    public void showToast(@NonNull Context context,
                          @StringRes int resId,
                          int duration,
                          @Nullable Build build) {
        showToast( context, context.getString(resId), duration, build );
    }
    public void showToast(@NonNull Context context, @StringRes int resId, int duration) {
        showToast( context, context.getString(resId), duration );
    }

    public void showToast(@NonNull Context context,
                          @StringRes int resId,
                          @Nullable Build build) {
        showToast( context, resId, Toast.LENGTH_SHORT, build );
    }
    public void showToast(@NonNull Context context, @StringRes int resId) {
        showToast( context, resId, Toast.LENGTH_SHORT );
    }

    public void showToastForLong(@NonNull Context context,
                                 CharSequence s,
                                 @Nullable Build build) {
        showToast( context, s, Toast.LENGTH_LONG, build );
    }
    public void showToastForLong(@NonNull Context context, CharSequence s) {
        showToast( context, s, Toast.LENGTH_LONG );
    }

    public void showToastForLong(@NonNull Context context,
                                 @StringRes int resId,
                                 @Nullable Build build) {
        showToast( context, resId, Toast.LENGTH_LONG, build );
    }
    public void showToastForLong(@NonNull Context context, @StringRes int resId) {
        showToast( context, resId, Toast.LENGTH_LONG );
    }
}