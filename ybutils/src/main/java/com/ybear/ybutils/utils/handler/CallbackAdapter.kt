package com.ybear.ybutils.utils.handler

import android.os.Message
import java.lang.ref.WeakReference

abstract class CallbackAdapter<T> @JvmOverloads constructor(data: T? = null) : Callback {
    private val mWeakReferenceData: WeakReference<T?> = WeakReference(data)

    val data: T?
        get() = mWeakReferenceData.get()

    override fun handleMessage(msg: Message): Boolean {
        return handleMessage(msg, data)
    }

    override fun dispatchMessage(msg: Message) {
        dispatchMessage(msg, data)
    }

    open fun handleMessage(msg: Message, data: T?): Boolean {
        return false
    }

    open fun dispatchMessage(msg: Message, data: T?) {}
}