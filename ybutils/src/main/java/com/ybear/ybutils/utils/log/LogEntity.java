package com.ybear.ybutils.utils.log;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.io.File;
import java.nio.charset.Charset;

public final class LogEntity implements Parcelable {
    //保存的日志路径（不保存文件名）
    private File savePath;
    //是否保存日志到本地（即回调时不会存储到设备中）
    private boolean enableSave;
    //生成新的日志间隔秒数
    private long newLogFileIntervalSecond = 300L;
    //日志编码类型
    private Charset charset = Charset.defaultCharset();
    //日志标签
    private String tag;
    //日志类型
    @LogType
    private int type;
    private String typeString;
    //日志内容
    private String message;
    private long createTimestamp;
    //自定义日志（不为空时，只会将它保存到本地）
    private String customMessage;

    public LogEntity() {}
    public LogEntity(Parcel in) {
        enableSave = in.readByte() != 0;
        newLogFileIntervalSecond = in.readLong();
        tag = in.readString();
        type = in.readInt();
        typeString = in.readString();
        message = in.readString();
        createTimestamp = in.readLong();
        customMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (enableSave ? 1 : 0));
        dest.writeLong(newLogFileIntervalSecond);
        dest.writeString(tag);
        dest.writeInt(type);
        dest.writeString(typeString);
        dest.writeString(message);
        dest.writeLong(createTimestamp);
        dest.writeString(customMessage);
    }

    @Override
    public int describeContents() { return 0; }

    public static final Creator<LogEntity> CREATOR = new Creator<LogEntity>() {
        @Override
        public LogEntity createFromParcel(Parcel in) { return new LogEntity( in ); }

        @Override
        public LogEntity[] newArray(int size) { return new LogEntity[ size ]; }
    };

    @NonNull
    @Override
    public String toString() {
        return "LogEntity{" +
                "savePath=" + savePath +
                ", enableSave=" + enableSave +
                ", newLogFileIntervalSecond=" + newLogFileIntervalSecond +
                ", charset=" + charset +
                ", tag='" + tag + '\'' +
                ", type=" + type +
                ", typeString='" + typeString + '\'' +
                ", message='" + message + '\'' +
                ", createTimestamp=" + createTimestamp +
                ", customMessage='" + customMessage + '\'' +
                '}';
    }

    void setSavePath(File savePath, boolean enableSave) {
        this.savePath = savePath;
        this.enableSave = enableSave;
    }
    @NonNull
    File getSavePath() { return savePath; }

    boolean isEnableSave() { return enableSave; }

    void setNewLogFileIntervalSecond(long intervalSecond) {
        newLogFileIntervalSecond = intervalSecond;
    }
    long getNewLogFileIntervalSecond() { return newLogFileIntervalSecond; }

    LogEntity setType(@LogType int type) {
        this.type = type;
        return this;
    }
    @LogType
    public int getType() { return type; }

    String getTypeString() {
        return typeString;
    }

    LogEntity setTypeString(String typeString) {
        this.typeString = typeString;
        return this;
    }

    long getCreateTimestamp() {
        return createTimestamp;
    }

    void setCreateTimestamp(long createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public void setCustomMessage(String message) {
        customMessage = message;
    }
    String getCustomMessage() { return customMessage; }

    public LogEntity setCharset(Charset charset) {
        this.charset = charset;
        return this;
    }
    public Charset getCharset() { return charset; }


    public LogEntity setTag(String tag) {
        this.tag = tag;
        return this;
    }
    public String getTag() { return tag; }

    public LogEntity setMessage(String message) {
        this.message = message;
        return this;
    }
    public String getMessage() { return message; }

    public LogEntity setSavePath(File savePath) {
        this.savePath = savePath;
        return this;
    }
}