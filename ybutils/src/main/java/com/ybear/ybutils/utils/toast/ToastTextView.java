package com.ybear.ybutils.utils.toast;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import java.util.Locale;

public class ToastTextView extends AppCompatTextView implements IToastXShape {
    private ToastXShapeHelper mHelper;

    public ToastTextView(@NonNull Context context) {
        this(context, null);
    }

    public ToastTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToastTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mHelper = new ToastXShapeHelper().init( this );
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        mHelper.dispatchDraw( canvas, this );
    }

    @Override
    public void setRadius(float r) { mHelper.setRadius( r ); }

    @Override
    public void setRadius(float lt, float rt, float lb, float rb) {
        mHelper.setRadius( lt, rt, lb, rb );
    }

    @Override
    public void setRadii(float[] radii) { mHelper.setRadii( radii ); }

    @Nullable
    @Override
    public float[] getRadii() { return mHelper.getRadii(); }

    @Override
    public void setBorderSize(int size) { mHelper.setBorderSize( size ); }
    @Override
    public void setBorderColor(int color) { mHelper.setBorderColor( color ); }

    @Override
    public void setShadowRadius(int radius) { mHelper.setShadowRadius( radius ); }
    @Override
    public void setShadowColor(@ColorInt int color) { mHelper.setShadowColor( color ); }
    @Override
    public void setShadowOffsetX(int offsetX) { mHelper.setShadowOffsetX( offsetX ); }
    @Override
    public void setShadowOffsetY(int offsetY) { mHelper.setShadowOffsetY( offsetY ); }

    @Override
    public void setEnableRtlLayout(Locale locale) { mHelper.setEnableRtlLayout( locale ); }

    @Override
    public void setDisableRtlLayout() { mHelper.setDisableRtlLayout(); }

    @Override
    public void setEnableLayerTypeHardware(@NonNull View view, boolean enable) {
        mHelper.setEnableLayerTypeHardware( view, enable );
    }
}
