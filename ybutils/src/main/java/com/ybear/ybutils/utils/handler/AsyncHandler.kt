package com.ybear.ybutils.utils.handler

import androidx.arch.core.util.Function
import androidx.core.util.Consumer
import java.util.concurrent.Callable

interface AsyncHandler {
    /**
     * 异步Handler处理（通过Return返回数据）
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post回调
     * @param <T>               数据源
     * @return                  post结果
    </T> */
    fun <T> postAsync(executeCall: Callable<T>, resultCall: Consumer<T>, postResult: Boolean): Boolean

    /**
     * 异步Handler处理（通过回调返回数据）
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post回调
     * @param <T>               数据源
     * @return                  post结果
    </T> */
    fun <T> postAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>, postResult: Boolean): Boolean

    /**
     * 异步Handler处理（通过Return返回数据） - 延迟调用
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post返回。默认：true
     * @param <T>               数据源
     * @return                  post结果
    </T> */
    fun <T> postDelayedAsync(executeCall: Callable<T>, resultCall: Consumer<T>, postResult: Boolean, delayMillis: Long): Boolean

    /**
     * 异步Handler处理（通过回调返回数据） - 延迟调用
     * @param executeCall       处理的异步内容
     * @param resultCall        返回处理的内容
     * @param postResult        返回是否使用post返回。默认：true
     * @param <T>               数据源
     * @return                  post结果
    </T> */
    fun <T> postDelayedAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>, postResult: Boolean, delayMillis: Long): Boolean
    fun <T> postAsync(executeCall: Callable<T>, resultCall: Consumer<T>, postResult: Boolean, delayMillis: Long): Boolean
    fun <T> postAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>, postResult: Boolean, delayMillis: Long): Boolean
    fun <T> postAsync(executeCall: Callable<T>, resultCall: Consumer<T>): Boolean
    fun <T> postAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>): Boolean
    fun <T> postDelayedAsync(executeCall: Callable<T>, resultCall: Consumer<T>, delayMillis: Long): Boolean
    fun <T> postDelayedAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>, delayMillis: Long): Boolean
    fun <T> postAsync(executeCall: Callable<T>, resultCall: Consumer<T>, delayMillis: Long): Boolean
    fun <T> postAsync(executeCall: Consumer<Consumer<T>>, resultCall: Consumer<T>, delayMillis: Long): Boolean

    /**
     * 异步Handler处理 - 此方法不支持post返回
     * @param executeCall       处理的异步内容
     * @return                  post结果
     */
    fun postAsync(executeCall: Runnable): Boolean
    fun postDelayedAsync(executeCall: Runnable, delayMillis: Long): Boolean
    fun postAsync(executeCall: Runnable, delayMillis: Long): Boolean
}