package com.ybear.ybutils.utils.handler

import android.os.Looper
import android.view.View

class HandlerManage private constructor() {
    private fun newHandler(looper: Looper, call: Callback): Handler {
        return HandlerX( looper, call )
    }
    private fun newHandler(looper: Looper): Handler {
        return HandlerX( looper )
    }
    private fun newHandler(call: Callback): Handler {
        return HandlerX( call )
    }
    private fun newHandlerForOs(looper: Looper, call: android.os.Handler.Callback): Handler {
        return HandlerX( looper, call )
    }
    private fun newHandlerForOs(call: android.os.Handler.Callback): Handler {
        return HandlerX( call )
    }
    private fun newHandlerForOs(looper: Looper): Handler {
        return HandlerX( looper )
    }

    companion object {
        @JvmStatic
        private val i : HandlerManage by lazy { HandlerManage() }
        /**
         * 创建一个主消息队列Handler
         * @param looper  当前的消息循环
         * @param call    消息回调
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun create(looper: Looper, call: Callback): Handler {
            return i.newHandler( looper, call )
        }

        /**
         * 创建一个主消息队列Handler
         * @param looper  当前的消息循环
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun create(looper: Looper): Handler {
            return i.newHandler( looper )
        }
        /**
         * 创建一个主消息队列Handler
         * @param call    消息回调
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun create(call: Callback): Handler {
            return i.newHandler( call )
        }

        @JvmStatic
        fun create(): Handler {
            return i.newHandler( Looper.getMainLooper() )
        }

        /**
         * 创建一个主消息队列Handler（Android自带的Handler）
         * @param looper  当前的消息循环
         * @param call    消息回调
         * @return        Handler [android.os.Handler]
         */
        @JvmStatic
        fun createForOs(looper: Looper, call: android.os.Handler.Callback): Handler {
            return i.newHandlerForOs( looper, call )
        }

        /**
         * 创建一个主消息队列Handler（Android自带的Handler）
         * @param call    消息回调
         * @return        Handler [android.os.Handler]
         */
        @JvmStatic
        fun createForOs(call: android.os.Handler.Callback): Handler {
            return i.newHandlerForOs( call )
        }

        /**
         * 创建一个主消息队列Handler（Android自带的Handler）
         * @param looper  当前的消息循环
         * @param call    消息回调
         * @return        Handler [android.os.Handler]
         */
        @JvmStatic
        fun createForOs(looper: Looper, call: Callback): Handler {
            return i.newHandlerForOs(looper, call)
        }

        /**
         * 创建一个主消息队列Handler（Android自带的Handler）
         * @param call    消息回调
         * @return        Handler [android.os.Handler]
         */
        @JvmStatic
        fun createForOs(call: Callback): Handler {
            return i.newHandlerForOs( call )
        }
        @JvmStatic
        fun createForOs(): Handler {
            return i.newHandlerForOs( Looper.getMainLooper() )
        }

        /**
         * 创建一个主消息队列Handler
         * @param v       获取当前View下的myLooper
         * @param call    消息回调
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun create(v: View, call: Callback): Handler {
            return create( getLooper( v ), call )
        }

        /**
         * 创建一个主消息队列Handler
         * @param v       获取当前View下的myLooper
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun create(v: View): Handler { return create( getLooper( v ) ) }
        /**
         * 创建一个当前消息队列Handler
         * @return        Handler [com.ybear.ybutils.utils.handler.Handler]
         */
        @JvmStatic
        fun createForMyLooper(call: Callback): Handler {
            val looper = Looper.myLooper()
            return if( looper == null )
                create( Looper.getMainLooper(), call )
            else
                create( looper, call )
        }

        /**
         * 创建一个当前消息队列Handler（Android自带的Handler）
         * @param call    消息回调
         * @return        Handler [android.os.Handler]
         */
        @JvmStatic
        fun createForOsMyLooper(call: android.os.Handler.Callback): Handler {
            val looper = Looper.myLooper()
            return if( looper == null )
                createForOs( Looper.getMainLooper(), call )
            else
                createForOs( looper, call )
        }

        private fun getLooper(v: View): Looper {
            return if ( v.context != null ) v.context.mainLooper else Looper.getMainLooper()
        }
    }
}