package com.ybear.ybutils.utils.toast;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.Gravity;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

import com.ybear.ybutils.utils.ResUtil;

import java.util.Arrays;

public class Build implements Parcelable {
    private int[] compoundDrawables = { 0, 0, 0, 0 };
    private int compoundDrawablesPadding = 0;
    //字体大小
    private float textSize = 12;
    //字体颜色
    @ColorInt
    private int textColor = Color.BLACK;
    //文字显示位置
    private int textGravity = Gravity.CENTER;
    //字体
    private Typeface typeface = null;
    /* 背景 */
    @DrawableRes
    private int backgroundResource = -1;
    @ColorRes
    private int backgroundColor = -1;
    private Drawable background = null;
    //内边距
    private int[] padding = { 30, 8, 30, 8 };
    private float[] margin = { 0, 0 };
    //位置
    private int gravity = Gravity.BOTTOM;
    private int xOffset = 0;
    private int yOffset = 0;

    /* Shape */
    private float[] radius;
    private int borderSize;
    @ColorInt
    private int borderColor;
    private int shadowRadius;
    @ColorInt
    private int shadowColor;
    private int shadowOffsetX;
    private int shadowOffsetY;
    private boolean cancelOnDestroy = true;

    public Build() {}

    protected Build(Parcel in) {
        compoundDrawables = in.createIntArray();
        compoundDrawablesPadding = in.readInt();
        textSize = in.readFloat();
        textColor = in.readInt();
        textGravity = in.readInt();
        backgroundResource = in.readInt();
        backgroundColor = in.readInt();
        padding = in.createIntArray();
        margin = in.createFloatArray();
        gravity = in.readInt();
        xOffset = in.readInt();
        yOffset = in.readInt();
        cancelOnDestroy = in.readInt() == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(compoundDrawables);
        dest.writeInt(compoundDrawablesPadding);
        dest.writeFloat(textSize);
        dest.writeInt(textColor);
        dest.writeInt(textGravity);
        dest.writeInt(backgroundResource);
        dest.writeInt(backgroundColor);
        dest.writeIntArray(padding);
        dest.writeFloatArray(margin);
        dest.writeInt(gravity);
        dest.writeInt(xOffset);
        dest.writeInt(yOffset);
        dest.writeInt(cancelOnDestroy ? 1 : 0);
    }

    public static final Creator<Build> CREATOR = new Creator<Build>() {
        @Override
        public Build createFromParcel(Parcel in) {
            return new Build(in);
        }

        @Override
        public Build[] newArray(int size) {
            return new Build[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "Build{" +
                "compoundDrawables=" + Arrays.toString(compoundDrawables) +
                ", compoundDrawablesPadding=" + compoundDrawablesPadding +
                ", textSize=" + textSize +
                ", textColor=" + textColor +
                ", textGravity=" + textGravity +
                ", typeface=" + typeface +
                ", backgroundResource=" + backgroundResource +
                ", backgroundColor=" + backgroundColor +
                ", background=" + background +
                ", padding=" + Arrays.toString(padding) +
                ", margin=" + Arrays.toString(margin) +
                ", gravity=" + gravity +
                ", xOffset=" + xOffset +
                ", yOffset=" + yOffset +
                ", cancelOnDestroy=" + cancelOnDestroy +
                '}';
    }

    public Build setCompoundDrawablesWithIntrinsicBounds(@DrawableRes int start,
                                                         @DrawableRes int top,
                                                         @DrawableRes int end,
                                                         @DrawableRes int bottom) {
        compoundDrawables[ 0 ] = start;
        compoundDrawables[ 1 ] = top;
        compoundDrawables[ 2 ] = end;
        compoundDrawables[ 3 ] = bottom;
        return this;
    }
    public int[] getCompoundDrawables() { return compoundDrawables; }

    public int getCompoundDrawablesPadding() { return compoundDrawablesPadding; }
    public Build setCompoundDrawablesPadding(int padding) {
        compoundDrawablesPadding = padding;
        return this;
    }

    public float getTextSize() { return textSize; }
    public Build setTextSize(float textSize) {
        this.textSize = textSize;
        return this;
    }

    @ColorInt
    public int getTextColor() { return textColor; }

    public Build setTextColor(@ColorInt int textColor) {
        this.textColor = textColor;
        return this;
    }

    public Build setTextColorResources(Context context, @ColorRes int textColor) {
        return setTextColor( ResUtil.getColorInt( context, textColor ) );
    }

    public int getTextGravity() { return textGravity; }
    public Build setTextGravity(int textGravity) {
        this.textGravity = textGravity;
        return this;
    }

    public Typeface getTypeface() { return typeface; }
    public Build setTypeface(Typeface typeface) {
        this.typeface = typeface;
        return this;
    }

    @DrawableRes
    public int getBackgroundResource() { return backgroundResource; }

    public Build setBackgroundResource(@DrawableRes int resource) {
        backgroundResource = resource;
        return this;
    }

    public int getBackgroundColor() { return backgroundColor; }
    public Build setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public Drawable getBackground() { return background; }
    public Build setBackground(Drawable background) {
        this.background = background;
        return this;
    }

    public int[] getPadding() { return padding; }
    public Build setPadding(int start, int top, int end, int bottom) {
        this.padding[0] = start;
        this.padding[1] = top;
        this.padding[2] = end;
        this.padding[3] = bottom;
        return this;
    }

    public Build setMargin(float horizontalMargin, float verticalMargin) {
        margin[ 0 ] = horizontalMargin;
        margin[ 1 ] = verticalMargin;
        return this;
    }

    public float[] getMargin() { return margin; }

    public int getGravity() { return gravity; }
    public Build setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public int getXOffset() { return xOffset; }
    public Build setXOffset(int xOffset) {
        this.xOffset = xOffset;
        return this;
    }

    public int getYOffset() { return yOffset; }
    public Build setYOffset(int yOffset) {
        this.yOffset = yOffset;
        return this;
    }

    /* Shape */
    public Build  setRadius(float lt, float rt, float lb, float rb) {
        radius = new float[] { lt, rt, lb, rb };
        return this;
    }
    public Build  setRadius(float r) {
        return setRadius( r, r, r, r );
    }

    public float[] getRadius() { return radius; }

    public Build setBorderSize(int size) {
        borderSize = size;
        return this;
    }
    public int getBorderSize() { return borderSize; }

    public Build setBorderColor(@ColorInt int color) {
        borderColor = color;
        return this;
    }
    public int getBorderColor() { return borderColor; }

    public Build setShadowRadius(int radius) {
        shadowRadius = radius;
        return this;
    }
    public int getShadowRadius() { return shadowRadius; }

    public Build setShadowColor(@ColorInt int color) {
        shadowColor = color;
        return this;
    }
    public int getShadowColor() { return shadowColor; }

    public Build setShadowOffsetX(int offsetX) {
        shadowOffsetX = offsetX;
        return this;
    }
    public int getShadowOffsetX() { return shadowOffsetX; }

    public Build setShadowOffsetY(int offsetY) {
        shadowOffsetY = offsetY;
        return this;
    }
    public int getShadowOffsetY() { return shadowOffsetY; }

    public boolean isCancelOnDestroy() {
        return cancelOnDestroy;
    }

    public Build setCancelOnDestroy(boolean cancelOnDestroy) {
        this.cancelOnDestroy = cancelOnDestroy;
        return this;
    }
}