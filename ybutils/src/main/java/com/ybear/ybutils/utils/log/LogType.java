package com.ybear.ybutils.utils.log;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({ LogType.V, LogType.D, LogType.I, LogType.W, LogType.E })
@Retention(RetentionPolicy.SOURCE)
public @interface LogType {
    int V = 0;
    int D = 1;
    int I = 2;
    int W = 3;
    int E = 4;
}