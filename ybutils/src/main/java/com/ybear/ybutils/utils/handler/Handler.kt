package com.ybear.ybutils.utils.handler

import android.os.Build
import android.os.Looper
import android.os.Message
import android.util.Printer
import androidx.annotation.RequiresApi

interface Handler : AsyncHandler {
    val osHandler: android.os.Handler
    val handler: Handler

    fun postCheck(looper: Looper?, r: Runnable): Boolean
    fun postCheck(r: Runnable): Boolean

    fun post(r: Runnable): Boolean
    fun post(r: Runnable, delayMillis: Long): Boolean
    @RequiresApi(api = Build.VERSION_CODES.P)
    fun post(r: Runnable, token: Any?, delayMillis: Long): Boolean
    fun postDelayed(r: Runnable, delayMillis: Long): Boolean
    @RequiresApi(api = Build.VERSION_CODES.P)
    fun postDelayed(r: Runnable, token: Any?, delayMillis: Long): Boolean
    fun postAtFrontOfQueue(r: Runnable): Boolean
    fun postAtTime(r: Runnable, uptimeMillis: Long): Boolean
    fun postAtTime(r: Runnable, token: Any?, uptimeMillis: Long): Boolean

    fun sendMessage(msg: Message): Boolean
    fun sendEmptyMessage(what: Int): Boolean
    fun sendEmptyMessageDelayed(what: Int, delayMillis: Long): Boolean
    fun sendEmptyMessageAtTime(what: Int, uptimeMillis: Long): Boolean
    fun sendMessageDelayed(msg: Message, delayMillis: Long): Boolean
    fun sendMessageAtTime(msg: Message, uptimeMillis: Long): Boolean
    fun sendMessageAtFrontOfQueue(msg: Message): Boolean
    fun hasMessages(what: Int): Boolean
    fun hasMessages(what: Int, `object`: Any?): Boolean
    fun removeMessages(what: Int)
    fun removeMessages(what: Int, `object`: Any?)

    @RequiresApi(api = Build.VERSION_CODES.Q)
    fun hasCallbacks(r: Runnable): Boolean

    fun removeCallbacks(r: Runnable)
    fun removeCallbacks(r: Runnable, token: Any?)
    fun removeCallbacksAndMessages(token: Any?)

    fun obtainMessage(): Message
    fun obtainMessage(what: Int): Message
    fun obtainMessage(what: Int, obj: Any?): Message
    fun obtainMessage(what: Int, arg1: Int, arg2: Int): Message
    fun obtainMessage(what: Int, arg1: Int, arg2: Int, obj: Any?): Message

    @RequiresApi(api = Build.VERSION_CODES.P)
    fun createAsyncForOs(looper: Looper): android.os.Handler
    @RequiresApi(api = Build.VERSION_CODES.P)
    fun createAsyncForOs(looper: Looper, callback: android.os.Handler.Callback): android.os.Handler

    fun dump(pw: Printer, prefix: String)
    val looper: Looper
}