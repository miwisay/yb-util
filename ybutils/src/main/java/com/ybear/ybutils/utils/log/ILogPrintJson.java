package com.ybear.ybutils.utils.log;

import java.io.Serializable;

/**
 * 实现该接口后自动输出成json
 */
public interface ILogPrintJson extends Serializable {
    default String toJSONString() {
        return LogUtil.toJSONString( this );
    }
}