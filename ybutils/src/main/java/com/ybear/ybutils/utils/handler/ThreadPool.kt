package com.ybear.ybutils.utils.handler

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ThreadPool {
    private val mPool = Executors.newCachedThreadPool()

    companion object {
        @JvmStatic
        private val i : ThreadPool by lazy { ThreadPool() }
        @JvmStatic
        fun get(): ThreadPool { return i }
    }

    fun execute(run: Runnable?) {
        mPool.execute( run )
    }

    fun getExecutorService(): ExecutorService { return mPool }
}