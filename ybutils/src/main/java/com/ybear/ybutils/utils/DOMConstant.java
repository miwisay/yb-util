package com.ybear.ybutils.utils;

public class DOMConstant {
    private static final int INIT_VAL = Integer.MAX_VALUE - 65536;
    /**
     * 退出APP通知
     */
    public static final int DOM_EXIT_APP = INIT_VAL;
    /**
     * 重启APP通知
     */
    public static final int DOM_RESTART_APP = INIT_VAL + 1;
    /**
     * 回到桌面通知
     */
    public static final int DOM_BACK_PRESSED_HOME = INIT_VAL + 2;
    /**
     * 双击返回第一次
     */
    public static final int DOM_DOUBLE_BACK_EXIT_START = INIT_VAL + 3;
    /**
     * 双击返回最后一次
     */
    public static final int DOM_DOUBLE_BACK_EXIT_END = INIT_VAL + 4;
}
