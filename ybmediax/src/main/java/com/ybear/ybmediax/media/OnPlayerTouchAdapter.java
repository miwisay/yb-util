package com.ybear.ybmediax.media;

public class OnPlayerTouchAdapter implements OnPlayerTouchListener {
    @Override
    public void onDown() { }

    @Override
    public void onClick() { }

    @Override
    public void onLongClick() { }

    @Override
    public void onDoubleClick() { }

    @Override
    public void onSlide(Slide slide, int progress) { }
}
