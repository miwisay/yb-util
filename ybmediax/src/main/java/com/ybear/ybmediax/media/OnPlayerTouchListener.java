package com.ybear.ybmediax.media;

public interface OnPlayerTouchListener {
    void onDown();
    void onClick();
    void onLongClick();
    void onDoubleClick();
    void onSlide(Slide slide, int value);

    enum Slide {
        LEFT_TOP_AND_BOTTOM,
        RIGHT_TOP_AND_BOTTOM,
        LEFT_AND_RIGHT_OF_LEFT,
        LEFT_AND_RIGHT_OF_RIGHT
    }
}
