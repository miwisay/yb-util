package com.ybear.ybmediax.media;

public class MediaXStatusAdapter implements MediaXStatus {
    @Override
    public void onPlay() { }

    @Override
    public void onPause() { }

    @Override
    public void onStop() { }

    @Override
    public void onReset() { }

    @Override
    public void onRelease() { }

    @Override
    public void onCompletion(int currentPlayNum, int playTotal, boolean isCompletion) { }

    @Override
    public void onBufferingUpdate(int percent) { }

    @Override
    public boolean onError(int what, int extra) { return false; }
}
