package com.ybear.ybmediax.media;

public interface ScreenOrientationChangeListener {
    void onOrientationChange(int orientation, int angle);
}
