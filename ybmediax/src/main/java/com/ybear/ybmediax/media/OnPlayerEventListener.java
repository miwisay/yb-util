package com.ybear.ybmediax.media;

public interface OnPlayerEventListener {
    void onPlayerEvent();
    void onPauseEvent();
    void onOnEvent();
    void onNextEvent();
    void onPortraitFlipEvent();
    void onLandscapeFlipEvent();
    void onFinishEvent();
}
