package com.ybear.ybmediax.media;

import android.content.res.AssetFileDescriptor;
import android.media.MediaDataSource;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.io.FileDescriptor;
import java.net.HttpCookie;
import java.util.List;
import java.util.Map;

public final class Data {
    private Object data;
    private Map<String, String> headers;
    private List<HttpCookie> cookies;
    private long offset = -1;
    private long length = -1;

    public Data(Data data) {
        this.data = data.getData();
        this.headers = data.getHeaders();
        this.cookies = data.getCookies();
        this.offset = data.getOffset();
        this.length = data.getLength();
    }

    public boolean isEq(Data data) {
        return this.data != null && data != null && this.data.equals( data.getData() );
//        if( this.data == null || !this.data.equals( data.getData() ) ) return false;
//        if( headers == null || !headers.equals( data.getHeaders() ) ) return false;
//        if( cookies == null || !cookies.equals( data.getCookies() ) ) return false;
//        if( offset != data.getOffset() ) return false;
//        return length == data.getLength();
    }
    public Data(String s) { data = s; }
    public Data(Uri uri, Map<String, String> headers, List<HttpCookie> cookies) {
        data = uri;
        this.headers = headers;
        this.cookies = cookies;
    }
    public Data(Uri uri, Map<String, String> headers) { this( uri, headers, null ); }
    public Data(Uri uri) { this( uri, null, null ); }
    public Data(FileDescriptor fd, long offset, long length) {
        data = fd;
        this.offset = offset;
        this.length = length;
    }
    public Data(FileDescriptor fd) { this( fd, -1, -1 ); }
    public Data(AssetFileDescriptor afd) { data = afd; }
    public Data(MediaDataSource mds) { data = mds; }

    @NonNull
    @Override
    public String toString() {
        return "Data{" +
                "data=" + data +
                ", headers=" + headers +
                ", cookies=" + cookies +
                ", offset=" + offset +
                ", length=" + length +
                '}';
    }

    public Object getData() { return data; }
    public void setData(Object data) { this.data = data; }

    public Map<String, String> getHeaders() { return headers; }
    public void setHeaders(Map<String, String> headers) { this.headers = headers; }

    public List<HttpCookie> getCookies() { return cookies; }
    public void setCookies(List<HttpCookie> cookies) { this.cookies = cookies; }

    public long getOffset() { return offset; }
    public void setOffset(long offset) { this.offset = offset; }

    public long getLength() { return length; }
    public void setLength(long length) { this.length = length; }
}