package com.ybear.ybmediax.media;

public interface MediaXStatus {
    void onPlay();
    void onPause();
    void onStop();
    void onReset();
    void onRelease();
    void onCompletion(int currentPlayNum, int playTotal, boolean isCompletion);
    void onBufferingUpdate(int percent);
    boolean onError(int what, int extra);
}