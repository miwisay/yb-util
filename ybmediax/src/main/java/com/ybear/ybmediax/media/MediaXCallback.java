package com.ybear.ybmediax.media;

import android.content.res.AssetFileDescriptor;
import android.media.MediaDataSource;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.FileDescriptor;
import java.net.HttpCookie;
import java.util.List;
import java.util.Map;

public interface MediaXCallback {
    MediaX setDataSource(String... paths);
    MediaX setDataSource(Uri... uris);
    MediaX setDataSource(@NonNull Uri uri, @Nullable Map<String, String> headers);
    MediaX setDataSource(@NonNull Uri uri,
                       @Nullable Map<String, String> headers,
                       @Nullable List<HttpCookie> cookies);
    MediaX setDataSource(FileDescriptor... fd);
    MediaX setDataSource(@NonNull FileDescriptor fd, long offset, long length);
    MediaX setDataSource(AssetFileDescriptor... afd);
    MediaX setDataSource(MediaDataSource... mds);

    void play();
    void play(long delayed);
    void on();
    void next();
    void select(int index);
    void pause();
    void stop();
    void reset();
    void release();
    MediaX setSpeed(float speed);
    MediaX seekTo(int progress);
    MediaX setLooping(boolean enable);
    boolean isLooping();
    boolean isPlaying();
//    void setEnableOrientation(boolean enable);
    MediaX setOnMediaStatusListener(MediaXStatus l);
//    void setFollowSystemRotation(boolean enable);
//    void setOrientationChangedListener(ScreenOrientationChangeListener l);
}
