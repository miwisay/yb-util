package com.ybear.ybmediax.audio;

public class OnAudioStatusAdapter implements OnAudioStatusListener {
    @Override
    public void onPlayed() { }

    @Override
    public void onPaused() { }

    @Override
    public void onCompleted() { }

    @Override
    public boolean onError(Exception e) { return true; }
}
