package com.ybear.ybmediax.audio;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

public class Config {
    //流类型
    @StreamType
    private int streamType = StreamType.STREAM_MUSIC;
    //采样率
    @RateInHzType
    private int sampleRateInHz = RateInHzType.RATE_8000;
    //声道
    @ChannelType
    private int channelConfig = ChannelType.CHANNEL_OUT_STEREO;
    //比特位
    private int encoding = AudioFormat.ENCODING_PCM_16BIT;

    private int contentType = AudioAttributes.CONTENT_TYPE_MUSIC;

    private Config() {}
    public static Config newConfig() { return new Config(); }

    @StreamType
    public int getStreamType() { return streamType; }
    public Config setStreamType(@StreamType int streamType) {
        this.streamType = streamType;
        return this;
    }

    @RateInHzType
    public int getSampleRateInHz() { return sampleRateInHz; }
    public Config setSampleRateInHz(@RateInHzType int sampleRateInHz) {
        this.sampleRateInHz = sampleRateInHz;
        return this;
    }

    @ChannelType
    public int getChannelConfig() { return channelConfig; }
    public Config setChannelConfig(@ChannelType int channelConfig) {
        this.channelConfig = channelConfig;
        return this;
    }

    public int getEncoding() { return encoding; }

    /**
     *         AudioFormat.ENCODING_AAC_ELD,
     *         AudioFormat.ENCODING_AAC_HE_V1,
     *         AudioFormat.ENCODING_AAC_HE_V2,
     *         AudioFormat.ENCODING_AAC_LC,
     *         AudioFormat.ENCODING_AAC_XHE,
     *         AudioFormat.ENCODING_AC3,
     *         AudioFormat.ENCODING_AC4,
     *         AudioFormat.ENCODING_DEFAULT,
     *         AudioFormat.ENCODING_DOLBY_MAT,
     *         AudioFormat.ENCODING_DOLBY_TRUEHD,
     *         AudioFormat.ENCODING_DRA,
     *         AudioFormat.ENCODING_DTS,
     *         AudioFormat.ENCODING_DTS_HD,
     *         AudioFormat.ENCODING_DTS_UHD,
     *         AudioFormat.ENCODING_E_AC3,
     *         AudioFormat.ENCODING_E_AC3_JOC,
     *         AudioFormat.ENCODING_IEC61937,
     *         AudioFormat.ENCODING_INVALID,
     *         AudioFormat.ENCODING_MP3,
     *         AudioFormat.ENCODING_MPEGH_BL_L3,
     *         AudioFormat.ENCODING_MPEGH_BL_L4,
     *         AudioFormat.ENCODING_MPEGH_LC_L3,
     *         AudioFormat.ENCODING_MPEGH_LC_L4,
     *         AudioFormat.ENCODING_OPUS,
     *         AudioFormat.ENCODING_PCM_16BIT,
     *         AudioFormat.ENCODING_PCM_24BIT_PACKED,
     *         AudioFormat.ENCODING_PCM_32BIT,
     *         AudioFormat.ENCODING_PCM_8BIT,
     *         AudioFormat.ENCODING_PCM_FLOAT
     */
    public Config setEncoding(int encoding) {
        this.encoding = encoding;
        return this;
    }

    public int getMinBufferSize() {
        return AudioTrack.getMinBufferSize( sampleRateInHz, channelConfig, encoding );
    }

    public int getContentType() {
        return contentType;
    }

    /**
     *     AudioFormat.CONTENT_TYPE_MOVIE,
     *     AudioFormat.CONTENT_TYPE_MUSIC,
     *     AudioFormat.CONTENT_TYPE_SONIFICATION,
     *     AudioFormat.CONTENT_TYPE_SPEECH,
     *     AudioFormat.CONTENT_TYPE_UNKNOWN
     */
    public void setContentType(int contentType) {
        this.contentType = contentType;
    }
}