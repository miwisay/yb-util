package com.ybear.ybmediax.audio;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface RateInHzType {
    int RATE_4000 = 4000;

    int RATE_6000 = 6000;

    int RATE_8000 = 8000;

    int RATE_11025 = 11025;

    int RATE_16000 = 16000;

    int RATE_22050 = 22050;

    int RATE_32000 = 32000;

    int RATE_44100 = 44100;

    int RATE_48000 = 48000;

    int RATE_96000 = 96000;

    int RATE_192000 = 192000;
}