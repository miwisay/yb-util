package com.ybear.ybmediax.audio;

import android.media.AudioFormat;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface ChannelType {
    int CHANNEL_OUT_FRONT_LEFT = AudioFormat.CHANNEL_OUT_FRONT_LEFT;
    int CHANNEL_OUT_FRONT_RIGHT = AudioFormat.CHANNEL_OUT_FRONT_RIGHT;
    int CHANNEL_OUT_FRONT_CENTER = AudioFormat.CHANNEL_OUT_FRONT_CENTER;
    int CHANNEL_OUT_LOW_FREQUENCY = AudioFormat.CHANNEL_OUT_LOW_FREQUENCY;
    int CHANNEL_OUT_BACK_LEFT = AudioFormat.CHANNEL_OUT_BACK_LEFT;
    int CHANNEL_OUT_BACK_RIGHT = AudioFormat.CHANNEL_OUT_BACK_RIGHT;
    int CHANNEL_OUT_FRONT_LEFT_OF_CENTER = AudioFormat.CHANNEL_OUT_FRONT_LEFT_OF_CENTER;
    int CHANNEL_OUT_FRONT_RIGHT_OF_CENTER = AudioFormat.CHANNEL_OUT_FRONT_RIGHT_OF_CENTER;
    int CHANNEL_OUT_BACK_CENTER = AudioFormat.CHANNEL_OUT_BACK_CENTER;
    int CHANNEL_OUT_SIDE_LEFT =         AudioFormat.CHANNEL_OUT_SIDE_LEFT;
    int CHANNEL_OUT_SIDE_RIGHT =       AudioFormat.CHANNEL_OUT_SIDE_RIGHT;
    int CHANNEL_OUT_MONO = AudioFormat.CHANNEL_OUT_MONO;
    int CHANNEL_OUT_STEREO = AudioFormat.CHANNEL_OUT_STEREO;
    // aka QUAD_BACK
    int CHANNEL_OUT_QUAD = AudioFormat.CHANNEL_OUT_QUAD;
    int CHANNEL_OUT_SURROUND = AudioFormat.CHANNEL_OUT_SURROUND;
    // aka 5POINT1_BACK
    int CHANNEL_OUT_5POINT1 = AudioFormat.CHANNEL_OUT_5POINT1;
    // matches AUDIO_CHANNEL_OUT_7POINT1
    @RequiresApi(api = Build.VERSION_CODES.M)
    int CHANNEL_OUT_7POINT1_SURROUND = AudioFormat.CHANNEL_OUT_7POINT1_SURROUND;
}