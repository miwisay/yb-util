package com.ybear.ybmediax.audio;

import android.media.AudioManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE )
public @interface StreamType {
    /** Used to identify the volume of audio streams for phone calls */
    int STREAM_VOICE_CALL = AudioManager.STREAM_VOICE_CALL;
    /** Used to identify the volume of audio streams for system sounds */
    int STREAM_SYSTEM = AudioManager.STREAM_SYSTEM;
    /** Used to identify the volume of audio streams for the phone ring */
    int STREAM_RING = AudioManager.STREAM_RING;
    /** Used to identify the volume of audio streams for music playback */
    int STREAM_MUSIC = AudioManager.STREAM_MUSIC;
    /** Used to identify the volume of audio streams for alarms */
    int STREAM_ALARM = AudioManager.STREAM_ALARM;
    /** Used to identify the volume of audio streams for notifications */
    int STREAM_NOTIFICATION = AudioManager.STREAM_NOTIFICATION;
    /** Used to identify the volume of audio streams for DTMF Tones */
    int STREAM_DTMF = AudioManager.STREAM_DTMF;
    /** Used to identify the volume of audio streams for accessibility prompts */
    @RequiresApi(api = Build.VERSION_CODES.O)
    int STREAM_ACCESSIBILITY = AudioManager.STREAM_ACCESSIBILITY;
}