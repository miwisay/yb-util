package com.ybear.ybmediax.audio;

public interface OnAudioStatusListener {
    void onPlayed();
    void onPaused();
    void onCompleted();

    /**
     * 异常时回调
     * @param e     异常结果
     * @return      返回true时，拦截异常时打印的日志 {@link Exception#printStackTrace()}
     *              返回false时，出现异常时会自动打印日志 {@link Exception#printStackTrace()}
     */
    boolean onError(Exception e);
}