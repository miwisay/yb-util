package com.ybear.ybmediax.cache;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.ybear.ybnetworkutil.http.Req;

import java.io.File;
import java.util.LinkedHashSet;

public interface IMediaXCache {
    void init(@NonNull Application app);
    void init(@NonNull Application app, @Nullable Req req);
    void init(@NonNull Application app, @Nullable String rootPath);
    void init(@NonNull Application app, @Nullable Req req, @Nullable String rootPath);
    File get(String url);
    void add(Consumer<Boolean> call, LinkedHashSet<String> urls);
    void add(Consumer<Boolean> call, String urls);
    void setDataSource(String urls);
    void setDataSource(Consumer<Boolean> call, String urls);
    void remove(String url);
    void clear();
    void setMaxQueue(int max);
}
