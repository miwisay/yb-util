package com.ybear.ybmediax.cache;

import android.app.Application;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.ybear.ybnetworkutil.call.CallDownloadAdapter;
import com.ybear.ybnetworkutil.http.HttpClient;
import com.ybear.ybnetworkutil.http.Req;
import com.ybear.ybutils.utils.DigestUtil;
import com.ybear.ybutils.utils.SysUtil;
import com.ybear.ybutils.utils.log.LogUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.Call;

/**
 缓存帮助类
 流程：
 1.
     初始化时会检查并加入 {@link CacheHelper#mRootPath} 路径下的文件列表
     {@link CacheHelper#init(Application)}
 2.
     添加需要下载的链接（http/https） or 缓存File的路径({@link File#getAbsolutePath()})
     {@link CacheHelper#add(Consumer, String)}

 3.
     1.添加时会将链接 or 路径生成一个唯一的key，通过这个key与本地缓存文件作比对
     2.如果这个key不存在，会发起下载请求下载这个文件 {@link Req}
     3.如果请求的链接过多，会将链接加入等待队列 {@link CacheHelper#mPathWaitingQueue}
     4.一次性请求最大上限为 {@link CacheHelper#mMaxQueue}
     5.可以通过 {@link CacheHelper#setMaxQueue(int)} 设置一次性请求最大上限
 */
public final class CacheHelper implements IMediaXCache {
    private final String TAG = "CacheHelper";
    private final Queue<String> mPathWaitingQueue = new LinkedBlockingQueue<>();
    private final Map<String, File> mFileMap = new IdentityHashMap<>();
    private final Map<String, Consumer<Boolean>> mCallMap = new IdentityHashMap<>();
    private final AtomicInteger mQueueAtomic = new AtomicInteger( 0 );

    private int mMaxQueue = 4;
    private Req mReq;
    private String mRootPath;

    private CacheHelper() { }
    public static CacheHelper get() { return HANDLER.I; }
    private static final class HANDLER { private static final CacheHelper I = new CacheHelper(); }

    @Override
    public void init(@NonNull Application app) {
        init( app, null, null );
    }

    @Override
    public void init(@NonNull Application app, @Nullable Req req) {
        init( app, req, null );
    }

    @Override
    public void init(@NonNull Application app, @Nullable String rootPath) {
        init( app, null, rootPath );
    }

    @Override
    public void init(@NonNull Application app, @Nullable Req req, @Nullable String rootPath) {
        //存放缓存文件的路径
        mRootPath = rootPath == null || rootPath.length() == 0 ?
                SysUtil.getObbDir( app ).getAbsolutePath() + File.separator + "MediaXCache" + File.separator :
                rootPath;

        File rootFile = new File( mRootPath );
        if( !rootFile.exists() ) {
            boolean mk = rootFile.mkdirs();
            LogUtil.d( TAG, "root path mkdirs:" + mk );
        }
        File[] cacheFileList = rootFile.listFiles();
        if( cacheFileList != null && cacheFileList.length > 0 ) {
            new Thread(() -> {
                for( File file : cacheFileList ) mFileMap.put( file.getName(), file );
            }).start();
        }

        mReq = req == null ? HttpClient.create()
                .writeTimeout( 15 )
                .readTimeout( 15 )
                .connectTimeout( 15 )
                .build() :
                req;
    }

    @Override
    public File get(String url) {
        String findKey = getKey( url );
        for( String key : mFileMap.keySet() ) {
            if( !TextUtils.isEmpty( key ) && key.equals( findKey ) ) return mFileMap.get( key );
        }
        return null;
    }

    @Override
    public void add(Consumer<Boolean> call, LinkedHashSet<String> urls) {
        if( urls == null || urls.size() == 0 ) {
            if( call != null ) call.accept( true );
            return;
        }

        List<String> urlList = new ArrayList<>( urls );
        String nextKey, nextUrl;

        for( int i = 0; i < urlList.size(); i++ ) {
            nextUrl = urlList.get( i );
            if( TextUtils.isEmpty( nextUrl ) ) continue;
            nextKey = getKey( nextUrl );
            if( TextUtils.isEmpty( nextKey ) ) continue;

            if( i >= urlList.size() - 1 ) {
                mCallMap.put( nextKey, call );
                LogUtil.e( TAG, "add -> last url -> key:" + nextKey + " | " + nextUrl );
            }
            //是否存在缓存文件
            for( String fileKey : mFileMap.keySet() ) {
                if( TextUtils.isEmpty( fileKey ) || !fileKey.equals( nextKey ) ) continue;
                File f = mFileMap.get( fileKey );
                String path = f != null ? f.getAbsolutePath() : null;
                if( !TextUtils.isEmpty( path ) ) nextUrl = path;
                break;
            }
            //加入队列
            startQueue( nextKey, nextUrl );
        }
    }

    @Override
    public void add(Consumer<Boolean> call, String url) {
        add( call, new LinkedHashSet<>( Collections.singletonList( url ) ) );
    }

    @Override
    public void setDataSource(String urls) { add( null, urls ); }

    @Override
    public void setDataSource(Consumer<Boolean> call, String url) { add( call, url ); }

    @Override
    public void remove(String url) { mFileMap.remove( url ); }

    @Override
    public void clear() { mFileMap.clear(); }

    @Override
    public void setMaxQueue(int max) { mMaxQueue = max; }

    /**
     请求队列
     @param key         地址的key
     @param path        链接 or 文件路径
     */
    private void startQueue(String key, String path) {
        //最大队列限制
        if( mQueueAtomic.get() >= mMaxQueue ) {
            //加入队列
            mPathWaitingQueue.offer( toBase64OfEncode( path ) );
            LogUtil.d( TAG, "maximum queue limit  -> key:" + key + " | path:" + path );
            return;
        }
        //增加队列数
        mQueueAtomic.incrementAndGet();
        //非链接时，检查是不是文件地址
        if( !path.startsWith( "http" ) ) {
            File f = new File( path );
            putFileQueue( key, f );
            LogUtil.d( TAG, "local file -> key:" + key + " | file:" + f );
            return;
        }
        //下载文件
        mReq.req( path, new CallDownloadAdapter() {
            @Override
            public void onDownloadComplete(@NonNull Call call, @NonNull File f) {
                super.onDownloadComplete( call, f );
                //文件下载完成，加入到文件队列
                putFileQueue( key, f );
                LogUtil.d( TAG, "download complete -> key:" + key + " | file:" + f );
            }
            @Override
            public void onDownloadFailure(@NonNull Call call, @Nullable File f, @NonNull Exception e) {
                super.onDownloadFailure( call, f, e );
                //文件下载失败，加入到文件队列
                putFileQueue( key, f );
                LogUtil.e( TAG, "download failure -> key:" + key + " | file:" + f );
            }
        }).download( mRootPath + key );
    }
    /**
     文件队列
     @param key         文件的key
     @param file        文件（如果为空，说明这个key所指向的文件不存在（下载失败））
     */
    private void putFileQueue(String key, @Nullable File file) {
        //最后一个标记的回调
        Iterator<Map.Entry<String, Consumer<Boolean>>> iteratorCall = mCallMap.entrySet().iterator();

        while( !TextUtils.isEmpty( key ) && iteratorCall.hasNext() ) {
            Map.Entry<String, Consumer<Boolean>> entry = iteratorCall.next();
            if( entry == null ) continue;
            String keyCall = entry.getKey();
            Consumer<Boolean> valCall = entry.getValue();
            if( !key.equals( keyCall ) ) {
                LogUtil.e( TAG, "putFileQueue -> not exist -> key:" + key + " | keyCall:" + keyCall );
                continue;
            }
            if( valCall != null ) {
                valCall.accept( true );
                LogUtil.e( TAG, "putFileQueue -> call -> key:" + key + " | keyCall:" + keyCall + " | call:" + valCall );
            }
            iteratorCall.remove();
            break;
        }

        //减少队列数
        if( mQueueAtomic.get() > 0 ) mQueueAtomic.decrementAndGet();
        //加入/移除文件管理集合
        if( file != null && file.exists() && file.length() > 0 ) {
            mFileMap.put( key, file );
        }else {
            mFileMap.remove( key );
            if( file != null && file.length() == 0 ) {
                try { file.deleteOnExit(); }catch(Exception ignored) { }
            }
        }
        //开始下一个队列
        if( mPathWaitingQueue.size() > 0 ) {
            String nextUrl = toBase64OfDecode( mPathWaitingQueue.poll() );
            startQueue( getKey( nextUrl ), nextUrl );
        }
    }

    private String getKey(String s) {
        if( TextUtils.isEmpty( s ) ) return s;
        //处理链接
        int lastIndex = s.lastIndexOf( "/" );
        if( lastIndex != -1 ) lastIndex += 1;
        //处理文件
        if( lastIndex == -1 ) {
            lastIndex = s.lastIndexOf( File.separator );
            if( lastIndex != -1 ) lastIndex += 1;
        }
        //sha1加密
        return DigestUtil.toSHA1(
                lastIndex == -1 || lastIndex >= s.length() ? s : s.substring( lastIndex )
        );
    }

    private String toBase64OfEncode(String s) {
        return TextUtils.isEmpty( s ) ? s : Base64.encodeToString( s.getBytes(), Base64.DEFAULT );
    }
    private String toBase64OfDecode(String s) {
        byte[] decode = Base64.decode( s, Base64.DEFAULT );
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ) {
            return new String( decode, StandardCharsets.US_ASCII );
        }
        try {
            return new String( decode, "US-ASCII" );
        }catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String( decode );
    }
}
