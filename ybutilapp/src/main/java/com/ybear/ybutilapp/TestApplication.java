package com.ybear.ybutilapp;

import android.app.Application;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;

import com.ybear.ybcomponent.widget.dialog.Dialog;
import com.ybear.ybcomponent.widget.ribbon.config.EnterRibbonConfig;
import com.ybear.ybcomponent.widget.shape.helper.ShapeConfig;
import com.ybear.ybnetworkutil.network.Importance;
import com.ybear.ybnetworkutil.network.NetworkChangeManage;
import com.ybear.ybnetworkutil.network.NetworkConfig;
import com.ybear.ybutils.utils.LiveTime;
import com.ybear.ybutils.utils.Utils;
import com.ybear.ybutils.utils.log.LogUtil;
import com.ybear.ybutils.utils.notification.NotificationX;
import com.ybear.ybutils.utils.toast.Build;
import com.ybear.ybutils.utils.toast.ToastManage;

import java.nio.charset.Charset;
import java.util.Locale;

public class TestApplication extends Application {
    public static final int LIVE_TIME_ID_TEXT_1_SECOND = 200;
    public static final int LIVE_TIME_ID_TEXT_5_SECOND = 201;

    @Override
    public void onCreate() {
        super.onCreate();
        //日志
        initLog();

        //Shape控件
//        ShapeConfig.get().enableHardware();
        ShapeConfig.get().setLocale( Locale.getDefault() );
        //启用Shape控件支持RTL布局
        ShapeConfig.get().enableSupportRtlLayout();

        //通知栏
        NotificationX.get().init( this );

        //网络
        initNetwork();

        //心跳
        initLiveTime();

        ToastManage.get()
                //初始化Toast生命周期
                .initLifecycle( this )
                //初始化Toast样式
                .setBuild( new Build()
                        //圆角
                .setRadius(
                        Utils.dp2Px( this, 12 ), 0,
                        0, Utils.dp2Px( this, 12 )
                )
                        //背景
                .setBackgroundResource( R.color.colorAccent )
                        //阴影色
                .setShadowColor( Color.BLACK )
                        //阴影X距离
                .setShadowOffsetX( Utils.dp2Px( this, 12 ) )
                        //阴影Y距离
                .setShadowOffsetY( Utils.dp2Px( this, 12 ) )
                        //文本色
                .setTextColor( Color.WHITE )
        );

        //初始化DialogQueue生命周期
        Dialog.initLifecycle( this );

        //初始化彩带全局配置
        EnterRibbonConfig.get()
                //启用彩带
                .setEnableRibbon( true )
                //启用关闭彩带期间依旧添加（保证启用后关闭期间的彩带也能恢复）
                .setKeepAddQueueByDisabledRibbon( true );
    }

    private void initLog() {
        //自动配置是否打印日志（打包时，正式环境（release）关闭日志）
        LogUtil.setDebugEnableOfAuto( this );
        //手动配置是否打印日志（任何环境）
        LogUtil.setDebugEnable( this, true );
        //半自动配置是否打印日志（当isAuto为true时，无视enable）
        LogUtil.setDebugEnable( this, true, true );
        //保存日志的路径
        LogUtil.setLogSavePath( this, true );
        //启用/禁用回调日志
        LogUtil.setCallLogEnable( true );
        //全局Tag标签（LogUtil.v( "hello!" ); 不设置Tag时使用全局Tag）
        LogUtil.setTagOfGlobal( "TestTAG" );
        //生成新日志文件的间隔（秒）
        LogUtil.setNewLogFileIntervalSecond( 120 );
        //设置保存日志的类型文本（保存日志时会携带该文本）
        LogUtil.setLogSaveTypeText( new String[] { "V", "D", "I", "W", "E" } );
        //保存日志时的编码
        LogUtil.setLogSaveCharset( Charset.defaultCharset() );
        //回调日志接口
        LogUtil.setOnCallLogListener( data -> {
            /* 可选设置 */
//            //可以在回调时决定是否保存当前日志（这是一个全局性的决定，等同于LogUtil.setLogSavePath( *, true );）
//            data.setEnableSave( true );
//            /* 重新设置tag和message */
//            data.setTag( data.getTag() );
//            data.setMessage( data.getMessage() );
            /* 自定义保存的日志，可以是完全加密后的日志。CustomMessage不为空时，只保存CustomMessage的内容 */
            String custom = new String( Base64.encode(
                    data.getMessage().getBytes( data.getCharset() ),
                    Base64.DEFAULT
            ), data.getCharset() );
            data.setCustomMessage( custom );
            Log.e( "LOG_TAG", custom );
            return data;
        } );
        /* 打印日志 */
        LogUtil.v( "TAG", "自定义TAG：文本" );
        LogUtil.v( this, "自定义TAG：this" );
        LogUtil.v( getClass(), "自定义TAG：getClass()" );

        LogUtil.v( "TAG", "%s%s", "自定义TAG：", "文本，带参数" );
        LogUtil.v( this, "%s%s", "自定义TAG：", "this，带参数" );
        LogUtil.v( getClass(), "%s%s", "自定义TAG：", "getClass()，带参数" );

        try {
            LogUtil.v( this, new NullPointerException("自定义TAG：this，Throwable空指针") );
            LogUtil.v( getClass(), new NullPointerException("自定义TAG：getClass()，Throwable空指针") );
            LogUtil.v( new NullPointerException("全局TAG：Throwable空指针") );
        }catch(Exception e) {
            e.printStackTrace();
            LogUtil.v( e );
        }

        LogUtil.v( "全局TAG" );
        LogUtil.v( "%s%s", new String[] { "全局TAG：","带参数" } );

        LogUtil.w( getClass(), "生成新日志文件的间隔：%s", LogUtil.getNewLogFileIntervalSecond() );
        LogUtil.w( getClass(), "保存日志路径：%s", LogUtil.getLogSavePath() );
        //v,d,i,e...
        //json log
        //是否跳过实体类输出为json
        LogUtil.setSkipLogPrintJson( true );
        //添加指定无法实现 ILogPrintJson 接口的类，使其允许输出为json
//        LogUtil.addLogPrintJsonClass( TestEntity.class );
        //test class
        TestEntity test = new TestEntity(                   //implements ILogPrintJson
                "LogPrintJson 1",                           //text
                new TestEntity(  "LogPrintJson 2" ) //TestEntity
        );
        //第一种输出json的办法（不会受限于 setSkipLogPrintJson）
        Log.d( "TAG", "LogPrintJson 1 -> " + test.toJSONString() );
        //第二种输出json的办法（不会受限于 setSkipLogPrintJson）
        LogUtil.d( "TAG", "LogPrintJson 2 -> %s", test.toJSONString() );
        //第三种输出json的办法（受限于 setSkipLogPrintJson）
        LogUtil.d( "TAG", "LogPrintJson 3 -> %s", test );
        /*
         * 输出的日志
         * >>> LogPrintJson 1 -> {"content":"LogPrintJson 1","testEntity":{"content":"LogPrintJson 2"}}
         * >>> LogPrintJson 2 -> {"content":"LogPrintJson 1","testEntity":{"content":"LogPrintJson 2"}}
         * >>> LogPrintJson 3 -> TestEntity{content='LogPrintJson 1', testEntity=TestEntity{content='LogPrintJson 2', testEntity=null}}
         * */
    }

    private void initNetwork() {
        NetworkConfig config = NetworkConfig.get();
        //不展示通知
        config.setEnableForegroundNotification( false );
        //通知标题
        config.setForegroundTitle( "网络服务运行中" );
        //通知内容
        config.setForegroundContent( "正在运行中" );
        //通知小图标      默认展示图标：R.drawable.ic_def_notify_small_icon
        config.setSmallIcon( R.drawable.ic_launcher_foreground );
        //通知大图标
        config.setLargeIcon( R.mipmap.ic_launcher );
        //通知渠道id
        config.setNotificationChannelId( "10000" );
        //通知渠道名
        config.setNotificationChannelName("Network channel");
        //通知等级
        config.setNotificationImportance( Importance.MIN );

        /* 网络状态监听 */
        NetworkChangeManage ncm = NetworkChangeManage.get();
        ncm.init( this );
        ncm.registerNetworkChange((isAvailable, type) ->
                LogUtil.e("onNetworkChange -> isAvailable:" + isAvailable + " | type:" + type)
        );
    }

    private void initLiveTime() {
        LiveTime liveTime = LiveTime.get();
        liveTime.addOnLiveTimeListener(new LiveTime.OnLiveTimeListener() {
            @Override
            public void onCreate(int id) {
                LogUtil.d( "LiveTime.Listener -> onCreate -> id:" + id );
            }

            @Override
            public boolean onLiveTime(int id) {
                switch ( id ) {
                    case LIVE_TIME_ID_TEXT_1_SECOND:
                        LogUtil.d( "LiveTime.Listener -> onLiveTime -> LIVE_TIME_ID_TEXT_1_SECOND" );
                        break;
                    case LIVE_TIME_ID_TEXT_5_SECOND:
                        LogUtil.d( "LiveTime.Listener -> onLiveTime -> LIVE_TIME_ID_TEXT_5_SECOND" );
                        break;
                }
                return false;
            }

            @Override
            public void onStop(int id) {
                LogUtil.d( "LiveTime.Listener -> onStop -> id:" + id );
            }

            @Override
            public void onDestroy(int id) {
                LogUtil.d( "LiveTime.Listener -> onDestroy -> id:" + id );
            }
        });

        /* LIVE_TIME_ID_TEXT_1_SECOND */
        /* 心跳间隔 LIVE_TIME_ID_TEXT_1_SECOND 每1秒执行一次 */
        liveTime.updateLiveTimeIntervalSecond( LIVE_TIME_ID_TEXT_1_SECOND, 1 );
        /* LIVE_TIME_ID_TEXT_1_SECOND 存在5秒后退出 */
        liveTime.updateLiveTimeSecond( LIVE_TIME_ID_TEXT_1_SECOND, 5 );

        /* LIVE_TIME_ID_TEXT_5_SECOND */
        /* 心跳间隔 LIVE_TIME_ID_TEXT_5_SECOND 每3秒执行一次 */
        liveTime.updateLiveTimeIntervalSecond( LIVE_TIME_ID_TEXT_5_SECOND, 3 );
        /* LIVE_TIME_ID_TEXT_5_SECOND 一直存活（默认为：0） */
//        liveTime.updateLiveTimeSecond( LIVE_TIME_ID_TEXT_5_SECOND, 0 );
    }
}
