package com.ybear.ybutilapp;

import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.pager.BaseRecyclerPagerAdapter;

import java.util.List;

public class TestRvPagerAdapter extends BaseRecyclerPagerAdapter
        <TestRvPagerAdapter.ItemData, TestRvPagerAdapter.TestHolder>{

    public TestRvPagerAdapter(List<ItemData> list) { super(list); }

    @NonNull
    @Override
    public TestHolder onCreatePagerHolder(@NonNull ViewGroup container, int viewType) {
        return new TestHolder(container, R.layout.item_test_pager);
    }

    @Override
    public void onBindPagerHolder(@NonNull TestHolder holder, int position) {
        ItemData data = getItemData( position );
        //设置文本同时高亮指定文本, -1为无限制高亮
        setTextAndHighlightByForeAndBack(
                holder.getTitle(), data.getTitle(), "测试高亮", Color.YELLOW, Color.BLACK, 2
        );
    }

    public static class ItemData implements IItemData {
        private String title;

        public String getTitle() { return title; }
        public ItemData setTitle(String title) {
            this.title = title;
            return this;
        }
    }

    public static class TestHolder extends BaseRecyclerPagerAdapter.PagerHolder {
        private final TextView tvTitle;

        public TestHolder(@NonNull ViewGroup container, int layout) {
            super(container, layout);
            tvTitle = getItemView().findViewById(R.id.item_test_pager_tv_title);
        }

//        public TestHolder(View itemView) {
//            super(itemView);
//            tvTitle = itemView.findViewById(R.id.item_test_pager_tv_title);
//        }

        public TextView getTitle() { return tvTitle; }
    }
}
