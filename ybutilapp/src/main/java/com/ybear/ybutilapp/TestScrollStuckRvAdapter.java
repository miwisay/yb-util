package com.ybear.ybutilapp;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseScrollStuckRecyclerViewAdapter;
import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.widget.MaskImageView;

public class TestScrollStuckRvAdapter extends BaseScrollStuckRecyclerViewAdapter<IItemData, BaseViewHolder> {

    public TestScrollStuckRvAdapter(@Nullable RecyclerView rv) {
        super(rv);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BaseViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_test_list, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, IItemData data, int pos) {
        MaskImageView miv = (MaskImageView) holder.itemView;
        miv.setImageResource( R.mipmap.ic_launcher, 100, 100 );
        miv.setForegroundMask( R.mipmap.ic_launcher );
        miv.setBackgroundMask( R.mipmap.ic_launcher );
        miv.setBackgroundColor( Color.BLACK );
        miv.setStartOfProgress( 0 );
        miv.setRepeatCount( 0 );
        miv.setDuration( 6000 );
        if( miv.isPauseAnim() ) {
            miv.resumeAnim();
        }else {
            miv.startAnimOfProgress();
        }
        Log.e( "TAG", "RibbonViewHolder -> bind -> " + pos );
    }

    @Override
    public void onUnBindViewHolder(BaseViewHolder holder, IItemData data, int pos) {
        MaskImageView miv = (MaskImageView) holder.itemView;
        miv.pauseAnim();
        Log.e( "TAG", "RibbonViewHolder -> unBind -> " + pos );
    }
}
