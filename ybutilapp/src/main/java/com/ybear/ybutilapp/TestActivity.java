package com.ybear.ybutilapp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybutils.utils.toast.ToastManage;

import java.util.ArrayList;
import java.util.List;

public class TestActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
//        StartUtils.startWhatsApp( this, "123456" );
        setContentView( R.layout.activity_test );
//        initToast();
//        initRecyclerView();
        initRecyclerViewOfScrollStuck();
    }

    private void initToast() {
        for (int i = 0; i < 50; i++) {
            ToastManage.get().showToast( this, "toast -> " + ( i + 1 ) );
        }
    }

//    private void initRecyclerView() {
//        RecyclerView rv = findViewById(R.id.rv_list);
//        LinearLayoutManager llm = new LinearLayoutManager( this );
////        llm.setRecycleChildrenOnDetach( true );
//        rv.setLayoutManager( llm );
//        List<TestRvAdapter.TestData> list = new ArrayList<>();
//        for (int i = 0; i < 50; i++) {
//            list.add( new TestRvAdapter.TestData().setName( "测试 " + i ) );
//        }
//        TestRvAdapter mAdapter = new TestRvAdapter( list );
////        mAdapter.setEnableMultiSelect( false );
//        mAdapter.setReplaceLastSelect( true );
//        mAdapter.setMaxMultiSelectCount( 5 );
//        mAdapter.setMinMultiSelectCount( 2 );
////        mAdapter.setItemClickType( ItemClickType.SWITCH );
//
////        new Handler().postDelayed(new Runnable() {
////            @Override
////            public void run() {
////                mAdapter.setMultiSelectStatusOfFirst( 4, true );
////            }
////        }, 6000);
//
//
//        rv.setAdapter( mAdapter );
//
//        Button btnSelectAll = findViewById(R.id.btn_select_all);
//        btnSelectAll.setOnClickListener(v -> mAdapter.switchMultiSelectStatusAll());
//
//        Button btnFinish = findViewById(R.id.btn_finish);
//        btnFinish.setOnClickListener(v -> finish());
//
//        mAdapter.setOnMultiSelectChangeListener(new OnMultiSelectChangeAdapter
//                <TestRvAdapter.TestHolder>() {
//            @Override
//            public void onMultiSelectAll(RecyclerView.Adapter<TestRvAdapter.TestHolder> adapter,
//                                         boolean isSelectAll, boolean fromUser) {
//                btnSelectAll.setText( isSelectAll ? "反选" : "全选" );
//            }
//        });
//
//        //点击事件
//        mAdapter.setOnItemClickListener((adapter, v, data, position) -> {
//            LogUtil.e("TAG", "Click -> " + position);
//            ToastManage.get().showToast(v.getContext(), "点击事件：" + position);
//        });
//
//        //长按事件
//        mAdapter.setOnItemLongClickListener((adapter1, v, data, position) -> {
//            LogUtil.e("TAG", "LongClick -> " + position);
//            ToastManage.get().showToast(v.getContext(), "长按事件：" + position);
//            return false;
//        });
//    }

    private void initRecyclerViewOfScrollStuck() {
        RecyclerView rv = findViewById(R.id.rv_list);
        LinearLayoutManager llm = new LinearLayoutManager( this );
        rv.setLayoutManager( llm );
        List<IItemData> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add( null );
        }
        TestScrollStuckRvAdapter mAdapter = new TestScrollStuckRvAdapter( rv );
        mAdapter.addItemData( list );
        rv.setAdapter( mAdapter );
    }
}
