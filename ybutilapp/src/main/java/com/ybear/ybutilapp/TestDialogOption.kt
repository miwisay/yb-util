package com.ybear.ybutilapp

import android.content.Context
import android.content.DialogInterface
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ybear.ybcomponent.Utils
import com.ybear.ybcomponent.base.adapter.IItemData
import com.ybear.ybcomponent.widget.dialog.Dialog
import com.ybear.ybcomponent.widget.dialog.DialogOption
import com.ybear.ybutils.utils.toast.ToastManage

class TestDialogOption(context: Context) : DialogOption() {
    init {
        val dialog = Dialog.with( context )
            //透明背景
            .transparentBackground()
//            //透明蒙层
//            .transparentDimAmount()
            //默认蒙层
            .defaultDimAmount()
            .animOfBottomTranslate()
            .setCornerRadiusTop( Utils.dp2Px( context, 12 ) )
//            .setTitle("TestDialogOption title")
//            .setMessage( "TestDialogOption content" )
//            .setOnPositiveButtonListener { dialog: DialogInterface, which: Int ->
//                dialog.dismiss()
//            }.setOnNegativeButtonListener(
//                "关闭对话框"
//            ) { dialog: DialogInterface, _: Int ->
//                dialog.cancel()
//                ToastManage.get().showToast(context, "关闭了对话框")
//            }
//        //定义圆角
//        dialog.setCornerRadius( Utils.dp2Px( context, 12 ) )
//        dialog.setCornerRadiusTop(...)
//        dialog.setCornerRadiusBottom(...)
        // 这个必须设置，否则不会展示Dialog
        dialog.setDialogOption( this )
        dialog.createOfFree( R.layout.dialog_test_last, ViewGroup.LayoutParams.MATCH_PARENT, Utils.dp2Px( context, 500 ) )

        setCancelable( true )
        setCanceledOnTouchOutside( true )

        findViewById<TextView>( R.id.tv_content )?.apply {
            text = "TestDialogOption"
        }
        findViewById<RecyclerView>(R.id.rv_list)?.apply {
            val llm = LinearLayoutManager( context )
            setLayoutManager( llm )
            val list: MutableList<IItemData?> = ArrayList()
            for (i in 0..49) {
                list.add(null)
            }
            val mAdapter = TestScrollStuckRvAdapter( this )
            mAdapter.addItemData(list)
            setAdapter( mAdapter )
        }

        findViewById<Button>( R.id.btn_changed_height )?.setOnClickListener {
            updateDialogHeight( Utils.dp2Px( context, 200 ), true )
        }
        findViewById<Button>( R.id.btn_reset_size )?.setOnClickListener {
            resetDialogSize( true )
        }

        /* 监听器 */
//        setOnShowListener(...)
//        setOnDismissListener(...)
//        setOnCancelListener(...)
//        setOnKeyListener(...)

        /* 常用方法 */
        //Size 转换
//        dp2Px( 10 )
//        dp2Px( 10F )

        //资源id转换
//        getString( R.string.app_name )
//        getColor( R.color.colorWhite )

        //View 转 Bitmap
//        drawToBitmap( ImageView( context ) )

        //创建一个Popup弹窗
//        createPopupWindow( ImageView( context ) )

        //点击的缩放效果
//        ImageView( context ).setOnTouchListener(object : OnTouchListener {
//            override fun onTouch(v: View?, ev: MotionEvent?): Boolean {
//                //点击的缩放效果
//                onTouchScaleAnimation( v, ev )
//                return false
//            }
//        })
    }
}