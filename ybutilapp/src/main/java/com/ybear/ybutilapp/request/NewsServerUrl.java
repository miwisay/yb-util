package com.ybear.ybutilapp.request;

import com.ybear.ybutilapp.util.Constant;
import com.ybear.ybnetworkutil.request.Request;

public abstract class NewsServerUrl extends Request {
    @Override
    public String url() {
        return Constant.BASE_URL + "news";
    }
}
