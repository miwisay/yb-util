package com.ybear.ybutilapp;

import androidx.annotation.NonNull;

import com.ybear.ybutils.utils.log.ILogPrintJson;

public class TestEntity implements ILogPrintJson {
    private String content;
    private TestEntity testEntity;

    public TestEntity() {}

    public TestEntity(String content) {
        this.content = content;
    }

    public TestEntity(String content, TestEntity testEntity) {
        this.content = content;
        this.testEntity = testEntity;
    }

    @NonNull
    @Override
    public String toString() {
        return "TestEntity{" +
                "content='" + content + '\'' +
                ", testEntity=" + testEntity +
                '}';
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TestEntity getTestEntity() {
        return testEntity;
    }

    public void setTestEntity(TestEntity testEntity) {
        this.testEntity = testEntity;
    }
}
