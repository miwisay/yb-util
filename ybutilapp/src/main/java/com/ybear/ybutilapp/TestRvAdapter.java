//package com.ybear.ybutilapp;
//
//import android.graphics.Color;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.ybear.ybcomponent.base.adapter.BaseSwipeMultiSelectAdapter;
//import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
//import com.ybear.ybcomponent.base.adapter.IItemData;
//import com.ybear.ybcomponent.widget.ItemSwipeLayout;
//
//import java.util.List;
//
//public class TestRvAdapter extends BaseSwipeMultiSelectAdapter<TestRvAdapter.TestData,
//        TestRvAdapter.TestHolder> {
//    @Override
//    public TestData getItemData(int position) {
//        return super.getItemData(position);
//    }
//
//    public TestRvAdapter(@NonNull List<TestData> mDataList) {
//        super(mDataList, true);
//    }
//
//    @NonNull
//    @Override
//    public TestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new TestHolder(LayoutInflater
//                .from(parent.getContext())
//                .inflate(R.layout.item_test_list, parent, false)
//        );
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull TestHolder h, int position, boolean status) {
//        TestRvAdapter.TestData data = getItemData( position );
//        if( data == null ) return;
//        h.tvName.setText( data.name );
//        ItemSwipeLayout isLayout = onCreateItemSwipeLayout( h, position );
//        //侧滑布局, 建议在布局中配置，而不是在绑定的时候New
//        for (int i = 0; i < 3; i++) {
//            TextView tv = new TextView( h.getContext() );
//            tv.setText( new StringBuilder().append( "测试" ).append( i ) );
//            tv.setTextColor( Color.WHITE );
//            tv.setBackgroundColor( Color.rgb(
//                    (int) (Math.random() * 255F),
//                    (int) (Math.random() * 255F),
//                    (int) (Math.random() * 255F)
//            ));
//            tv.setGravity( Gravity.CENTER );
//            isLayout.setSwipeViews( tv );
//        }
//        //测试隐藏功能
//        if( position % 2 == 0 ) {
////            h.hideItemView( false );
//            h.hideItemView( true );
//        }
//    }
//
//    @Override
//    public boolean onInitMultiSelect(int position) {
//        Log.e("TestRvAdapter", "onInitMultiSelect -> " + position);
//        return true;
//    }
//
//    @Override
//    public void onMultiSelectChange(RecyclerView.Adapter<TestHolder> adapter,
//                                    @Nullable TestHolder h,
//                                    int position,
//                                    boolean isChecked,
//                                    boolean fromUser) {
//        if( h == null ) return;
//        Log.e("TestRvAdapter", "onMultiSelectChange -> isChecked -> " + isChecked);
//        h.tvName.setBackgroundColor( isChecked ? Color.YELLOW : Color.BLUE );
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
//    }
//
//    @NonNull
//    @Override
//    public ItemSwipeLayout onCreateItemSwipeLayout(@NonNull TestHolder h, int position) {
//        return h.islItemSwipeLayout;
//    }
//
//    public static class TestHolder extends BaseViewHolder {
//        private final ItemSwipeLayout islItemSwipeLayout;
//        private final TextView tvName;
//
//        public TestHolder(@NonNull View itemView) {
//            super(itemView);
//            islItemSwipeLayout = itemView.findViewById(R.id.item_test_list_swipe_layout);
//            tvName = itemView.findViewById(R.id.item_test_list_tv_name);
//        }
//    }
//
//    public static class TestData implements IItemData {
//        private String name;
//
//        public TestData setName(String name) {
//            this.name = name;
//            return this;
//        }
//    }
//}
