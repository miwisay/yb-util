package com.ybear.ybnetworkutil.annotations;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 网络类型
 */
@StringDef({ NetworkType.UNKNOWN, NetworkType.NONE, NetworkType.WIFI, NetworkType.MOBILE })
@Retention(RetentionPolicy.SOURCE)
public @interface NetworkType {
    String UNKNOWN = "UNKNOWN"; //未知网络
    String NONE = "NONE";       //无网络
    String WIFI = "WIFI";       //无线网络
    String MOBILE = "MOBILE";   //移动网络
}
