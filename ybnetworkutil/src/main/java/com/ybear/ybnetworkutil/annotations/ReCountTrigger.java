package com.ybear.ybnetworkutil.annotations;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 重试触发条件
 * 触发条件允许多选，当存在多选时满足一项即可
 */
@StringDef({
        ReCountTrigger.NETWORK_AVAILABLE,
        ReCountTrigger.NETWORK_SWITCH,
        ReCountTrigger.EXIST_RESPONSE
})
@Retention(RetentionPolicy.SOURCE)
public @interface ReCountTrigger {
    String NETWORK_AVAILABLE = "NETWORK_AVAILABLE";     //网络可用时触发
    String NETWORK_SWITCH = "NETWORK_SWITCH";           //网络切换时触发
    String EXIST_RESPONSE = "EXIST_RESPONSE";           //存在发起请求并且成功响应时触发
}