package com.ybear.ybnetworkutil.annotations;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 网络触发器类型枚举
 */
@StringDef({
        TriggerNetworkType.NONE,
        TriggerNetworkType.ALL,
        TriggerNetworkType.AVAILABLE,
        TriggerNetworkType.SWITCH,
        TriggerNetworkType.EXIST_RESPONSE
})
@Retention(RetentionPolicy.SOURCE)
public @interface TriggerNetworkType {
    String NONE = "NONE";                       //不设置类型
    String ALL = "ALL";                         //所有类型
    String AVAILABLE = "AVAILABLE";             //网络正常时
    String SWITCH = "SWITCH";                   //切换网络时
    String EXIST_RESPONSE = "EXIST_RESPONSE";   //存在请求成功时
}
