package com.ybear.ybnetworkutil.request;

public interface IRequest {
    String url();
    String api();
}
