package com.ybear.ybnetworkutil.request;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({
        Method.GET, Method.POST, Method.PUT,
        Method.DELETE, Method.PATCH, Method.HEAD, Method.DOWNLOAD
})
@Retention(RetentionPolicy.SOURCE)
public @interface Method {
    String GET = "GET";
    String POST = "POST";
    String PUT = "PUT";
    String DELETE = "DELETE";
    String PATCH = "PATCH";
    String HEAD = "HEAD";
    String DOWNLOAD = "DOWNLOAD";
}