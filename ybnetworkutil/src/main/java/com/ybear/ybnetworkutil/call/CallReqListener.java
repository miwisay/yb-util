package com.ybear.ybnetworkutil.call;

import androidx.annotation.NonNull;

import com.ybear.ybnetworkutil.request.Request;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public interface CallReqListener {
    void onRequest(@NonNull Request r);
    void onResult(String url, String result);
    void onFailure(@NonNull Call call, @NonNull IOException e);
    void onResponse(@NonNull Call call, @NonNull Response r) throws IOException;
}