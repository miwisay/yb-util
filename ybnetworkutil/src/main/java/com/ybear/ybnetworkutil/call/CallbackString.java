package com.ybear.ybnetworkutil.call;

import androidx.annotation.Nullable;

public interface CallbackString {
    void onRequestString(@Nullable String request, boolean isResponse);
}
