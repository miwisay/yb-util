package com.ybear.ybnetworkutil.call;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public interface Callback {
    void onFailure(@NonNull Call call, @NonNull IOException e);
    void onResponse(@NonNull Call call, @NonNull Response response) throws IOException;
}
