package com.ybear.ybnetworkutil.call;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;

import okhttp3.Call;

public interface CallDownloadListener {
    void onDownloadComplete(@NonNull Call call, @NonNull File f);
    void onDownloadFailure(@NonNull Call call, @Nullable File f, @NonNull Exception e);
    void onDownloadProgress(@NonNull Call call, int progress, long fileProgress, long fileSize);
}