package com.ybear.ybnetworkutil.call;

import com.ybear.ybnetworkutil.annotations.NetworkType;

/**
 * 网络状态监听器
 */
public interface OnNetworkChangeListener {
    void change(boolean isAvailable, @NetworkType String type);
}
