package com.ybear.ybnetworkutil.call;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;

import okhttp3.Call;

public class CallDownloadAdapter implements CallDownloadListener {
    @Override
    public void onDownloadComplete(@NonNull Call call, @NonNull File f) { }

    @Override
    public void onDownloadFailure(@NonNull Call call, @Nullable File f, @NonNull Exception e) { }

    @Override
    public void onDownloadProgress(@NonNull Call call, int progress, long fileProgress, long fileSize) { }
}
