package com.ybear.ybnetworkutil.call;

import androidx.annotation.NonNull;

import com.ybear.ybnetworkutil.request.Request;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public class CallReqAdapter implements CallReqListener {
    @Override
    public void onRequest(@NonNull Request r) { }
    @Override
    public void onResult(String url, String result) { }
    @Override
    public void onFailure(@NonNull Call call, @NonNull IOException e) { }
    @Override
    public void onResponse(@NonNull Call call, @NonNull Response r) throws IOException { }
}
