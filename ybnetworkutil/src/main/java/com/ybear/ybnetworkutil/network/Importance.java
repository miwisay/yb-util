package com.ybear.ybnetworkutil.network;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({
        Importance.MAX, Importance.HIGH, Importance.DEFAULT, Importance.LOW,
        Importance.MIN, Importance.NONE, Importance.UNSPECIFIED
})
@Retention(RetentionPolicy.SOURCE)
public @interface Importance {
    int MAX = 5;
    int HIGH = 4;
    int DEFAULT = 3;
    int LOW = 2;
    int MIN = 1;
    int NONE = 0;
    int UNSPECIFIED = -1000;
}
