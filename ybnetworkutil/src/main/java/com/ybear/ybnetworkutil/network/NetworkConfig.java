package com.ybear.ybnetworkutil.network;

import com.ybear.ybnetworkutil.R;

public class NetworkConfig {
    private boolean enableForegroundNotification = false;
    private int mForegroundId = 1;
    private String notificationChannelId = "network_foreground";
    private String notificationChannelName = "Network Foreground";
    private int notificationImportance = Importance.MIN;
    private int smallIcon = R.drawable.ic_def_notify_small_icon;
    private int largeIcon;
    private String foregroundTitle;
    private String foregroundContent;


    private NetworkConfig() {}
    public static NetworkConfig get() { return HANDLER.I; }
    private static final class HANDLER {
        private static final NetworkConfig I = new NetworkConfig();
    }

    public boolean isEnableForegroundNotification() {
        return enableForegroundNotification;
    }

    public void setEnableForegroundNotification(boolean enableForegroundNotification) {
        this.enableForegroundNotification = enableForegroundNotification;
    }

    public int getForegroundId() { return mForegroundId; }
    public NetworkConfig setForegroundId(int id) {
        mForegroundId = id;
        return this;
    }

    public String getNotificationChannelId() { return notificationChannelId; }
    public NetworkConfig setNotificationChannelId(String id) {
        notificationChannelId = id;
        return this;
    }

    public String getNotificationChannelName() { return notificationChannelName; }
    public NetworkConfig setNotificationChannelName(String name) {
        notificationChannelName = name;
        return this;
    }

    @Importance
    public int getNotificationImportance() { return notificationImportance; }
    public NetworkConfig setNotificationImportance(int importance) {
        notificationImportance = importance;
        return this;
    }

    public int getSmallIcon() { return smallIcon; }
    public NetworkConfig setSmallIcon(int icon) {
        smallIcon = icon;
        return this;
    }

    public int getLargeIcon() { return largeIcon; }
    public NetworkConfig setLargeIcon(int largeIcon) {
        this.largeIcon = largeIcon;
        return this;
    }

    public String getForegroundTitle() { return foregroundTitle; }
    public NetworkConfig setForegroundTitle(String foregroundTitle) {
        this.foregroundTitle = foregroundTitle;
        return this;
    }

    public String getForegroundContent() { return foregroundContent; }
    public NetworkConfig setForegroundContent(String foregroundContent) {
        this.foregroundContent = foregroundContent;
        return this;
    }
}
