package com.ybear.ybnetworkutil.network;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;

import com.ybear.ybnetworkutil.annotations.NetworkType;
import com.ybear.ybnetworkutil.call.OnNetworkChangeListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 网络状态管理器
 */
public class NetworkChangeManage {
    private final List<OnNetworkChangeListener> mNetworkChangeList = new ArrayList<>();
    private Application mApp;
    private Intent mNetworkIntent;
    //运行状态。0：未运行，1：启动中，2：运行中
    private int mRunningState = 0;

    private NetworkChangeManage() {}

    public static NetworkChangeManage get() { return HANDLER.I; }
    private static class HANDLER {
        private static final NetworkChangeManage I = new NetworkChangeManage();
    }

    public NetworkChangeManage init(Application app) {
        mApp = app;
        return this;
    }

    /**
     * 注册监听服务
     * 使用场景：
     *     进入常驻：Application下使用
     *     随时退出：BaseActivity 的 onCreate or onResume 下使用
     *
     *     注意：如果是后台服务，需要启用通知，否则可能会闪退。最好onPause的时候解除监听服务
     *     //启用通知
     *     {@link NetworkConfig#setEnableForegroundNotification(boolean)}
     *     //解除监听服务
     *     {@link NetworkChangeManage#unregisterService()}
     *
     * 个人比较推荐 随时退出
     */
    public void registerService(Handler handler, long delayMillis) {
        if( mApp == null ) return;
        Runnable r = () -> {
            if( isRunningService() ) return;
            mNetworkIntent = new Intent( mApp, NetworkChangeService.class );
            try {
                boolean enableNotify = NetworkConfig.get().isEnableForegroundNotification();
                if( enableNotify && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
                    mApp.startForegroundService( mNetworkIntent );
                }else {
                    mApp.startService( mNetworkIntent );
                }
                mRunningState = 1;
            } catch (Exception e) {
                e.printStackTrace();
                mRunningState = 0;
            }
        };
        if( handler == null || delayMillis <= 0 ) {
            r.run();
            return;
        }
        handler.postDelayed( r, delayMillis );
    }
    public void registerService(Handler handler) { registerService( handler, 5000 ); }
    public void registerServiceNoDelay(Handler handler) { registerService( handler, 0 ); }
    public void registerService() { registerService( null ); }

    /**
     * 解除监听服务
     * 使用场景：
     *     退出常驻：既然是常驻，那就没有退出的必要了
     *     随时退出：BaseActivity 的 onDestroy 下使用，或者在 onPause 下调用
     *             com.ybear.ybutils.utils.StackManage.isHaveExistActivityOfSkip( MainActivity )
     *             检查除了MainActivity之外，没有其他页面时，来决定当前的 pause 是否切到后台，
     *             而不是打开其他的页面也会解除注册
     */
    public void unregisterService() {
        if( mApp == null || mNetworkIntent == null ) return;
        try {
            mApp.stopService( mNetworkIntent );
        } catch (Exception e) {
            e.printStackTrace();
        }
        mRunningState = 0;
    }

    /**
     * 注册网络状态 - 注册后可以收到状态改变的结果
     * @param l    回调
     */
    public void registerNetworkChange(OnNetworkChangeListener l) {
        mNetworkChangeList.add( l );
    }

    /**
     * 解除注册网络状态
     */
    public void unregisterNetworkChange(OnNetworkChangeListener l) {
        mNetworkChangeList.remove( l );
    }

    /**
     * 解除注册全部网络状态
     */
    public void unregisterNetworkChangeAll() {
        mNetworkChangeList.clear();
    }

    public boolean isRunningService() { return mRunningState > 0; }

    /**
     运行状态
     @return    0：未运行，1：启动中，2：运行中
     */
    public int getRunningState() { return mRunningState; }

    void setRunningService(boolean runningService) { mRunningState = runningService ? 2 : 0; }

    /**
     * 回调网络状态监听
     * @param isAvailable   是否活动
     * @param type          网络类型
     */
    void callbackNetworkChange(boolean isAvailable, @NetworkType String type) {
        for( OnNetworkChangeListener l : mNetworkChangeList ) {
            if( l != null ) l.change( isAvailable, type );
        }
    }
}
