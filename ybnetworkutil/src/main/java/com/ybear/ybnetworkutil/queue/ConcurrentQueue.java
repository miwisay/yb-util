package com.ybear.ybnetworkutil.queue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybnetworkutil.request.Request;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 并发队列
 * 封装了用于高并发的 {@link ConcurrentHashMap}
 */
public final class ConcurrentQueue implements Queue {
    private Map<Long, Request> mQueueMap;

    ConcurrentQueue() { mQueueMap = new ConcurrentHashMap<>(); }

    @Override
    public int size() { return mQueueMap.size(); }

    @Override
    public boolean isEmpty() { return mQueueMap.isEmpty(); }

    @Override
    public boolean containsKey(long key) { return mQueueMap.containsKey( key ); }

    @Override
    public boolean containsValue(@Nullable Request value) {
        return mQueueMap.containsValue( value );
    }

    @Nullable
    @Override
    public Request get(long key) { return mQueueMap.get( key ); }

    @Nullable
    @Override
    public Request put(long key, @NonNull Request value) {
        return mQueueMap.put( key, value );
    }

    @Nullable
    @Override
    public Request remove(long key) { return mQueueMap.remove( key ); }

    @Override
    public void clear() { mQueueMap.clear(); }

    @NonNull
    @Override
    public Set<Long> keySet() { return mQueueMap.keySet(); }

    @NonNull
    @Override
    public Collection<Request> values() { return mQueueMap.values(); }
}