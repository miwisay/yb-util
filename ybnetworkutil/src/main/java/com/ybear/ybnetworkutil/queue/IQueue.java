package com.ybear.ybnetworkutil.queue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybnetworkutil.request.Request;

import java.util.Collection;
import java.util.Set;

public interface IQueue {
    @NonNull
    Queue getQueue();

    @NonNull
    Set<Long> keySet();

    @NonNull
    Collection<Request> values();

    @Nullable
    Request addDoQueue(@NonNull Request request);

    @NonNull
    Request removeDoQueue(@NonNull Request request);

    Request addToQueue(long key, Request val);

    Request removeToQueue(long key);
}