package com.ybear.ybnetworkutil.queue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybnetworkutil.request.Request;

import java.util.Collection;
import java.util.Set;

public interface Queue {
    int size();
    
    boolean isEmpty();

    boolean containsKey(long key);

    boolean containsValue(@Nullable Request value);

    @Nullable
    Request get(long key);

    @Nullable
    Request put(long key, @NonNull Request value);

    @Nullable
    Request remove(long key);

    void clear();

    @NonNull
    Set<Long> keySet();

    @NonNull
    Collection<Request> values();
}
