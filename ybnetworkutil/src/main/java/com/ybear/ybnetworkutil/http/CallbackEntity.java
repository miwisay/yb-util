package com.ybear.ybnetworkutil.http;

import com.ybear.ybnetworkutil.call.CallDownloadListener;
import com.ybear.ybnetworkutil.call.Callback;
import com.ybear.ybnetworkutil.call.CallbackString;

final class CallbackEntity {

    private Callback callback;
    private CallbackString callbackString;
    private CallDownloadListener callDownloadListener;

    public CallbackEntity(Callback callback) { this.callback = callback; }
    public CallbackEntity(CallbackString callback) { callbackString = callback; }
    public CallbackEntity(CallDownloadListener callback) { callDownloadListener = callback; }

    public Callback getCallback() { return callback; }
    public CallbackString getCallbackString() { return callbackString; }
    public CallDownloadListener getCallDownloadListener() { return callDownloadListener; }
}
