package com.ybear.ybnetworkutil.http;

import android.text.TextUtils;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

import okhttp3.Headers;

/**
 * 请求头
 */
public class Header {
    private Headers.Builder mBuilder;
    private Header() { mBuilder = new Headers.Builder(); }
    public static Header create() { return HANDLER.I; }
    private static final class HANDLER { private static final Header I = new Header(); }

    Headers callHeader() { return mBuilder.build(); }

    public void add(String name, String value) { mBuilder.add(name, value); }

    public void add(String name, Date value) { mBuilder.add(name, value); }

    public void add(String line) { mBuilder.add(line); }

    public void addUnsafeNonAscii(String name, String value) {
        mBuilder.addUnsafeNonAscii(name, value);
    }

    public void addAll(Header header) { mBuilder.addAll( header.callHeader() ); }

    public void addAllForObject(Map<String, Object> header) {
        for( String key : header.keySet() ) {
            Object val = header.get( key );
            mBuilder.add( key, val == null ? "" : val.toString() );
        }
    }

    public void addAll(Map<String, String> header) {
        for( String key : header.keySet() ) {
            String val = header.get( key );
            mBuilder.add( key, val == null ? "" : val );
        }
    }

    public String get(String name) { return mBuilder.get(name); }

    public void set(String name, String value) { mBuilder.set(name, value); }

    public void set(String name, Date value) { mBuilder.set(name, value); }

    public void removeAll(String name) { mBuilder.removeAll(name); }

    public void clear() { mBuilder = new Headers.Builder(); }
}
