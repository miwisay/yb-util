package com.ybear.ybnetworkutil.http;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import okhttp3.CookieJar;
import okhttp3.OkHttpClient;

/**
 * 网络请求
 */
public class HttpClient {
    @NonNull
    private final OkHttpClient.Builder mBuilder;
    private HttpReboot mHttpReboot;
    private HttpClient(@NonNull OkHttpClient.Builder builder) {
        mBuilder = builder;
    }
    public static HttpClient create() {
        return create( createOkBuilder() );
    }

    public static HttpClient create(@Nullable OkHttpClient.Builder builder) {
        boolean hasBuilder = builder != null;
        HttpClient client = new HttpClient( hasBuilder ? builder : createOkBuilder() );
        //如果传入的builder不为空，则跳过默认设置
        if( hasBuilder ) return client;
        //默认连接超时
        client.connectTimeout( 45 );
        //默认回调超时
        client.callTimeout( 30 );
        //默认读取超时
        client.readTimeout( 60 );
        //默认写入超时
        client.writeTimeout( 60 );
        return client;
    }

    /**
     * 构建Http请求
     * @return      {@link Req}
     */
    public Req build() { return Req.create( mBuilder.build(), mHttpReboot ); }

    /**
     * 创建一个OkHttp的构建器
     * @return      {@link OkHttpClient#newBuilder()}
     */
    public static OkHttpClient.Builder createOkBuilder() {
        return new OkHttpClient().newBuilder();
    }

    /**
     * 设置重连机制
     * @param reboot    重连机制
     * @return          this
     */
    public HttpClient setHttpReboot(HttpReboot reboot) {
        mHttpReboot = reboot;
        return this;
    }

    /**
     * 设置连接超时时间
     * @param timeout   超时时间
     * @param unit      时间单位
     * @return          this
     */
    public HttpClient connectTimeout(long timeout, TimeUnit unit) {
        mBuilder.connectTimeout( timeout, unit );
        return this;
    }
    public HttpClient connectTimeout(long timeout) {
        return connectTimeout( timeout, TimeUnit.SECONDS );
    }

    /**
     * 设置回调超时时间
     * @param timeout   超时时间
     * @param unit      时间单位
     * @return          this
     */
    public HttpClient callTimeout(long timeout, TimeUnit unit) {
        mBuilder.callTimeout(timeout, unit);
        return this;
    }
    public HttpClient callTimeout(long timeout) {
        return callTimeout( timeout, TimeUnit.SECONDS );
    }

    /**
     * 设置读取超时时间
     * @param timeout   超时时间
     * @param unit      时间单位
     * @return          this
     */
    public HttpClient readTimeout(long timeout, TimeUnit unit) {
        mBuilder.readTimeout(timeout, unit);
        return this;
    }
    public HttpClient readTimeout(long timeout) {
        return readTimeout( timeout, TimeUnit.SECONDS );
    }

    /**
     * 设置写入超时时间
     * @param timeout   超时时间
     * @param unit      时间单位
     * @return          this
     */
    public HttpClient writeTimeout(long timeout, TimeUnit unit) {
        mBuilder.writeTimeout(timeout, unit);
        return this;
    }
    public HttpClient writeTimeout(long timeout) {
        return writeTimeout( timeout, TimeUnit.SECONDS );
    }

    /**
     * cookie
     * @param cookieJar     回调
     * @return              this
     */
    public HttpClient cookieJar(CookieJar cookieJar) {
        mBuilder.cookieJar( cookieJar );
        return this;
    }
}
