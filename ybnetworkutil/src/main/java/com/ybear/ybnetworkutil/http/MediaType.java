package com.ybear.ybnetworkutil.http;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 提交类型
 */
public class MediaType {
    @Retention(RetentionPolicy.SOURCE)
    public @interface MultipartType {
        okhttp3.MediaType MIXED = okhttp3.MediaType.get( "multipart/mixed" );
        okhttp3.MediaType ALTERNATIVE = okhttp3.MediaType.get( "multipart/alternative" );
        okhttp3.MediaType DIGEST = okhttp3.MediaType.get( "multipart/digest" );
        okhttp3.MediaType PARALLEL = okhttp3.MediaType.get( "multipart/parallel" );
        okhttp3.MediaType FORM = okhttp3.MediaType.get( "multipart/form-data" );
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface RequestType {
        String FORM = "application/x-www-form-urlencoded";
        String JSON = "application/json";
        String JS = "application/javascript";
        String XML = "application/xml";
        String TEXT_PLAIN = "text/plain";
        String TEXT_XML = "text/xml";
        String TEXT_HTML = "text/html";
    }

    private final Builder mBuilder;

    public MediaType() { mBuilder = new Builder().setType( RequestType.FORM ); }

    public static MediaType create() {
        return new MediaType();
    }

    okhttp3.MediaType toOkMediaType() { return mBuilder.toOkMediaType(); }

    String toMediaType() { return mBuilder.toMediaType(); }

    public void form() {
        mBuilder.setType( RequestType.FORM );
    }
    public void json() {
        mBuilder.setType( RequestType.JSON );
    }
    public void js() {
        mBuilder.setType( RequestType.JS );
    }
    public void xml() {
        mBuilder.setType( RequestType.XML );
    }
    public void textPlain() {
        mBuilder.setType( RequestType.TEXT_PLAIN );
    }
    public void textXml() {
        mBuilder.setType( RequestType.TEXT_XML );
    }
    public void textHtml() {
        mBuilder.setType( RequestType.TEXT_HTML );
    }

    static class Builder {
        private String mediaType;
        Builder setType(String mediaType) {
            this.mediaType = mediaType;
            return this;
        }
        okhttp3.MediaType toOkMediaType() {
            return mediaType != null ? okhttp3.MediaType.get( mediaType ) : null;
        }
        String toMediaType() { return mediaType; }
    }
}