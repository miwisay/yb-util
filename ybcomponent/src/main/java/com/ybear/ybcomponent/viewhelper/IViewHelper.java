package com.ybear.ybcomponent.viewhelper;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;

public interface IViewHelper {
    ViewHelper setBackground(@IdRes int id, Drawable background);
    ViewHelper setBackgroundColor(@IdRes int id, @ColorInt int color);
    ViewHelper setBackgroundResource(@IdRes int id, @DrawableRes int resId);

    @RequiresApi(api = Build.VERSION_CODES.Q)
    ViewHelper setBackgroundTintBlendMode(@IdRes int id, @Nullable BlendMode blendMode);
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    ViewHelper setBackgroundTintList(@IdRes int id, @Nullable ColorStateList tint);
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    ViewHelper setBackgroundTintMode(@IdRes int id, @Nullable PorterDuff.Mode tintMode);

    ViewHelper setInvisible(@IdRes int id, boolean isVisible);
    ViewHelper setGone(@IdRes int id, boolean isVisible);

    /* TextView ****************************************************************************/

    ViewHelper setText(@IdRes int id, CharSequence text);
    ViewHelper setText(@IdRes int id, String text);
    ViewHelper setText(@IdRes int id, char[] text, int start, int len);
    ViewHelper setText(@IdRes int id, @StringRes int textResId);
    ViewHelper setText(@IdRes int id, @StringRes int textResId, TextView.BufferType type);
    ViewHelper setTextSize(@IdRes int id, float size);
    ViewHelper setTextSize(@IdRes int id, int unit, float size);
    ViewHelper setTextColor(@IdRes int id, @ColorInt int color);
    ViewHelper setTypeface(@IdRes int id, @Nullable Typeface tf);
    ViewHelper setTypeface(@IdRes int id, @Nullable Typeface tf, int style);
    ViewHelper setHint(@IdRes int id, CharSequence hint);
    ViewHelper setHint(@IdRes int id, @StringRes int resId);
    ViewHelper setHintTextColor(@IdRes int id, ColorStateList colors);
    ViewHelper setHintTextColor(@IdRes int id, @ColorInt int color);
    ViewHelper setLinksClickable(@IdRes int id, boolean whether);
    ViewHelper setLinkTextColor(@IdRes int id, ColorStateList colors);
    ViewHelper setLinkTextColor(@IdRes int id, @ColorInt int color);
    ViewHelper setAutoLinkMask(@IdRes int id, int mask);

    /* ImageView ****************************************************************************/

    ViewHelper setImageResource(@IdRes int id, @DrawableRes int resId);
    ViewHelper setImageAlpha(@IdRes int id, int alpha);
    ViewHelper setImageBitmap(@IdRes int id, Bitmap bm);
    ViewHelper setImageDrawable(@IdRes int id, @Nullable Drawable drawable);
    @RequiresApi(api = Build.VERSION_CODES.M)
    ViewHelper setImageIcon(@IdRes int id, @Nullable Icon icon);
    ViewHelper setImageLevel(@IdRes int id, int level);
    ViewHelper setImageMatrix(@IdRes int id, Matrix matrix);
    ViewHelper setImageState(@IdRes int id, int[] state, boolean merge);
    @RequiresApi(api = Build.VERSION_CODES.Q)
    ViewHelper setImageTintBlendMode(@IdRes int id, @Nullable BlendMode blendMode);
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    ViewHelper setImageTintList(@IdRes int id, @Nullable ColorStateList tint);
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    ViewHelper setImageTintMode(@IdRes int id, @Nullable PorterDuff.Mode tintMode) ;
    ViewHelper setImageURI(@IdRes int id, @Nullable Uri uri);
    ViewHelper setScaleType(@IdRes int id, ImageView.ScaleType scaleType);
    ViewHelper setScaleMatrix(@IdRes int id);
    ViewHelper setScaleFitXY(@IdRes int id);
    ViewHelper setScaleFitStart(@IdRes int id);
    ViewHelper setScaleFitCenter(@IdRes int id);
    ViewHelper setScaleFitEnd(@IdRes int id);
    ViewHelper setScaleCenter(@IdRes int id);
    ViewHelper setScaleCenterCrop(@IdRes int id);
    ViewHelper setScaleCenterInside(@IdRes int id);
    ViewHelper setColorFilter(@IdRes int id, int color, PorterDuff.Mode mode);
    ViewHelper setColorFilter(@IdRes int id, int color);
    ViewHelper setColorFilter(@IdRes int id, ColorFilter cf);

    /* ********************************************************************************/

    @Nullable
    <T extends View> T findView(@IdRes int id);
}