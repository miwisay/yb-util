package com.ybear.ybcomponent;

import android.widget.TextView;

import com.ybear.ybcomponent.highlight.ITextHighlight;
import com.ybear.ybcomponent.span.SpanType;
import com.ybear.ybcomponent.span.TextSpanHelper;

public class TextHighlight implements ITextHighlight {

    private final TextSpanHelper mTextSpanHelper = new TextSpanHelper();

    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color,
                                    int highlightCount) {
        mTextSpanHelper.setTextAndHighlight(
                tv, text, highlight, color, highlightCount, SpanType.FOREGROUND
        );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color) {
        mTextSpanHelper.setTextAndHighlight( tv, text, highlight, color, SpanType.FOREGROUND );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight) {
        mTextSpanHelper.setTextAndHighlight( tv, text, highlight, SpanType.FOREGROUND );
    }

    /**
     * 设置高亮文本背景
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight,
                                                int color, int highlightCount) {
        mTextSpanHelper.setTextAndHighlight(
                tv, text, highlight, color, highlightCount, SpanType.BACKGROUND
        );
    }

    /**
     * 设置高亮文本背景
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight, int color) {
        mTextSpanHelper.setTextAndHighlight( tv, text, highlight, color, SpanType.BACKGROUND );
    }

    /**
     * 设置高亮文本背景
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight) {
        mTextSpanHelper.setTextAndHighlight( tv, text, highlight, SpanType.BACKGROUND );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight,
                                                 int foreColor, int backColor, int highlightCount) {
        mTextSpanHelper.setTextAndHighlight(
                tv, text, highlight, new int[] { foreColor, backColor }, highlightCount, SpanType.FORE_AND_BACK
        );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight, int foreColor, int backColor) {
        mTextSpanHelper.setTextAndHighlight(
                tv, text, highlight, new int[] { foreColor, backColor }, SpanType.FORE_AND_BACK
        );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight) {
        mTextSpanHelper.setTextAndHighlight( tv, text, highlight, SpanType.FORE_AND_BACK );
    }
}
