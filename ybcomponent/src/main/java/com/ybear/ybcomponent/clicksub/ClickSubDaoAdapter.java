package com.ybear.ybcomponent.clicksub;

import android.view.View;

public class ClickSubDaoAdapter implements ClickSubDao {
    @Override
    public boolean containsClick(View v, View.OnClickListener l) {
        return false;
    }

    @Override
    public boolean containsClick(View v) {
        return false;
    }

    @Override
    public boolean containsClick(View.OnClickListener l) {
        return false;
    }

    @Override
    public void addOnClickListener(View v, View.OnClickListener l) {

    }

    @Override
    public void removeOnClickListener(View v, View.OnClickListener l) {

    }

    @Override
    public void removeOnClickListener(View v) {

    }

    @Override
    public void removeOnClickListener(View.OnClickListener l) {

    }

    @Override
    public boolean containsLongClick(View v, View.OnLongClickListener l) {
        return false;
    }

    @Override
    public boolean containsLongClick(View v) {
        return false;
    }

    @Override
    public boolean containsLongClick(View.OnLongClickListener l) {
        return false;
    }

    @Override
    public void addOnLongClickListener(View v, View.OnLongClickListener l) {

    }

    @Override
    public void removeOnLongClickListener(View v, View.OnLongClickListener l) {

    }

    @Override
    public void removeOnLongClickListener(View v) {

    }

    @Override
    public void removeOnLongClickListener(View.OnLongClickListener l) {

    }
}
