package com.ybear.ybcomponent.clicksub;

import android.view.View;

/**
 * 点击订阅接口
 */
public interface ClickSubDao {
    /**
     * 点击监听器是否存在
     * @param v     处理事件的View
     * @param l     监听器
     * @return      是否存在
     */
    boolean containsClick(View v, View.OnClickListener l);

    /**
     * 点击监听器是否存在
     * @param v     处理事件的View
     * @return      是否存在
     */
    boolean containsClick(View v);

    /**
     * 点击监听器是否存在
     * @param l     监听器
     * @return      是否存在
     */
    boolean containsClick(View.OnClickListener l);

    /**
     * 添加点击事件监听器
     * @param v     处理事件的View
     * @param l     监听器
     */
    void addOnClickListener(View v, View.OnClickListener l);

    /**
     * 移除点击事件监听器
     * @param v     处理事件的View
     * @param l     监听器
     */
    void removeOnClickListener(View v, View.OnClickListener l);

    /**
     * 移除点击事件监听器
     * @param v     处理事件的View
     */
    void removeOnClickListener(View v);

    /**
     * 移除点击事件监听器
     * @param l     监听器
     */
    void removeOnClickListener(View.OnClickListener l);

    /**
     * 长按监听器是否存在
     * @param v     处理事件的View
     * @param l     监听器
     * @return      是否存在
     */
    boolean containsLongClick(View v, View.OnLongClickListener l);

    /**
     * 长按监听器是否存在
     * @param v     处理事件的View
     * @return      是否存在
     */
    boolean containsLongClick(View v);

    /**
     * 长按监听器是否存在
     * @param l     监听器
     * @return      是否存在
     */
    boolean containsLongClick(View.OnLongClickListener l);

    /**
     * 添加长按事件监听器
     * @param v     处理事件的View
     * @param l     监听器
     */
    void addOnLongClickListener(View v, View.OnLongClickListener l);

    /**
     * 移除长按事件监听器
     * @param v     处理事件的View
     * @param l     监听器
     */
    void removeOnLongClickListener(View v, View.OnLongClickListener l);

    /**
     * 移除长按事件监听器
     * @param v     处理事件的View
     */
    void removeOnLongClickListener(View v);

    /**
     * 移除长按事件监听器
     * @param l     监听器
     */
    void removeOnLongClickListener(View.OnLongClickListener l);
}