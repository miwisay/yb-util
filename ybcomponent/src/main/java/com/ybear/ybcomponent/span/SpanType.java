package com.ybear.ybcomponent.span;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface SpanType {
    int NONE = 0;
    int FOREGROUND = 1;
    int BACKGROUND = 2;
    int FORE_AND_BACK = 3;
}