package com.ybear.ybcomponent.span;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import androidx.annotation.ColorInt;

public class TextSpanHelper implements ITextSpan {
    private void setSpan(SpannableString spanStr, Object span, int findIndex, int lastIndex) {
        spanStr.setSpan( span, findIndex, lastIndex, Spanned.SPAN_INCLUSIVE_INCLUSIVE );
    }

    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     * @param spanType          高亮类型 {@link SpanType}
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text,
                                    String highlight, @ColorInt int[] color, int highlightCount,
                                    @SpanType int spanType) {
        if( tv == null ) return;
        if( TextUtils.isEmpty( text ) || TextUtils.isEmpty( highlight ) ) {
            tv.setText( text );
            return;
        }
        SpannableString spanStr = new SpannableString( text );
        int lastIndex = 0;
        int findIndex;
        while ( highlightCount == -1 || highlightCount > 0 ) {
            findIndex = text.toLowerCase().indexOf( highlight.toLowerCase(), lastIndex );
            if( findIndex == -1 ) break;
            lastIndex = findIndex + highlight.length();
            //设置高亮区域
            switch ( spanType ) {
                case SpanType.FOREGROUND:       //字体高亮
                    setSpan( spanStr, new ForegroundColorSpan( color[ 0 ] ), findIndex, lastIndex );
                    break;
                case SpanType.BACKGROUND:       //背景高亮
                    setSpan( spanStr, new BackgroundColorSpan( color[ 0 ] ), findIndex, lastIndex );
                    break;
                case SpanType.FORE_AND_BACK:    //字体和背景同时高亮
                    setSpan( spanStr, new ForegroundColorSpan( color[ 0 ] ), findIndex, lastIndex );
                    setSpan( spanStr, new BackgroundColorSpan( color[ 1 ] ), findIndex, lastIndex );
                    break;
                default: break;
            }
            if( highlightCount != -1 ) highlightCount--;
            if( highlightCount == 0 ) break;
        }
        tv.setText( spanStr );
    }

    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     * @param spanType          高亮类型 {@link SpanType}
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color,
                                    int highlightCount, int spanType) {
        setTextAndHighlight( tv, text, highlight, new int[] { color }, highlightCount, spanType );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     * @param spanType          高亮类型 {@link SpanType}
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, @ColorInt int[] color,
                                    @SpanType int spanType) {
        setTextAndHighlight( tv, text, highlight, color, -1, spanType );
    }

    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color, int spanType) {
        setTextAndHighlight( tv, text, highlight, new int[] { color }, spanType );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param spanType          高亮类型 {@link SpanType}
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight,
                                    @SpanType int spanType) {
        setTextAndHighlight( tv, text, highlight, Color.YELLOW, spanType );
    }
}