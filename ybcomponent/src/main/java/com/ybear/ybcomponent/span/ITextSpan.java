package com.ybear.ybcomponent.span;

import android.widget.TextView;

import androidx.annotation.ColorInt;

public interface ITextSpan {
    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     * @param spanType          高亮类型 {@link SpanType}
     */
    void setTextAndHighlight(
            TextView tv, String text, String highlight, @ColorInt int[] color,
            int highlightCount, @SpanType int spanType
    );
    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     * @param spanType          高亮类型 {@link SpanType}
     */
    void setTextAndHighlight(
            TextView tv, String text, String highlight, @ColorInt int color,
            int highlightCount, @SpanType int spanType
    );

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     * @param spanType          高亮类型 {@link SpanType}
     */
    void setTextAndHighlight(
            TextView tv, String text, String highlight, @ColorInt int[] color, @SpanType int spanType
    );
    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     * @param spanType          高亮类型 {@link SpanType}
     */
    void setTextAndHighlight(
            TextView tv, String text, String highlight, @ColorInt int color, @SpanType int spanType
    );

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param spanType          高亮类型 {@link SpanType}
     */
    void setTextAndHighlight(TextView tv, String text, String highlight, @SpanType int spanType);
}
