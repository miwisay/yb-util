package com.ybear.ybcomponent;

import android.view.MotionEvent;
import android.view.View;

public interface OnTouchListener {
    boolean onTouch(View v, MotionEvent event);
}