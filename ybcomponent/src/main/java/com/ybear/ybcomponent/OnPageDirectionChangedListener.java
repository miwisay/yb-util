package com.ybear.ybcomponent;

public interface OnPageDirectionChangedListener {
    void onPageDirection(int position, float positionOffset, int positionOffsetPixels,
                         int direction);
    void onPageDirection(int position, int mOldPosition, int direction);
    void onPageDirectionChanged(int state);
}