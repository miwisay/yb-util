package com.ybear.ybcomponent.widget.ribbon;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybcomponent.base.adapter.IItemData;
/**
 动画监听器
 */
public interface OnViewAnimationListener {
    /**
     进入动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    @Nullable
    CreateQueue onEnterCreateQueue(RibbonViewHolder holder, @NonNull IItemData data,
                                   float itemOffsetX, int multiLineDirection);

    /**
     进入动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    @NonNull
    AnimationInit onEnterAnimationInit(float itemOffsetX, int multiLineDirection);

    /**
     进入动画 - 值更新
     @param val                     更新的值
     */
    void onEnterAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data, float val);

    /**
     等待动画 - 实际无动画，仅返回等待的时间
     @param waitMillisecond         等待的时间
     */
    void onWaitAnimation(RibbonViewHolder holder, @NonNull IItemData data, long waitMillisecond);

    /**
     退出动画 - 值更新
     @param val                     更新的值
     */
    void onExitAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data, float val);

    /**
     退出动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    @Nullable
    CreateQueue onExitCreateQueue(RibbonViewHolder holder, @NonNull IItemData data,
                                  float itemOffsetX, int multiLineDirection);

    /**
     退出动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    AnimationInit onExitAnimationInit(float itemOffsetX, int multiLineDirection);
}