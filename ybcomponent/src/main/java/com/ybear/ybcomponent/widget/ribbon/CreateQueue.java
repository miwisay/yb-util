package com.ybear.ybcomponent.widget.ribbon;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.Nullable;

public class CreateQueue {
    private float[] values;
    private TimeInterpolator timeInterpolator;

    private CreateQueue() {}
    private CreateQueue(TimeInterpolator timeInterpolator, float... values) {
        this.values = values;
        this.timeInterpolator = timeInterpolator;
    }

    public static CreateQueue create(@Nullable TimeInterpolator timeInterpolator,
                                     float... values) {
        return new CreateQueue( timeInterpolator, values );
    }
    public static CreateQueue create(float... values) {
        return create( new DecelerateInterpolator(), values );
    }

    public float[] getValues() { return values; }
    public void setValues(float[] values) { this.values = values; }

    public TimeInterpolator getTimeInterpolator() { return timeInterpolator; }
}