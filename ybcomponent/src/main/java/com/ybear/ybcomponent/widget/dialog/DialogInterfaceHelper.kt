package com.ybear.ybcomponent.widget.dialog

import android.app.ActionBar
import android.os.Bundle
import android.view.ActionMode
import android.view.ContextMenu
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.SearchEvent
import android.view.View
import android.view.WindowManager
import android.view.accessibility.AccessibilityEvent

/**
 * Dialog 接口，定义了 Dialog 的各种回调方法。
 */
interface DialogInterfaceHelper {

    /**
     *  分发触摸事件。
     *  @param ev 触摸事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchTouchEvent(ev: MotionEvent): Boolean

    /**
     *  分发按键事件。
     *  @param event 按键事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchKeyEvent(event: KeyEvent): Boolean

    /**
     *  分发按键快捷键事件。
     *  @param event 按键快捷键事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchKeyShortcutEvent(event: KeyEvent): Boolean

    /**
     *  分发轨迹球事件。
     *  @param ev 轨迹球事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchTrackballEvent(ev: MotionEvent): Boolean

    /**
     *  分发通用运动事件。
     *  @param ev 通用运动事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchGenericMotionEvent(ev: MotionEvent): Boolean

    /**
     *  分发填充辅助功能事件。
     *  @param event 辅助功能事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun dispatchPopulateAccessibilityEvent(event: AccessibilityEvent): Boolean

    /**
     *  创建面板视图。
     *  @param featureId 面板的特征 ID。
     *  @return 面板视图，或者 null。
     */
    fun onCreatePanelView(featureId: Int): View?

    /**
     *  创建面板菜单。
     *  @param featureId 面板的特征 ID。
     *  @param menu 菜单。
     *  @return 如果菜单被创建，则返回 true，否则返回 false。
     */
    fun onCreatePanelMenu(featureId: Int, menu: Menu): Boolean

    /**
     *  准备面板。
     *  @param featureId 面板的特征 ID。
     *  @param view 面板视图。
     *  @param menu 菜单。
     *  @return 如果面板被准备，则返回 true，否则返回 false。
     */
    fun onPreparePanel(featureId: Int, view: View?, menu: Menu): Boolean

    /**
     *  菜单打开时调用。
     *  @param featureId 菜单的特征 ID。
     *  @param menu 菜单。
     *  @return 如果菜单被处理，则返回 true，否则返回 false。
     */
    fun onMenuOpened(featureId: Int, menu: Menu): Boolean

    /**
     *  菜单项被选中时调用。
     *  @param featureId 菜单的特征 ID。
     *  @param item 被选中的菜单项。
     *  @return 如果菜单项被处理，则返回 true，否则返回 false。
     */
    fun onMenuItemSelected(featureId: Int, item: MenuItem): Boolean

    /**
     *  窗口属性改变时调用。
     *  @param params 新的窗口属性。
     */
    fun onWindowAttributesChanged(params: WindowManager.LayoutParams?)

    /**
     *  内容改变时调用。
     */
    fun onContentChanged()

    /**
     *  窗口焦点改变时调用。
     *  @param hasFocus 如果窗口获得焦点，则为 true，否则为 false。
     */
    fun onWindowFocusChanged(hasFocus: Boolean)

    /**
     *  附加到窗口时调用。
     */
    fun onAttachedToWindow()

    /**
     *  从窗口分离时调用。
     */
    fun onDetachedFromWindow()

    /**
     *  面板关闭时调用。
     *  @param featureId 面板的特征 ID。
     *  @param menu 菜单。
     */
    fun onPanelClosed(featureId: Int, menu: Menu)

    /**
     *  请求搜索时调用。
     *  @param searchEvent 搜索事件。
     *  @return 如果搜索请求被处理，则返回 true，否则返回 false。
     */
    fun onSearchRequested(searchEvent: SearchEvent): Boolean

    /**
     *  请求搜索时调用。
     *  @return 如果搜索请求被处理，则返回 true，否则返回 false。
     */
    fun onSearchRequested(): Boolean

    /**
     *  窗口开始动作模式时调用。
     *  @param callback 动作模式回调。
     *  @return 动作模式，或者 null。
     */
    fun onWindowStartingActionMode(callback: ActionMode.Callback?): ActionMode?

    /**
     *  窗口开始动作模式时调用。
     *  @param callback 动作模式回调。
     *  @param type 动作模式类型。
     *  @return 动作模式，或者 null。
     */
    fun onWindowStartingActionMode(callback: ActionMode.Callback?, type: Int): ActionMode?

    /**
     *  动作模式开始时调用。
     *  @param mode 动作模式。
     */
    fun onActionModeStarted(mode: ActionMode?)

    /**
     *  动作模式结束时调用。
     *  @param mode 动作模式。
     */
    fun onActionModeFinished(mode: ActionMode?)

    /**
     *  按键按下时调用。
     *  @param keyCode 按键代码。
     *  @param event 按键事件。
     *  @return 如果按键事件被处理，则返回 true，否则返回 false。
     */
    fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean

    /**
     *  按键长按时调用。
     *  @param keyCode 按键代码。
     *  @param event 按键事件。
     *  @return 如果按键事件被处理，则返回 true，否则返回 false。
     */
    fun onKeyLongPress(keyCode: Int, event: KeyEvent): Boolean

    /**
     *  按键抬起时调用。
     *  @param keyCode 按键代码。
     *  @param event 按键事件。
     *  @return 如果按键事件被处理，则返回 true，否则返回 false。
     */
    fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean

    /**
     *  多个按键按下时调用。
     *  @param keyCode 按键代码。
     *  @param repeatCount 重复次数。
     *  @param event 按键事件。
     *  @return 如果按键事件被处理，则返回 true，否则返回 false。
     */
    fun onKeyMultiple(keyCode: Int, repeatCount: Int, event: KeyEvent): Boolean

    /**
     *  创建上下文菜单时调用。
     *  @param menu 上下文菜单。
     *  @param v 触发上下文菜单的视图。
     *  @param menuInfo 上下文菜单信息。
     */
    fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?)

    /**
     *  获取 ActionBar。
     *  @return ActionBar，或者 null。
     */
    fun getActionBar(): ActionBar?

    /**
     *  创建 Dialog 时调用。
     *  @param savedInstanceState 保存的状态。
     */
    fun onCreate(savedInstanceState: Bundle?)

    /**
     *  Dialog 开始时调用。
     */
    fun onStart()

    /**
     *  Dialog 停止时调用。
     */
    fun onStop()

    /**
     *  保存 Dialog 状态时调用。
     *  @return 保存的状态。
     */
    fun onSaveInstanceState(outState: Bundle? = null): Bundle?

    /**
     *  恢复 Dialog 状态时调用。
     *  @param savedInstanceState 保存的状态。
     */
    fun onRestoreInstanceState(savedInstanceState: Bundle)

    /**
     *  获取当前焦点视图。
     *  @return 当前焦点视图，或者 null。
     */
    fun getCurrentFocus(): View?

    /**
     *  按下返回键时调用。
     */
    fun onBackPressed()

    /**
     *  按键快捷键按下时调用。
     *  @param keyCode 按键代码。
     *  @param event 按键事件。
     *  @return 如果按键事件被处理，则返回 true，否则返回 false。
     */
    fun onKeyShortcut(keyCode: Int, event: KeyEvent): Boolean

    /**
     *  轨迹球事件发生时调用。
     *  @param event 轨迹球事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun onTrackballEvent(event: MotionEvent): Boolean

    /**
     *  通用运动事件发生时调用。
     *  @param event 通用运动事件。
     *  @return 如果事件被处理，则返回 true，否则返回 false。
     */
    fun onGenericMotionEvent(event: MotionEvent): Boolean

    /**
     *  创建选项菜单时调用。
     *  @param menu 选项菜单。
     *  @return 如果菜单被创建，则返回 true，否则返回 false。
     */
    fun onCreateOptionsMenu(menu: Menu): Boolean

    /**
     *  准备选项菜单时调用。
     *  @param menu 选项菜单。
     *  @return 如果菜单被准备，则返回 true，否则返回 false。
     */
    fun onPrepareOptionsMenu(menu: Menu): Boolean

    /**
     *  选项菜单项被选中时调用。
     *  @param item 被选中的菜单项。
     *  @return 如果菜单项被处理，则返回 true，否则返回 false。
     */
    fun onOptionsItemSelected(item: MenuItem): Boolean

    /**
     *  选项菜单关闭时调用。
     *  @param menu 选项菜单。
     */
    fun onOptionsMenuClosed(menu: Menu)

    /**
     *  打开选项菜单。
     */
    fun openOptionsMenu()

    /**
     *  关闭选项菜单。
     */
    fun closeOptionsMenu()

    /**
     *  使选项菜单无效。
     */
    fun invalidateOptionsMenu()

    /**
     *  注册上下文菜单。
     *  @param view 要注册上下文菜单的视图。
     */
    fun registerForContextMenu(view: View)

    /**
     *  取消注册上下文菜单。
     *  @param view 要取消注册上下文菜单的视图。
     */
    fun unregisterForContextMenu(view: View)

    /**
     *  打开上下文菜单。
     *  @param view 触发上下文菜单的视图。
     */
    fun openContextMenu(view: View)

    /**
     *  上下文菜单项被选中时调用。
     *  @param item 被选中的菜单项。
     *  @return 如果菜单项被处理，则返回 true，否则返回 false。
     */
    fun onContextItemSelected(item: MenuItem): Boolean

    /**
     *  上下文菜单关闭时调用。
     *  @param menu 上下文菜单。
     */
    fun onContextMenuClosed(menu: Menu)

    /**
     *  设置是否获取按键事件。
     *  @param get 如果要获取按键事件，则为 true，否则为 false。
     */
    fun takeKeyEvents(get: Boolean)

    /**
     *  获取 LayoutInflater。
     *  @return LayoutInflater。
     */
    fun getLayoutInflater(): LayoutInflater?
}