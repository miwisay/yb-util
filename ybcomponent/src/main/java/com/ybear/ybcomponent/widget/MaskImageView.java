package com.ybear.ybcomponent.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.ybear.ybcomponent.widget.shape.helper.ShapeConfig;
import com.ybear.ybcomponent.widget.shape.ShapeImageView;

/**
 * 遮罩图片控件
 */
public class MaskImageView extends AppCompatImageView implements ValueAnimator.AnimatorUpdateListener {
    private Canvas mMaskCanvas;
    private Canvas mImageCanvas;
    private Paint mPaint;
    private RectF mRectArc;
    private PorterDuffXfermode mXfermode;

    private Bitmap mBackgroundMaskWall;
    private Bitmap mForegroundMaskBitmap;
    private Bitmap mBackgroundMaskBitmap;
    private Bitmap mImageBitmap;
    private Rect mRectSrc, mRectDst;
    private ValueAnimator mValueAnim;

    private ColorFilter mImageColorFilter;
    private ColorFilter mBackgroundMaskColorFilter;
    private ColorFilter mForegroundMaskColorFilter;

    private int mStartAngle = 270;
    private int mSweepAngle = 360;
    private int mRepeatCount = 0;
    private long mDuration = 3000;
    private boolean isBackRunMask = false;
    private boolean isReverseSweep = false;
    private boolean isPause = false;

    private int mForegroundMaskWidth = -1;
    private int mForegroundMaskHeight = -1;
    private int mBackgroundMaskWidth = -1;
    private int mBackgroundMaskHeight = -1;
    private int mImageWidth = -1;
    private int mImageHeight = -1;

    public MaskImageView(Context context) {
        this(context, null);
    }

    public MaskImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaskImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mMaskCanvas = new Canvas();
        mImageCanvas = new Canvas();

        mPaint = new Paint();
        mRectArc = new RectF();
        mXfermode = new PorterDuffXfermode( PorterDuff.Mode.DST_OUT );//DST_OUT

        mRectSrc = new Rect();
        mRectDst = new Rect();

        mValueAnim = new ValueAnimator();
        //动画事件监听器
        mValueAnim.addUpdateListener( this );
        //启用硬件加速。设计器和模拟器可能依旧会存在黑边
        int hwStatus = ShapeConfig.get().getHardwareStatus();
        if( hwStatus != -1 ) {
            setEnableLayerTypeHardware( this, hwStatus == 1 );
        }
        setFocusable( false );
    }

    /**
     遮罩动画更新事件监听器
     @param animation           动画
     */
    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        updateSweepAngle( (int) animation.getAnimatedValue() );
    }

    public void setEnableLayerTypeHardware(@NonNull View view, boolean enable) {
        int curType = getLayerType();
        if( enable ) {
            if( curType != View.LAYER_TYPE_HARDWARE ) {
                setLayerType( View.LAYER_TYPE_HARDWARE, null );
            }
            return;
        }
        if( curType != View.LAYER_TYPE_SOFTWARE ) {
            setLayerType( View.LAYER_TYPE_SOFTWARE, null );
        }
    }

    /**
     * 动画绘制
     * @param canvas        画布
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if( mImageBitmap != null ) {
            mPaint.setColorFilter( mImageColorFilter );
            drawBitmap( canvas, mImageBitmap, mImageWidth, mImageHeight );
            mPaint.setColorFilter( null );
        }
        if( mBackgroundMaskBitmap != null ) {
            mPaint.setColorFilter( mBackgroundMaskColorFilter );
            drawBitmap( canvas, mBackgroundMaskBitmap, mBackgroundMaskWidth, mBackgroundMaskHeight );
            mPaint.setColorFilter( null );
        }

        if( mForegroundMaskBitmap != null ) {
            mPaint.setColorFilter( mForegroundMaskColorFilter );
            drawBitmap( mMaskCanvas, mForegroundMaskBitmap, mForegroundMaskWidth, mForegroundMaskHeight );
            mPaint.setColorFilter( null );
        }
        //绘制遮罩
        drawArc( mMaskCanvas, mStartAngle, mSweepAngle );
        //绘制遮罩背景墙
        drawBackgroundMaskWall( canvas );
    }

    /**
     遮罩旋转的角度
     @param angle           角度
     */
    public void startTo(@IntRange(from = 0, to = 360) int angle) {
        updateSweepAngle( 360 - angle );
    }

    /**
     遮罩旋转的角度（进度动画）
     @param angle           角度
     @param duration        动画时长
     */
    public void startAnimTo(@IntRange(from = 0, to = 360) int angle, long duration) {
        if( mValueAnim == null ) return;
        isPause = false;
        angle = 360 - angle;
        mValueAnim.setIntValues(
                isBackRunMask ? angle : mSweepAngle,
                isBackRunMask ? mSweepAngle : angle
        );
        mValueAnim.setDuration( duration );
        mValueAnim.setRepeatCount( mRepeatCount );
        mValueAnim.start();
    }

    /**
     遮罩旋转的角度（进度动画）
     默认时长：{@link MaskImageView#mDuration}
     @param angle           角度
     */
    public void startAnimTo(@IntRange(from = 0, to = 360) int angle) {
        startAnimTo( angle, mDuration );
    }

    /**
     遮罩旋转的角度（进度动画）
     默认角度：360
     @param duration        动画时长
     */
    public void startAnim(long duration) { startAnimTo( 360, duration ); }

    /**
     遮罩旋转的角度（进度动画）
     默认时长：{@link MaskImageView#mDuration}
     */
    public void startAnim() { startAnim( mDuration  ); }

    /**
     遮罩旋转的角度
     @param progress        位置（0 ~ 100）
     */
    public void startProgressTo(@IntRange(from = 0, to = 100) int progress) {
        startTo( toAngle( progress ) );
    }

    /**
     遮罩旋转的位置（进度动画）
     @param progress        位置（0 ~ 100）
     @param duration        动画时长
     */
    public void startAnimOfProgressTo(@IntRange(from = 0, to = 100) int progress, long duration) {
        startAnimTo( toAngle( progress ), duration );
    }

    /**
     遮罩旋转的位置（进度动画）
     @param progress        位置（0 ~ 100）
     */
    public void startAnimOfProgressTo(@IntRange(from = 0, to = 100) int progress) {
        startAnimOfProgressTo( progress, mDuration );
    }

    /**
     遮罩旋转的位置（进度动画）
     默认位置：100
     @param duration        动画时长
     */
    public void startAnimOfProgress(long duration) {
        startAnimOfProgressTo( 100, duration );
    }

    /**
     遮罩旋转的位置（进度动画）
     默认时长：{@link MaskImageView#mDuration}
     */
    public void startAnimOfProgress() { startAnimOfProgress( mDuration ); }

    /**
     遮罩旋转的位置（进度动画）
     @param from            开始位置（0 ~ 100）
     @param to              结束位置（0 ~ 100）
     @param toDuration      结束动画时长
     */
    public void startAnimOfProgress(@IntRange(from = 0, to = 100) int from,
                                    @IntRange(from = 0, to = 100) int to,
                                    long toDuration) {
        startProgressTo( from );
        if( toDuration > 0 ) {
            startAnimOfProgressTo( to, toDuration );
        }else {
            startProgressTo( from );
        }
    }

    /**
     遮罩旋转的位置（进度动画）
     @param from            开始位置（0 ~ 100）
     @param to              结束位置（0 ~ 100）
     */
    public void startAnimOfProgress(@IntRange(from = 0, to = 100) int from,
                                    @IntRange(from = 0, to = 100) int to) {
        startAnimOfProgress( from, to, mDuration );
    }

    /**
     遮罩旋转的位置（进度动画）
     @param from            开始位置（0 ~ 100）
     @param to              结束位置（0 ~ 100）
     */
    public void startOfProgress(@IntRange(from = 0, to = 100) int from,
                                    @IntRange(from = 0, to = 100) int to) {
        startAnimOfProgress( from, to, 0 );
    }

    /**
     暂停遮罩动画
     */
    public void pauseAnim() {
        if( mValueAnim == null ) return;
        isPause = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mValueAnim.pause();
            return;
        }
        mValueAnim.cancel();
    }

    /**
     恢复遮罩动画
     */
    public void resumeAnim() {
        if( mValueAnim == null ) return;
        isPause = false;
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ) {
            mValueAnim.resume();
            return;
        }
        mValueAnim.start();
    }

    /**
     结束遮罩动画
     */
    public void endAnim() {
        isPause = false;
        if( mValueAnim != null ) mValueAnim.end();
    }

    /**
     * 动画是否在运行
     */
    public boolean isRunningAnim() {
        return mValueAnim != null && mValueAnim.isRunning();
    }

    /**
     * 动画是否被暂停
     */
    public boolean isPauseAnim() { return isPause; }

    /**
     * 设置开始进度
     * @param angle             角度
     */
    public void setStartOfAngle(int angle) { mStartAngle = angle; }

    /**
     * 设置开始进度
     * @param progress          百分比进度
     */
    public void setStartOfProgress(int progress) { setStartOfAngle( toAngle( progress ) ); }

    /**
     * 动画播放时长
     * @param duration          时长
     */
    public void setDuration(long duration) { mDuration = duration; }

    /**
     * 动画循环次数
     * @param count             次数。默认不循环
     */
    public void setRepeatCount(int count) { mRepeatCount = count; }

    /**
     * 前景遮罩图
     * @param bitmap            图片
     */
    public void setForegroundMask(Bitmap bitmap, int width, int height) {
        if( bitmap == null ) return;
        if( mForegroundMaskBitmap != null ) mForegroundMaskBitmap.recycle();
        mForegroundMaskWidth = width == -1 ? bitmap.getWidth() : width;
        mForegroundMaskHeight = height == -1 ? bitmap.getHeight() : height;
        mForegroundMaskBitmap = bitmap;
//        postInvalidate();
    }

    /**
     * 前景遮罩图
     * @param bitmap            图片
     */
    public void setForegroundMask(Bitmap bitmap) {
        setForegroundMask( bitmap, -1, -1 );
    }

    /**
     * 前景遮罩图
     * @param resId             图片资源id
     * @param width             宽度。-1时自适应
     * @param height            高度。-1时自适应
     */
    public void setForegroundMask(@DrawableRes int resId, int width, int height) {
        setForegroundMask( resIdToBitmap( resId ), width, height );
    }

    /**
     * 前景遮罩图
     * @param resId             图片资源id
     */
    public void setForegroundMask(@DrawableRes int resId) {
        setForegroundMask( resId, -1, -1 );
    }

    /**
     * 背景遮罩图
     * @param bitmap            图片
     * @param width             宽度。-1时自适应
     * @param height            高度。-1时自适应
     */
    public void setBackgroundMask(Bitmap bitmap, int width, int height) {
        if( bitmap == null ) return;
        if( mBackgroundMaskBitmap != null ) mBackgroundMaskBitmap.recycle();
        mBackgroundMaskWidth = width == -1 ? bitmap.getWidth() : width;
        mBackgroundMaskHeight = height == -1 ? bitmap.getHeight() : height;
        mBackgroundMaskBitmap = bitmap;
//        createMaskBackgroundBitmap();
////        postInvalidate();
    }

    /**
     * 背景遮罩图
     * @param bitmap            图片
     */
    public void setBackgroundMask(Bitmap bitmap) {
        setBackgroundMask( bitmap, -1, -1 );
    }

    /**
     * 背景遮罩图
     * @param resId             图片资源id
     * @param width             宽度。-1时自适应
     * @param height            高度。-1时自适应
     */
    public void setBackgroundMask(@DrawableRes int resId, int width, int height) {
        setBackgroundMask( resIdToBitmap( resId ), width, height );
    }

    /**
     * 背景遮罩图
     * @param resId             图片资源id
     */
    public void setBackgroundMask(@DrawableRes int resId) {
        setBackgroundMask( resId, -1, -1 );
    }

    public void setImageBitmap(Bitmap bm, int width, int height) {
        if( mImageBitmap != null ) mImageBitmap.recycle();
        mImageWidth = width;
        mImageHeight = height;
        mImageBitmap = bm;
        postInvalidate();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
//        super.setImageBitmap(bm);
        setImageBitmap( bm, -1, -1 );
    }

    public void setImageDrawable(@Nullable Drawable drawable, int width, int height) {
        if ( drawable == null || mImageCanvas == null ) {
            super.setImageDrawable(drawable);
            setImageBitmap( null );
            return;
        }
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(
                    drawable.getIntrinsicWidth(),
                    drawable.getIntrinsicHeight(),
                    drawable.getOpacity() != PixelFormat.OPAQUE ?
                            Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565
            );
        }catch(Exception e) {
            e.printStackTrace();
        }
        drawable.setBounds(
                0,
                0,
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight()
        );
        mImageCanvas.setBitmap( bitmap );
        drawable.draw( mImageCanvas );
        setImageBitmap( bitmap, width, height );
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        setImageDrawable( drawable, -1, -1 );
    }

    public void setImageResource(int resId, int width, int height) {
        setImageBitmap( resIdToBitmap( resId ), width, height );
    }

    @Override
    public void setImageResource(int resId) {
//        super.setImageResource(resId);
        setImageResource( resId, -1, -1 );
    }

    public void setImageColorFilter(ColorFilter cf) { mImageColorFilter = cf; }

    public void setMakeBottomColorFilter(ColorFilter cf) { mBackgroundMaskColorFilter = cf; }

    public void setMakeTopColorFilter(ColorFilter cf) { mForegroundMaskColorFilter = cf; }

    /**
     * 倒放遮罩
     * @param isBackRunMask     是否倒放
     */
    public void setBackRunMask(boolean isBackRunMask) { this.isBackRunMask = isBackRunMask; }

    /**
     * 翻转遮罩
     * @param isReverseSweep    是否翻转
     */
    public void setReverseSweep(boolean isReverseSweep) {
        this.isReverseSweep = isReverseSweep;
    }

    /**
     更新遮罩旋转的角度
     @param angle               角度
     */
    private void updateSweepAngle(@IntRange(from = 0, to = 360) int angle) {
        mSweepAngle = angle;
        postInvalidate();
    }

    /**
     * 绘制遮罩背景墙
     */
    private void drawBackgroundMaskWall(Canvas canvas) {
        if( mBackgroundMaskWall == null ) {
            try {
                mBackgroundMaskWall = Bitmap.createBitmap(
                        getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888
                );
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if( mBackgroundMaskWall == null ) return;
        mMaskCanvas.setBitmap( mBackgroundMaskWall );
        canvas.drawBitmap( mBackgroundMaskWall, 0, 0, mPaint );
        mPaint.setColorFilter( null );
    }

    /**
     * 绘制扇形
     * @param canvas            画布
     * @param start             开始位置
     * @param sweep             当前位置
     */
    private void drawArc(Canvas canvas, float start, float sweep) {
        if( sweep == 0 ) return;
        mRectArc.set( -getWidth(), -getHeight(), getWidth() * 2, getHeight() * 2 );
        mPaint.setXfermode( mXfermode );
        sweep = isReverseSweep ? sweep : -sweep;
        canvas.drawArc( mRectArc, start, sweep, true, mPaint );
        mPaint.setXfermode( null );
//        LogUtil.e( "drawArc -> " + start + " | " + sweep);
    }

    /**
     * 绘制图片
     * @param canvas            画布
     * @param bitmap            图片
     */
    private void drawBitmap(@NonNull Canvas canvas, @Nullable Bitmap bitmap,
                            int width, int height) {
        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        if( bitmap == null ) return;
        if( width == -1 ) width = w;
        if( height == -1 ) height = h;
        int l = width == -1 ? 0 : ( w - width ) / 2;
        int t = height == -1 ? 0 : ( h - height ) / 2;
        mRectSrc.set( 0, 0, bitmap.getWidth(), bitmap.getHeight() );
        mRectDst.set( l, t, l + width, t + height );
        canvas.drawBitmap( bitmap, mRectSrc, mRectDst, mPaint );
    }

    /**
     * 资源id转Bitmap
     * @param resId             图片资源id
     * @return                  Bitmap
     */
    private Bitmap resIdToBitmap(@DrawableRes int resId) {
        return BitmapFactory.decodeResource( getResources(), resId );
    }

    /**
     * 百分比转角度
     * @param progress          百分比
     * @return                  角度
     */
    private int toAngle(int progress) { return (int)( ( 360F / 100F ) * progress ); }
}
