package com.ybear.ybcomponent.widget.ribbon.config

open class EnterRibbonConfig private constructor() {
    companion object {
        //默认启用彩带
        private var isEnableRibbon = true
        //默认关闭彩带后不会保留关闭期间的彩带数据
        private var keepAddQueueByDisabledRibbon = false
        @JvmStatic
        private val i : EnterRibbonConfig by lazy { EnterRibbonConfig() }
        @JvmStatic
        fun get(): EnterRibbonConfig { return i }
    }
    /**
     * 启用彩带
     */
    fun setEnableRibbon(enable: Boolean): EnterRibbonConfig {
        Companion.isEnableRibbon = enable
        return this
    }

    val isEnableRibbon: Boolean
        get() = Companion.isEnableRibbon

    /**
     * 启用后，禁用彩带功能，依旧会添加队列，保证启用后不会丢失之前添加的队列
     */
    fun setKeepAddQueueByDisabledRibbon(enable: Boolean): EnterRibbonConfig {
        keepAddQueueByDisabledRibbon = enable
        return this
    }

    /**
     * 检查禁用彩带后，是否添加队列
     */
    fun checkRibbonByAddQueue(): Boolean {
        return if (Companion.isEnableRibbon) true else keepAddQueueByDisabledRibbon
    }
}