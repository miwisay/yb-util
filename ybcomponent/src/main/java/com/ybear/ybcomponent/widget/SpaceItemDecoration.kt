package com.ybear.ybcomponent.widget

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ybear.ybcomponent.Utils

/**
 * 为 RecyclerView 添加 Item 间距的 ItemDecoration。
 *
 * @param spacingRect 间距大小 (以像素为单位)
 * 默认右边和底部设置间距，如需平均间距的情况
 */
class SpaceItemDecoration @JvmOverloads constructor(
    spacing: Int = 0, context: Context? = null
) : RecyclerView.ItemDecoration() {
    private val spacingRect: Rect = Rect()

    //滑动方向，内部会自动识别，如果是自定义LayoutManager可以自行修改
    var orientation = -1

    init {
        if( spacing != 0 ) {
            if( context == null ) spaceBottom( spacing ) else spaceBottom( context, spacing )
        }
    }

    /**
     * 自由间距
     */
    fun spaceRect(rect: Rect) : SpaceItemDecoration {
        spacingRect.set( rect )
        return this
    }

    /**
     * 水平模式：顶部间距
     * 垂直模式：左边间距
     */
    fun spaceTop(spacing: Int) : SpaceItemDecoration {
        spacingRect.set( spacing, spacing, 0, 0 )
        return this
    }

    /**
     * 水平模式：顶部间距
     * 垂直模式：左边间距
     */
    fun spaceTop(context: Context, spacingDp: Int) : SpaceItemDecoration {
        return spaceTop( Utils.dp2Px( context, spacingDp ) )
    }

    /**
     * 水平模式：底部间距
     * 垂直模式：右边间距
     */
    fun spaceBottom(spacing: Int) : SpaceItemDecoration {
        spacingRect.set( 0, 0, spacing, spacing )
        return this
    }

    /**
     * 水平模式：底部间距
     * 垂直模式：右边间距
     */
    fun spaceBottom(context: Context, spacingDp: Int) : SpaceItemDecoration {
        return spaceBottom( Utils.dp2Px( context, spacingDp ) )
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        // 获取 Item 在 Adapter 中的位置
        val position = parent.getChildAdapterPosition( view )
        // 获取每行/列的 Item 数量
        val spanCount = getSpanCount( parent )
        // 获取 RecyclerView 的方向 (VERTICAL 或 HORIZONTAL)
        val orientation = getOrientation( parent )
        val single = spanCount == 1
        // 计算当前 Item 所在的列/行
        val column = if( single ) position else position % spanCount
        // 判断方向是否是垂直
        val isVertical = orientation == RecyclerView.VERTICAL
        //左边距
        val l = getLeft( isVertical, single, column, spanCount )
        //右边距
        val r = getRight( isVertical, single, column, spanCount )
        //上边距
        val t = getTop( isVertical, single, position, spanCount )
        //下边距
        val b = getBottom( isVertical )
        // 根据 includeEdge 设置不同的边距计算方式
        outRect.set( l, t, r, b )
    }

    private fun getLeft(isVertical: Boolean, single: Boolean, column: Int, spanCount: Int): Int {
        val l = spacingRect.left
        return if ( isVertical )
            0
        else if( single ) l else column * l / spanCount
    }

    private fun getRight(isVertical: Boolean, single: Boolean, column: Int, spanCount: Int): Int {
        val r = spacingRect.right
        return if ( isVertical )
            0
        else if( single ) r else r - ( column + 1 ) * r / spanCount
    }

    private fun getTop(isVertical: Boolean, single: Boolean, position: Int, spanCount: Int): Int {
        val t = spacingRect.top
        return if ( isVertical )
            if ( single || position >= spanCount ) t else 0
        else
            0
    }

    private fun getBottom(isVertical: Boolean): Int {
        val b = spacingRect.bottom
        return if ( isVertical ) b else 0
    }

    /**
     * 获取每行/列的 Item 数量。
     *
     * @param parent RecyclerView
     * @return 每行/列的 Item 数量，如果无法获取则返回 1
     */
    private fun getSpanCount(parent: RecyclerView): Int {
        val layoutManager = parent.layoutManager ?: return 1
        return when (layoutManager) {
            is GridLayoutManager -> layoutManager.spanCount
            is StaggeredGridLayoutManager -> layoutManager.spanCount
            else -> 1
        }
    }

    /**
     * 获取 RecyclerView 的方向。
     *
     * @param parent RecyclerView
     * @return RecyclerView 的方向 (VERTICAL 或 HORIZONTAL)，如果无法获取则返回默认方向
     */
    private fun getOrientation(parent: RecyclerView): Int {
        val defaultOrientation = if( orientation == -1 ) RecyclerView.VERTICAL else orientation
        val layoutManager = parent.layoutManager ?: return defaultOrientation
        return when ( layoutManager ) {
            is GridLayoutManager -> layoutManager.orientation
            is LinearLayoutManager -> layoutManager.orientation
            is StaggeredGridLayoutManager -> layoutManager.orientation
            else -> defaultOrientation
        }
    }
}

//class SpaceItemDecoration(private val mRect: Rect?) : RecyclerView.ItemDecoration() {
//    @JvmOverloads
//    constructor(
//        space: Int,
//        @RecyclerView.Orientation orientation: Int = RecyclerView.VERTICAL
//    ) : this(
//        Rect(
//            if (orientation == RecyclerView.VERTICAL) 0 else space,
//            if (orientation == RecyclerView.VERTICAL) space else 0,
//            0,
//            0
//        )
//    )
//
//    override fun getItemOffsets(
//        outRect: Rect, view: View,
//        parent: RecyclerView, state: RecyclerView.State
//    ) {
////        super.getItemOffsets( outRect, view, parent, state );
//        if (parent.getChildAdapterPosition(view) == 0) return
//        outRect.set(mRect!!)
//    }
//
//    val space: Int
//        get() = mRect?.top ?: 0
//
//    val spaces: Rect
//        get() = mRect ?: Rect(0, 0, 0, 0)
//}
