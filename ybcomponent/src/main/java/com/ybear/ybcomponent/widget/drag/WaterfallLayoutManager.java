package com.ybear.ybcomponent.widget.drag;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WaterfallLayoutManager extends RecyclerView.LayoutManager {
    private int mTotalRowHeight;
    private int mLineCount;

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.MATCH_PARENT,
                RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onDetachedFromWindow(RecyclerView view, RecyclerView.Recycler recycler) {
        super.onDetachedFromWindow(view, recycler);
        removeAndRecycleAllViews(recycler);
        recycler.clear();
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        //移除存在的所有View
        detachAndScrapAttachedViews(recycler);
        //RecyclerView实际宽度
        int totalWidth = getWidth() - getPaddingLeft() - getPaddingRight();

        View view;
        RecyclerView.LayoutParams lp;
        int[] vSize = new int[ 2 ];//0:width, 1:height
        int vLeft, vTop, vRight;
        int margin = 0;
        int maxHeight = 0;
        int lineWidth = 0;
        int rowHeight = 0;
        int rowCount = 1;
        boolean isMaxSingleLine;

        mTotalRowHeight = 0;
        mLineCount = 1;

        for (int i = 0; i < getItemCount(); i++) {
            int tbMargin;
            //加入当前View到列表中
            addView( view = recycler.getViewForPosition( i ) );
            lp = (RecyclerView.LayoutParams) view.getLayoutParams();
            measuredViewSize( view, vSize );
            tbMargin = lp.topMargin + lp.bottomMargin;
            //Rect位置
            vLeft = lp.leftMargin;
            vRight = vSize[ 0 ] + vLeft;
            vTop = 0;
            //取当前行的最大高度的View
            if( maxHeight < vSize[ 1 ] ) maxHeight = vSize[ 1 ] + lp.topMargin + lp.bottomMargin;
            //取最大边距
            if( margin < tbMargin ) margin = tbMargin * 2;
            //当前行中，所有View的宽度和当前View宽度，是否超出总宽度
            isMaxSingleLine = mSingleLineMaxCount != -1 && i > 0 && i % mSingleLineMaxCount == 0;
            if( lineWidth + vRight + lp.rightMargin > totalWidth || isMaxSingleLine ) {
                //换行
                rowHeight += vTop + maxHeight;
                mLineCount++;
                lineWidth = 0;
                rowCount = 1;
            }else {
                //不换行
                vLeft += lineWidth;
                rowCount++;
            }
            if( lineWidth > 0 ) vLeft += lp.rightMargin * ( rowCount - 1 );
            vTop += lp.topMargin + rowHeight;
            //更新一次右边的位置
            vRight = vSize[ 0 ] + vLeft;
            //设置view的位置
            layoutDecorated( view, vLeft, vTop, vRight, vSize[ 1 ] + vTop );
            //追加每个view的宽度（包括了外边距）
            lineWidth += vRight - vLeft;
        }
        mTotalRowHeight = rowHeight + maxHeight + margin;
    }

    @Override
    public void onMeasure(@NonNull RecyclerView.Recycler recycler,
                          @NonNull RecyclerView.State state,
                          int widthSpec,
                          int heightSpec) {
        int minHeight = getMinimumHeight();
        setMeasuredDimension(
                View.MeasureSpec.getSize( widthSpec ),
                mTotalRowHeight < minHeight ? minHeight : mTotalRowHeight
        );
    }

    private int mSingleLineMaxCount = -1;
    public void setSingleLineMaxCount(int maxCount) {
        mSingleLineMaxCount = maxCount;
    }
    public int getTotalRowHeight() { return mTotalRowHeight; }

    public int getLineCount() { return mLineCount; }

    private void measuredViewSize(View v, int[] size) {
//        measureChildWithMargins( v, 0, 0 );
        v.measure( 0, 0 );
        size[ 0 ] = v.getMeasuredWidth();
        size[ 1 ] =v.getMeasuredHeight();
    }
}
