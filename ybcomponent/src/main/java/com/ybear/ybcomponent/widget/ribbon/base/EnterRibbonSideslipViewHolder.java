package com.ybear.ybcomponent.widget.ribbon.base;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.widget.ribbon.AnimationInit;
import com.ybear.ybcomponent.widget.ribbon.CreateQueue;
import com.ybear.ybcomponent.widget.ribbon.RibbonViewHolder;

import java.util.Locale;

/**
 彩带侧滑进入 ViewHolder
 动画流程：从左进入，悬停等待，淡出隐藏
 */
public class EnterRibbonSideslipViewHolder extends RibbonViewHolder {

    public EnterRibbonSideslipViewHolder(@NonNull ViewGroup parent, @NonNull View itemView, int width, int height) {
        super( parent, itemView, width, height );
    }

    public EnterRibbonSideslipViewHolder(@NonNull ViewGroup parent, @NonNull View itemView) {
        super( parent, itemView );
    }

    public EnterRibbonSideslipViewHolder(@NonNull ViewGroup parent, int layoutResId, int width, int height) {
        super( parent, layoutResId, width, height );
    }

    public EnterRibbonSideslipViewHolder(@NonNull ViewGroup parent, int layoutResId) {
        super( parent, layoutResId );
    }

    @Override
    public Locale onLocale() {
        return null;
    }

    @Override
    public boolean isRtlLayout() {
        return false;
    }

    /**
     进入动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        返回封装好的结果
     */
    @NonNull
    @Override
    public CreateQueue onEnterCreateQueue(RibbonViewHolder holder,
                                          @NonNull IItemData data,
                                          float itemOffsetX,
                                          int multiLineDirection) {
        //适配rtl布局
        boolean isRtl = isRtlLayout();
        //默认X轴位置在整个父布局之外
        float from = isRtl ? getParentWidth() : -getFullWidth();
        float to = isRtl ? -itemOffsetX : itemOffsetX;
        View v = getItemView();
        v.setTranslationX( from );
        //显示的Y轴只限制在 mMaxLine 范围内
        int ty = getFullHeight() * getCurrentLineIndex();
        v.setTranslationY( multiLineDirection < 0 ? -ty : ty );
        //上个动画结束后会隐藏这个View，所以这里要展示出来
        v.setAlpha( 1 );
        return CreateQueue.create( from, to );
    }

    /**
     进入动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        返回封装好的结果
     */
    @NonNull
    @Override
    public AnimationInit onEnterAnimationInit(float itemOffsetX,
                                                              int multiLineDirection) {
        boolean isRtl = TextUtils.getLayoutDirectionFromLocale( onLocale() ) == View.LAYOUT_DIRECTION_RTL;
        //默认X轴位置在整个父布局之外
        float from = isRtl ? getParentWidth() : -getFullWidth();
        float to = isRtl ? -itemOffsetX : itemOffsetX;
        View v = getItemView();
        v.setTranslationX( from );
        //显示的Y轴只限制在 mMaxLine 范围内
        int ty = getFullHeight() * getCurrentLineIndex();
        v.setTranslationY( multiLineDirection < 0 ? -ty : ty );
        v.setAlpha( 1 );
        return AnimationInit.create( from, to );
    }

    /**
     进入动画 - 值更新
     @param val                     更新的值
     */
    @Override
    public void onEnterAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data,
                                       float val) {
        getItemView().setTranslationX( val );
    }

    /**
     等待动画
     @param waitMillisecond         等待的时间
     */
    @Override
    public void onWaitAnimation(RibbonViewHolder holder, @NonNull IItemData data,
                                long waitMillisecond) {

    }

    /**
     退出动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    @Override
    public CreateQueue onExitCreateQueue(RibbonViewHolder holder,
                                                         @NonNull IItemData data,
                                                         float itemOffsetX,
                                                         int multiLineDirection) {
        return CreateQueue.create( 1.0F, 0.0F );
    }

    /**
     退出动画 - 初始化
     @param itemOffsetX             Item偏移的X值
     @param multiLineDirection      多列时的展示方向，1：从上往下，-1：从下往上
     @return                        用于初始化动画
     */
    @Override
    public AnimationInit onExitAnimationInit(float itemOffsetX,
                                                             int multiLineDirection) {
        return AnimationInit.create( 1.0F, 0.0F );
    }

    /**
     退出动画 - 值更新
     @param val                     更新的值
     */
    @Override
    public void onExitAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data, float val) {
        getItemView().setAlpha( val );
    }
}
