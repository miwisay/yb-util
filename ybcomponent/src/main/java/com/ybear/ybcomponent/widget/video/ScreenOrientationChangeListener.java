package com.ybear.ybcomponent.widget.video;

public interface ScreenOrientationChangeListener{
    void onOrientationChange(int orientation, int angle, boolean isPortrait);
}