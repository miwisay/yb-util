package com.ybear.ybcomponent.widget.ribbon;

public interface OnRibbonCallable<V, E> {
    void call(V view, E data);
}
