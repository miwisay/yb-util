package com.ybear.ybcomponent.widget.shape;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.StyleableRes;

import com.ybear.ybcomponent.R;
import com.ybear.ybcomponent.widget.shape.helper.IShape;
import com.ybear.ybcomponent.widget.shape.helper.ShapeHelper;

public class ShapeView extends View implements IShape {
    private final ShapeHelper mHelper = new ShapeHelper();

    public ShapeView(Context context) {
        this(context, null);
    }

    public ShapeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShapeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mHelper.init( this );
        initTypeArray( context, attrs, R.styleable.ShapeView );
    }

    private void initTypeArray(Context context, AttributeSet set, @StyleableRes int[] attrs) {
        TypedArray typedArray = context.obtainStyledAttributes( set, attrs );
        //形状类型（圆角、圆形）
        setShape(typedArray.getInt(
                R.styleable.ShapeView_shape, Shape.ROUND_RECT
        ));
        //圆角
        mHelper.updateRadius(
                typedArray,
                R.styleable.ShapeView_shapeRadius,
                R.styleable.ShapeView_shapeRadiusLT,
                R.styleable.ShapeView_shapeRadiusRT,
                R.styleable.ShapeView_shapeRadiusLB,
                R.styleable.ShapeView_shapeRadiusRB,
                R.styleable.ShapeView_shapeRadiusLRT,
                R.styleable.ShapeView_shapeRadiusLRB,
                R.styleable.ShapeView_shapeRadiusLTB,
                R.styleable.ShapeView_shapeRadiusRTB,
                R.styleable.ShapeView_shapeRadiusLTRB,
                R.styleable.ShapeView_shapeRadiusLBRT
        );
        //描边大小
        setBorderSize(typedArray.getDimensionPixelSize(
                R.styleable.ShapeView_shapeBorderSize, 0
        ));
        //描边颜色
        setBorderColor(typedArray.getColor(
                R.styleable.ShapeView_shapeBorderColor, Color.WHITE
        ));
        //阴影半径
        setShadowRadius(typedArray.getDimensionPixelSize(
                R.styleable.ShapeView_shapeShadowRadius, 0
        ));
        //阴影颜色
        setShadowColor(typedArray.getColor(
                R.styleable.ShapeView_shapeShadowColor, 0
        ));
        //阴影X偏移
        setShadowOffsetX(typedArray.getDimensionPixelSize(
                R.styleable.ShapeView_shapeShadowOffsetX, 0
        ));
        //阴影Y偏移
        setShadowOffsetY(typedArray.getDimensionPixelSize(
                R.styleable.ShapeView_shapeShadowOffsetY, 0
        ));
        //复用
        typedArray.recycle();

        //防止父控件内部的子控件被遮挡
        mHelper.updatePadding();
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        mHelper.dispatchDraw( canvas, this );
    }

    @Override
    public void setShape(@Shape int shape) { mHelper.setShape( shape ); }
    @Override
    public void setRadius(float r) { mHelper.setRadius( r ); }

    @Override
    public void setRadius(float lt, float rt, float lb, float rb) {
        mHelper.setRadius( lt, rt, lb, rb );
    }

    @Override
    public void setRadiusLRT(float r) { mHelper.setRadiusLRT( r ); }

    @Override
    public void setRadiusLRB(float r) { mHelper.setRadiusLRB( r ); }

    @Override
    public void setRadiusLTB(float r) { mHelper.setRadiusLTB( r ); }

    @Override
    public void setRadiusRTB(float r) { mHelper.setRadiusRTB( r ); }

    @Override
    public void setRadiusLTRB(float r) { mHelper.setRadiusLTRB( r ); }

    @Override
    public void setRadiusLBRT(float r) { mHelper.setRadiusLBRT( r ); }

    @Override
    public void setRadii(float[] radii) { mHelper.setRadii( radii ); }

    @Override
    public float[] getRadii() { return mHelper.getRadii(); }

    @Override
    public void setBorderSize(int size) { mHelper.setBorderSize( size ); }
    @Override
    public void setBorderColor(int color) { mHelper.setBorderColor( color ); }
    @Override
    public void setShadowRadius(int radius) { mHelper.setShadowRadius( radius ); }
    @Override
    public void setShadowColor(@ColorInt int color) { mHelper.setShadowColor( color ); }
    @Override
    public void setShadowOffsetX(int offsetX) { mHelper.setShadowOffsetX( offsetX ); }
    @Override
    public void setShadowOffsetY(int offsetY) { mHelper.setShadowOffsetY( offsetY ); }

    @Override
    public void setEnableLayerTypeHardware(@NonNull View view, boolean enable) {
        mHelper.setEnableLayerTypeHardware( view, enable );
    }
}
