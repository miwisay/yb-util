package com.ybear.ybcomponent.widget.dialog

import android.app.Application
import android.content.Context
import android.view.View
import androidx.annotation.StyleRes
import androidx.fragment.app.Fragment

object Dialog {
    private var dialogX : DialogX? = null
    @JvmStatic
    fun setDialog(dialogX: DialogX) {
        this.dialogX = dialogX
    }

    @JvmStatic
    fun initLifecycle(application: Application?): DialogQueue {
        return DialogQueue.get().initLifecycle(application)
    }

    @JvmStatic
    fun getInit() : DialogInit{ return DialogInit.get() }

    @JvmStatic
    @JvmOverloads
    fun with(context: Context, @StyleRes themeResId: Int = 0): DialogConfig {
        return DialogConfig( context, dialogX, themeResId )
    }
    @JvmStatic
    @JvmOverloads
    fun with(v: View, @StyleRes themeResId: Int = 0): DialogConfig {
        return with( v.context, themeResId )
    }

    @JvmStatic
    @JvmOverloads
    fun with(f: Fragment, @StyleRes themeResId: Int = 0): DialogConfig {
        val context = f.context ?: throw NullPointerException( "Dialog.width: context is null!" )
        return with( context, themeResId )
    }

    @JvmStatic
    @Deprecated("", ReplaceWith(
        "with(f: androidx.fragment.app, themeResId)",
        "com.ybear.ybcomponent.widget.dialog.Dialog.with"
    ))
    fun with(f: android.app.Fragment, @StyleRes themeResId: Int): DialogConfig {
        val act = f.activity ?: throw NullPointerException( "Dialog.width: activity is null!" )
        return with( act, themeResId )
    }

    @JvmStatic
    @Deprecated("", ReplaceWith(
        "with(f: androidx.fragment.app)",
        "com.ybear.ybcomponent.widget.dialog.Dialog.with"
    ))
    fun with(f: android.app.Fragment): DialogConfig { return with( f, 0 ) }
}