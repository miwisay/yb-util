package com.ybear.ybcomponent.widget.damping.helper

import android.view.MotionEvent
import android.view.animation.Interpolator

/**
 * 阻尼帮助类 - 实现接口
 */
interface IDamping {
    /**
     * 滑动事件监听器
     * @param listener        监听器
     */
    fun setOnDampingScrollListener(listener: OnDampingScrollListener?)

    /**
     * 阻尼值
     * @param value           数值越高Pull距离越长
     */
    fun setDampingValue(value: Float)

    /**
     * Pull结束时，恢复动画时长
     * @param duration        时长
     */
    fun setRecoverAnimationDuration(duration: Long)

    /**
     * Pull结束时的动画插值器
     * @param i               插值器，默认：[android.view.animation.DecelerateInterpolator]
     */
    fun setRecoverInterpolator(i: Interpolator)

    /**
     * 设置是否启用阻尼效果
     * @param enable true 启用，false 禁用
     */
    fun setEnabledDamping(enable: Boolean)

    /**
     * 获取阻尼效果是否启用
     * @return true 启用，false 禁用
     */
    fun isEnabledDamping(): Boolean

    /**
     * 是否启用下拉
     * @param enable          是否启用
     */
    fun setEnablePullDown(enable: Boolean)

    /**
     * 是否启用上拉
     * @param enable          是否启用
     */
    fun setEnablePullUp(enable: Boolean)

    /**
     * 设置滑动方向
     * @param orientation     滑动方向
     */
    fun setDampingOrientation(@Orientation orientation: Int)

    /**
     * 分发触摸事件
     * @param ev 触摸事件
     * @return 是否消费了触摸事件
     */
    fun superDispatchTouchEvent(ev: MotionEvent): Boolean
}
