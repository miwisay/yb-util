package com.ybear.ybcomponent.widget.ribbon;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import java.util.Locale;

public abstract class RibbonViewHolder implements OnViewAnimationListener {
        @NonNull
        private final ViewGroup parentView;
        @NonNull
        private final View itemView;
        private int width;
        private int height;
        private int idleIndex = -1;                //空闲下标为-1时为空闲状态，>=0时为池中的下标
        private int currentLineIndex = -1;         //当前展示的列
        private ValueAnimator enterValAnim;        //进入动画
        private ValueAnimator exitValAnim;         //退出动画
        private int itemViewType;                  //Item的布局类型
        private int horizontalMargins;             //Item的水平间距
        private int verticalMargins;               //Item的垂直间距

        public RibbonViewHolder(@NonNull ViewGroup parent, @NonNull View itemView, int width, int height) {
            parentView = parent;
            this.itemView = itemView;
            setSize( width, height );
        }
        public RibbonViewHolder(@NonNull ViewGroup parent, @NonNull View itemView) {
            this(
                    parent, itemView,
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            );
        }
        public RibbonViewHolder(@NonNull ViewGroup parent, @LayoutRes int layoutResId, int width, int height) {
            parentView = parent;
            itemView = LayoutInflater.from( parent.getContext() )
                    .inflate( layoutResId, parent, false );
            setSize( width, height );
        }
        public RibbonViewHolder(@NonNull ViewGroup parent, @LayoutRes int layoutResId) {
            this(
                    parent, layoutResId,
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            );
        }

    @NonNull
        @Override
        public String toString() {
            return "RibbonRibbonViewHolder{" +
                    "itemView=" + itemView +
                    ", width=" + width +
                    ", height=" + height +
                    ", idleIndex=" + idleIndex +
                    ", currentLineIndex=" + currentLineIndex +
                    ", enterValAnim=" + enterValAnim +
                    ", exitValAnim=" + exitValAnim +
                    ", itemViewType=" + itemViewType +
                    ", horizontalMargins=" + horizontalMargins +
                    ", verticalMargins=" + verticalMargins +
                    '}';
        }

        @NonNull
        public Context getContext() { return itemView.getContext(); }

        public abstract Locale onLocale();

        public abstract boolean isRtlLayout();

        /**
         Holder在释放资源前会调用该类
         */
        public void onReleaseHolder(@NonNull ViewGroup parent) {
            try {
                parent.removeView( itemView );
            }catch(Exception e) {
                e.printStackTrace();
            }
            try {
                clearAnim( enterValAnim );
            }catch(Exception e) {
                e.printStackTrace();
            } finally {
                enterValAnim = null;
            }
            try {
                clearAnim( exitValAnim );
            }catch(Exception e) {
                e.printStackTrace();
            } finally {
                exitValAnim = null;
            }
        }

        private void clearAnim(ValueAnimator valAnim) {
            valAnim.cancel();
            valAnim.removeAllUpdateListeners();
            valAnim.removeAllListeners();
        }

        @NonNull
        public View getItemView() { return itemView; }

        void setSize(int width, int height) {
            this.width = width;
            this.height = height;
        }
        public int getWidth() { return width; }
        public int getHeight() { return height; }
        public int getFullWidth() { return getWidth() + getHorizontalMargins(); }
        public int getFullHeight() { return getHeight() + getVerticalMargins(); }
        public int getParentWidth() { return parentView.getMeasuredWidth(); }
        public int getParentHeight() { return parentView.getMeasuredHeight(); }

        boolean isIdle() { return getIdleIndex() == -1; }
        int getIdleIndex() { return idleIndex; }
        void setIdleIndex(int index) { idleIndex = index; }

        public int getCurrentLineIndex() { return currentLineIndex; }
        void setCurrentLineIndex(int lineIndex) { currentLineIndex = lineIndex; }

        ValueAnimator getEnterValAnim() { return enterValAnim; }
        void setEnterValAnim(ValueAnimator enterValAnim) { this.enterValAnim = enterValAnim; }

        ValueAnimator getExitValAnim() { return exitValAnim; }
        void setExitValAnim(ValueAnimator exitValAnim) { this.exitValAnim = exitValAnim; }

        public int getItemViewType() { return itemViewType; }
        void setItemViewType(int type) { itemViewType = type; }

        public int getHorizontalMargins() { return horizontalMargins; }
        public void setHorizontalMargins(int margins) { horizontalMargins = margins; }

        public int getVerticalMargins() { return verticalMargins; }
        public void setVerticalMargins(int margins) { verticalMargins = margins; }
}