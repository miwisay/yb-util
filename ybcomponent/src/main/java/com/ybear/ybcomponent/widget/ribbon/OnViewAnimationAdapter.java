package com.ybear.ybcomponent.widget.ribbon;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybcomponent.base.adapter.IItemData;

public abstract class OnViewAnimationAdapter implements OnViewAnimationListener {

    @Nullable
    @Override
    public CreateQueue onEnterCreateQueue(RibbonViewHolder holder, @NonNull IItemData data,
                                          float itemOffsetX, int multiLineDirection) {
        return null;
    }

    @Override
    public void onEnterAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data, float val) {

    }

    @Override
    public void onWaitAnimation(RibbonViewHolder holder, @NonNull IItemData data, long waitMillisecond) {

    }

    @Nullable
    @Override
    public CreateQueue onExitCreateQueue(RibbonViewHolder holder, @NonNull IItemData data,
                                         float itemOffsetX, int multiLineDirection) {
        return null;
    }

    @Override
    public AnimationInit onExitAnimationInit(float itemOffsetX, int multiLineDirection) {
        return null;
    }

    @Override
    public void onExitAnimationUpdate(RibbonViewHolder holder, @NonNull IItemData data, float val) {

    }
}