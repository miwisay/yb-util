package com.ybear.ybcomponent.widget.dialog

import android.content.Context
import com.ybear.ybcomponent.Utils
import com.ybear.ybcomponent.widget.shape.helper.IShape

open class CornerRadius(private val mParent: IShape?) {
    private var defaultRadius = false
    var radius = -1F
        private set
    lateinit var radii: FloatArray
        private set

    open fun defaultRadius() { defaultRadius = true }
    open fun isDefaultRadius() : Boolean { return defaultRadius }

    open fun setCornerRadius(radius: Float) {
        this.radius = radius
        mParent?.setRadius(radius)
    }

    open fun setCornerRadii(radii: FloatArray): CornerRadius {
        this.radii = radii
        if (mParent != null) mParent.radii = radii
        return this
    }

    open fun setCornerRadius(
        lt1: Float, lt2: Float, rt1: Float, rt2: Float,
        lb1: Float, lb2: Float, rb1: Float, rb2: Float
    ): CornerRadius {
        return setCornerRadii(floatArrayOf(lt1, lt2, rt1, rt2, rb1, rb2, lb1, lb2))
    }

    open fun setCornerRadius(lt: Float, rt: Float, lb: Float, rb: Float): CornerRadius {
        return setCornerRadius(lt, lt, rt, rt, lb, lb, rb, rb)
    }

    open fun setCornerRadiusTop(left: Float, right: Float): CornerRadius {
        return setCornerRadius(left, right, 0f, 0f)
    }

    open fun setCornerRadiusTop(radius: Float): CornerRadius {
        return setCornerRadiusTop(radius, radius)
    }

    open fun setCornerRadiusBottom(left: Float, right: Float): CornerRadius {
        return setCornerRadius(0f, 0f, left, right)
    }

    open fun setCornerRadiusBottom(radius: Float): CornerRadius {
        return setCornerRadiusBottom(radius, radius)
    }

    open fun setCornerRadiusLeft(top: Float, bottom: Float): CornerRadius {
        return setCornerRadius(top, 0f, bottom, 0f)
    }

    open fun setCornerRadiusLeft(radius: Float): CornerRadius {
        return setCornerRadiusLeft(radius, radius)
    }

    open fun setCornerRadiusRight(top: Float, bottom: Float): CornerRadius {
        return setCornerRadius(0f, top, 0f, bottom)
    }

    open fun setCornerRadiusRight(radius: Float): CornerRadius {
        return setCornerRadiusRight(radius, radius)
    }
}