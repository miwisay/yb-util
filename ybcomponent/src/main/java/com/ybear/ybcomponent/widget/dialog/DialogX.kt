package com.ybear.ybcomponent.widget.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.ActionMode
import android.view.ContextMenu
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.SearchEvent
import android.view.View
import android.view.WindowManager
import android.view.accessibility.AccessibilityEvent
import androidx.arch.core.util.Function

open class DialogX : Dialog {
    constructor(context: Context) : super(context)
    constructor(context: Context, themeResId: Int) : super(context, themeResId)
    protected constructor(
        context: Context,
        cancelable: Boolean,
        cancelListener: DialogInterface.OnCancelListener?
    ) : super(context, cancelable, cancelListener)

    private var onDispatchTouchEvent: Function<MotionEvent, Boolean>? = null
    private var dialogInterfaceHelper: DialogInterfaceHelper? = null

    open fun setOnDispatchTouchEvent(call: Function<MotionEvent, Boolean>?) {
        onDispatchTouchEvent = call
    }

    /**
     * Dialog 接口，定义了 Dialog 的各种回调方法。
     */
    open fun setDialogInterfaceHelper(call: DialogInterfaceHelper?) {
        dialogInterfaceHelper = call
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        super.dispatchTouchEvent( ev )
        val t1 = onDispatchTouchEvent?.apply( ev ) ?: false
        val t2 = dialogInterfaceHelper?.dispatchTouchEvent( ev ) ?: false
        return t1 || t2
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val t1 = super.dispatchKeyEvent( event )
        val t2 = dialogInterfaceHelper?.dispatchKeyEvent( event ) ?: false
        return t1 || t2
    }

    override fun dispatchKeyShortcutEvent(event: KeyEvent): Boolean {
        val t1 = super.dispatchKeyShortcutEvent( event )
        val t2 = dialogInterfaceHelper?.dispatchKeyShortcutEvent( event ) ?: false
        return t1 || t2
    }

    override fun dispatchTrackballEvent(ev: MotionEvent): Boolean {
        val t1 = super.dispatchTrackballEvent( ev )
        val t2 = dialogInterfaceHelper?.dispatchTrackballEvent( ev ) ?: false
        return t1 || t2
    }

    override fun dispatchGenericMotionEvent(ev: MotionEvent): Boolean {
        val t1 = super.dispatchGenericMotionEvent( ev )
        val t2 = dialogInterfaceHelper?.dispatchGenericMotionEvent( ev ) ?: false
        return t1 || t2
    }

    override fun dispatchPopulateAccessibilityEvent(event: AccessibilityEvent): Boolean {
        val t1 = super.dispatchPopulateAccessibilityEvent( event )
        val t2 = dialogInterfaceHelper?.dispatchPopulateAccessibilityEvent( event ) ?: false
        return t1 || t2
    }

    override fun onCreatePanelView(featureId: Int): View? {
        val view = super.onCreatePanelView( featureId )
        return dialogInterfaceHelper?.onCreatePanelView( featureId ) ?: view
    }

    override fun onCreatePanelMenu(featureId: Int, menu: Menu): Boolean {
        val result = super.onCreatePanelMenu( featureId, menu )
        return dialogInterfaceHelper?.onCreatePanelMenu( featureId, menu ) ?: false || result
    }

    override fun onPreparePanel(featureId: Int, view: View?, menu: Menu): Boolean {
        val result = super.onPreparePanel( featureId, view, menu )
        return dialogInterfaceHelper?.onPreparePanel( featureId, view, menu ) ?: false || result
    }

    override fun onMenuOpened(featureId: Int, menu: Menu): Boolean {
        val result = super.onMenuOpened( featureId, menu )
        return dialogInterfaceHelper?.onMenuOpened( featureId, menu ) ?: false || result
    }

    override fun onMenuItemSelected(featureId: Int, item: MenuItem): Boolean {
        val result = super.onMenuItemSelected( featureId, item )
        return dialogInterfaceHelper?.onMenuItemSelected( featureId, item ) ?: false || result
    }

    override fun onWindowAttributesChanged(params: WindowManager.LayoutParams?) {
        super.onWindowAttributesChanged(params)
        dialogInterfaceHelper?.onWindowAttributesChanged( params )
    }

    override fun onContentChanged() {
        super.onContentChanged()
        dialogInterfaceHelper?.onContentChanged()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged( hasFocus )
        dialogInterfaceHelper?.onWindowFocusChanged( hasFocus )
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        dialogInterfaceHelper?.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        dialogInterfaceHelper?.onDetachedFromWindow()
    }

    override fun onPanelClosed(featureId: Int, menu: Menu) {
        super.onPanelClosed( featureId, menu )
        dialogInterfaceHelper?.onPanelClosed( featureId, menu )
    }

    override fun onSearchRequested(searchEvent: SearchEvent): Boolean {
        val result = super.onSearchRequested( searchEvent )
        return dialogInterfaceHelper?.onSearchRequested( searchEvent ) ?: false || result
    }

    override fun onSearchRequested(): Boolean {
        val result = super.onSearchRequested()
        return dialogInterfaceHelper?.onSearchRequested() ?: false || result
    }

    override fun onWindowStartingActionMode(callback: ActionMode.Callback?): ActionMode? {
        val mode = super.onWindowStartingActionMode( callback )
        return dialogInterfaceHelper?.onWindowStartingActionMode( callback ) ?: mode
    }

    override fun onWindowStartingActionMode(callback: ActionMode.Callback?, type: Int): ActionMode? {
        val mode = super.onWindowStartingActionMode( callback, type )
        return dialogInterfaceHelper?.onWindowStartingActionMode( callback, type ) ?: mode
    }

    override fun onActionModeStarted(mode: ActionMode?) {
        super.onActionModeStarted( mode )
        dialogInterfaceHelper?.onActionModeStarted( mode )
    }

    override fun onActionModeFinished(mode: ActionMode?) {
        super.onActionModeFinished( mode )
        dialogInterfaceHelper?.onActionModeFinished( mode )
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        val result = super.onKeyDown( keyCode, event )
        return dialogInterfaceHelper?.onKeyDown( keyCode, event ) ?: false || result
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent): Boolean {
        val result = super.onKeyLongPress( keyCode, event )
        return dialogInterfaceHelper?.onKeyLongPress( keyCode, event ) ?: false || result
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        val result = super.onKeyUp( keyCode, event )
        return dialogInterfaceHelper?.onKeyUp( keyCode, event ) ?: false || result
    }

    override fun onKeyMultiple(keyCode: Int, repeatCount: Int, event: KeyEvent): Boolean {
        val result = super.onKeyMultiple( keyCode, repeatCount, event )
        return dialogInterfaceHelper?.onKeyMultiple( keyCode, repeatCount, event ) ?: false || result
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu( menu, v, menuInfo )
        dialogInterfaceHelper?.onCreateContextMenu( menu, v, menuInfo )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate( savedInstanceState )
        dialogInterfaceHelper?.onCreate( savedInstanceState )
    }

    override fun onStart() {
        super.onStart()
        dialogInterfaceHelper?.onStart()
    }

    override fun onStop() {
        super.onStop()
        dialogInterfaceHelper?.onStop()
    }

    override fun onSaveInstanceState(): Bundle {
        val state = super.onSaveInstanceState()
        return dialogInterfaceHelper?.onSaveInstanceState() ?: state
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        val state = super.onRestoreInstanceState( savedInstanceState )
        return dialogInterfaceHelper?.onRestoreInstanceState( savedInstanceState ) ?: state
    }

    override fun onBackPressed() {
        super.onBackPressed()
        dialogInterfaceHelper?.onBackPressed()
    }

    override fun onKeyShortcut(keyCode: Int, event: KeyEvent): Boolean {
        val result = super.onKeyShortcut( keyCode, event )
        return dialogInterfaceHelper?.onKeyShortcut( keyCode, event ) ?: false || result
    }

    override fun onTrackballEvent(event: MotionEvent): Boolean {
        val result = super.onTrackballEvent( event )
        return dialogInterfaceHelper?.onTrackballEvent( event ) ?: false || result
    }

    override fun onGenericMotionEvent(event: MotionEvent): Boolean {
        val result = super.onGenericMotionEvent( event )
        return dialogInterfaceHelper?.onGenericMotionEvent( event ) ?: false || result
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val result = super.onCreateOptionsMenu( menu )
        return dialogInterfaceHelper?.onCreateOptionsMenu( menu ) ?: false || result
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val result = super.onPrepareOptionsMenu( menu )
        return dialogInterfaceHelper?.onPrepareOptionsMenu( menu ) ?: false || result
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val result = super.onOptionsItemSelected( item )
        return dialogInterfaceHelper?.onOptionsItemSelected( item ) ?: false || result
    }

    override fun onOptionsMenuClosed(menu: Menu) {
        super.onOptionsMenuClosed( menu )
        dialogInterfaceHelper?.onOptionsMenuClosed( menu )
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val result = super.onContextItemSelected( item )
        return dialogInterfaceHelper?.onContextItemSelected( item ) ?: false || result
    }

    override fun onContextMenuClosed(menu: Menu) {
        super.onContextMenuClosed( menu )
        dialogInterfaceHelper?.onContextMenuClosed( menu )
    }
}