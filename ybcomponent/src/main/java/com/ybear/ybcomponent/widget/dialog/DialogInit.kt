package com.ybear.ybcomponent.widget.dialog

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.Gravity
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.ybear.ybcomponent.R

class DialogInit private constructor() {
    companion object {
        @JvmStatic
        private val i by lazy { DialogInit() }
        @JvmStatic
        fun get(): DialogInit { return i }
    }

    var buttonLayoutLeftPadding = 0 //按钮父布局右边的内边距
        private set
    var buttonLayoutTopPadding = 0 //按钮父布局底部的内边距
        private set
    var buttonLayoutRightPadding = 4 //按钮父布局右边的内边距
        private set
    var buttonLayoutBottomPadding = 10 //按钮父布局底部的内边距
        private set
    var buttonPadding = 8 //按钮的内边距
        private set
    var buttonRightMargin = 12 //按钮右边的外边距
        private set
    var defTextSize = 14 //默认字体大小
        private set
    var defTitleTextSize = 20 //默认标题大小
        private set
    var defTitleLeftPadding = 14 //默认标题左边的内边距
        private set
    var defTitleTopPadding = 0 //默认标题顶部的内边距
        private set
    var defTitleRightPadding = 14 //默认标题右边的内边距
        private set
    var defTitleBottomPadding = 0 //默认标题底部的内边距
        private set
    var defMessageLeftPadding = 20 //默认消息内容左边的内边距
        private set
    var defMessageTopPadding = 10 //默认消息内容顶部的内边距
        private set
    var defMessageRightPadding = 20 //默认消息内容右边的内边距
        private set
    var defMessageBottomPadding = 14 //默认消息内容底部的内边距
        private set

    @get:ColorInt
    @ColorInt
    var defTextBackgroundColor = 0
        private set
    var defTextBackground: Drawable? = null
        private set

    @get:DrawableRes
    @DrawableRes
    var defTextBackgroundRes = 0
        private set
    var defTextUnit = TypedValue.COMPLEX_UNIT_SP
        private set

    @get:ColorRes
    @ColorRes
    var defTextColor = R.color.colorPrimary
        private set

    @get:ColorInt
    @ColorInt
    var defTextColorInt = -1
        private set

    @get:ColorRes
    @ColorRes
    var defTitleColor = R.color.colorPrimary
        private set

    @get:ColorInt
    @ColorInt
    var defTitleColorInt = -1
        private set
    var defTextTypeface: Typeface? = null
        private set

    @get:TypefaceStyle
    @TypefaceStyle
    var defTextStyle = TypefaceStyle.NORMAL
        private set
    var defTextGravity = Gravity.NO_GRAVITY
        private set
    var defTextPaddings: IntArray? = null
        private set
    var defTextMaxLines = -1
        private set

    fun setButtonLayoutPadding(l: Int, t: Int, r: Int, b: Int): DialogInit {
        buttonLayoutLeftPadding = l
        buttonLayoutTopPadding = t
        buttonLayoutRightPadding = r
        buttonLayoutBottomPadding = b
        return this
    }

    fun setButtonLayoutLeftPadding(buttonLayoutLeftPadding: Int): DialogInit {
        this.buttonLayoutLeftPadding = buttonLayoutLeftPadding
        return this
    }

    fun setButtonLayoutTopPadding(buttonLayoutTopPadding: Int): DialogInit {
        this.buttonLayoutTopPadding = buttonLayoutTopPadding
        return this
    }

    fun setButtonLayoutRightPadding(buttonLayoutRightPadding: Int): DialogInit {
        this.buttonLayoutRightPadding = buttonLayoutRightPadding
        return this
    }

    fun setButtonLayoutBottomPadding(buttonLayoutBottomPadding: Int): DialogInit {
        this.buttonLayoutBottomPadding = buttonLayoutBottomPadding
        return this
    }

    fun setButtonPadding(buttonPadding: Int): DialogInit {
        this.buttonPadding = buttonPadding
        return this
    }

    fun setButtonRightMargin(buttonRightMargin: Int): DialogInit {
        this.buttonRightMargin = buttonRightMargin
        return this
    }

    fun setDefTextSize(defTextSize: Int): DialogInit {
        this.defTextSize = defTextSize
        return this
    }

    fun setDefTitleTextSize(defTitleTextSize: Int): DialogInit {
        this.defTitleTextSize = defTitleTextSize
        return this
    }

    fun setDefTitlePadding(l: Int, t: Int, r: Int, b: Int): DialogInit {
        defTitleLeftPadding = l
        defTitleTopPadding = t
        defTitleRightPadding = r
        defTitleBottomPadding = b
        return this
    }

    fun setDefTitleLeftPadding(defTitleLeftPadding: Int): DialogInit {
        this.defTitleLeftPadding = defTitleLeftPadding
        return this
    }

    fun setDefTitleTopPadding(defTitleTopPadding: Int): DialogInit {
        this.defTitleTopPadding = defTitleTopPadding
        return this
    }

    fun setDefTitleRightPadding(defTitleRightPadding: Int): DialogInit {
        this.defTitleRightPadding = defTitleRightPadding
        return this
    }

    fun setDefTitleBottomPadding(defTitleBottomPadding: Int): DialogInit {
        this.defTitleBottomPadding = defTitleBottomPadding
        return this
    }

    fun setDefMessagePadding(l: Int, t: Int, r: Int, b: Int): DialogInit {
        defMessageLeftPadding = l
        defMessageTopPadding = t
        defMessageRightPadding = r
        defMessageBottomPadding = b
        return this
    }

    fun setDefMessageLeftPadding(defMessageLeftPadding: Int): DialogInit {
        this.defMessageLeftPadding = defMessageLeftPadding
        return this
    }

    fun setDefMessageTopPadding(defMessageTopPadding: Int): DialogInit {
        this.defMessageTopPadding = defMessageTopPadding
        return this
    }

    fun setDefMessageRightPadding(defMessageRightPadding: Int): DialogInit {
        this.defMessageRightPadding = defMessageRightPadding
        return this
    }

    fun setDefMessageBottomPadding(defMessageBottomPadding: Int): DialogInit {
        this.defMessageBottomPadding = defMessageBottomPadding
        return this
    }

    fun setDefTextBackgroundColor(@ColorInt defTextBackgroundColor: Int): DialogInit {
        this.defTextBackgroundColor = defTextBackgroundColor
        return this
    }

    fun setDefTextBackground(defTextBackground: Drawable?): DialogInit {
        this.defTextBackground = defTextBackground
        return this
    }

    fun setDefTextBackgroundRes(@DrawableRes defTextBackgroundRes: Int): DialogInit {
        this.defTextBackgroundRes = defTextBackgroundRes
        return this
    }

    fun setDefTextUnit(defTextUnit: Int): DialogInit {
        this.defTextUnit = defTextUnit
        return this
    }

    fun setDefTextColor(@ColorRes defTextColor: Int): DialogInit {
        this.defTextColor = defTextColor
        return this
    }

    fun setDefTextColorInt(@ColorInt defTextColorInt: Int): DialogInit {
        this.defTextColorInt = defTextColorInt
        return this
    }

    fun setDefTitleColor(@ColorRes defTitleColor: Int): DialogInit {
        this.defTitleColor = defTitleColor
        return this
    }

    fun setDefTitleColorInt(@ColorInt defTitleColorInt: Int): DialogInit {
        this.defTitleColorInt = defTitleColorInt
        return this
    }

    fun setDefTextTypeface(defTextTypeface: Typeface?): DialogInit {
        this.defTextTypeface = defTextTypeface
        return this
    }

    fun setDefTextStyle(@TypefaceStyle defTextStyle: Int): DialogInit {
        this.defTextStyle = defTextStyle
        return this
    }

    fun setDefTextGravity(defTextGravity: Int): DialogInit {
        this.defTextGravity = defTextGravity
        return this
    }

    fun setDefTextPaddings(left: Int, top: Int, right: Int, bottom: Int): DialogInit {
        defTextPaddings = intArrayOf(left, top, right, bottom)
        return this
    }

    fun setDefTextPaddings(defTextPaddings: IntArray): DialogInit {
        this.defTextPaddings = defTextPaddings
        return this
    }

    fun setDefTextMaxLines(defTextMaxLines: Int): DialogInit {
        this.defTextMaxLines = defTextMaxLines
        return this
    }
}
