package com.ybear.ybcomponent.widget.ribbon;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.Nullable;

public class AnimationInit {
    private float[] values;
    private TimeInterpolator timeInterpolator;

    private AnimationInit() {}
    private AnimationInit(TimeInterpolator timeInterpolator, float... values) {
        this.values = values;
        this.timeInterpolator = timeInterpolator;
    }

    public static AnimationInit create(@Nullable TimeInterpolator timeInterpolator,
                                       float... values) {
        return new AnimationInit( timeInterpolator, values );
    }
    public static AnimationInit create(float... values) {
        return create( new DecelerateInterpolator(), values );
    }

    public float[] getValues() { return values; }
    public void setValues(float[] values) { this.values = values; }

    public TimeInterpolator getTimeInterpolator() { return timeInterpolator; }
}