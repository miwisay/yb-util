package com.ybear.ybcomponent.widget.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import java.util.Arrays

class TextStyle {
    private val init = DialogInit.get()
    private var lpWidth = -2
    private var lpHeight = -2
    private var width = -1
    private var height = -1

    @ColorInt
    var backgroundColor = init.defTextBackgroundColor
        private set
    var background = init.defTextBackground
        private set

    @DrawableRes
    var backgroundRes = init.defTextBackgroundRes
        private set
    var textUnit = init.defTextUnit
        private set
    var textSize = init.defTextSize.toFloat()
        private set

    @get:ColorRes
    @ColorRes
    var textColor = init.defTextColor
        private set

    @get:ColorInt
    @ColorInt
    var textColorInt = init.defTextColorInt
        private set
    var textTypeface = init.defTextTypeface
        private set

    @TypefaceStyle
    var textStyle = init.defTextStyle
        private set
    var gravity = init.defTextGravity
        private set
    private var paddings: IntArray? = init.defTextPaddings
    var maxLines = init.defTextMaxLines
        private set
    var isEnableHorizontalScroll = false
        private set
    var isEnableVerticalScroll = false
        private set
    private var onClickListener: View.OnClickListener? = null
    private var onLongClickListener: OnLongClickListener? = null
    override fun toString(): String {
        return "TextStyle{" +
                "lpWidth=" + lpWidth +
                ", lpHeight=" + lpHeight +
                ", width=" + width +
                ", height=" + height +
                ", backgroundColor=" + backgroundColor +
                ", background=" + background +
                ", backgroundRes=" + backgroundRes +
                ", textUnit=" + textUnit +
                ", textSize=" + textSize +
                ", textColor=" + textColor +
                ", textColorInt=" + textColorInt +
                ", textTypeface=" + textTypeface +
                ", textStyle=" + textStyle +
                ", gravity=" + gravity +
                ", paddings=" + Arrays.toString(paddings) +
                ", maxLines=" + maxLines +
                ", enableHorizontalScroll=" + isEnableHorizontalScroll +
                ", enableVerticalScroll=" + isEnableVerticalScroll +
                ", onClickListener=" + onClickListener +
                ", onLongClickListener=" + onLongClickListener +
                '}'
    }

    fun setLayoutParams(width: Int, height: Int): TextStyle {
        lpWidth = width
        lpHeight = height
        return this
    }

    fun setSize(width: Int, height: Int): TextStyle {
        this.width = width
        this.height = height
        return this
    }

    fun setBackgroundColor(backgroundColor: Int): TextStyle {
        this.backgroundColor = backgroundColor
        return this
    }

    fun setBackground(background: Drawable?): TextStyle {
        this.background = background
        return this
    }

    fun setBackgroundRes(backgroundRes: Int): TextStyle {
        this.backgroundRes = backgroundRes
        return this
    }

    fun setTextUnit(textUnit: Int): TextStyle {
        this.textUnit = textUnit
        return this
    }

    fun setTextSize(textSize: Float): TextStyle {
        this.textSize = textSize
        return this
    }

    fun setTextColor(@ColorRes textColor: Int): TextStyle {
        this.textColor = textColor
        return this
    }

    fun setTextColorInt(textColorInt: Int): TextStyle {
        this.textColorInt = textColorInt
        return this
    }

    /**
     *
     * @param textTypeface  [Typeface]
     * @param textStyle     [TypefaceStyle.NORMAL]
     * [TypefaceStyle.BOLD]
     * [TypefaceStyle.ITALIC]
     * [TypefaceStyle.BOLD_ITALIC]
     * @return              [TextStyle]
     */
    fun setTextTypeface(
        textTypeface: Typeface?,
        @TypefaceStyle textStyle: Int
    ): TextStyle {
        this.textTypeface = textTypeface
        this.textStyle = textStyle
        return this
    }

    fun setTextTypeface(textTypeface: Typeface?): TextStyle {
        this.textTypeface = textTypeface
        return this
    }

    fun setGravity(gravity: Int): TextStyle {
        this.gravity = gravity
        return this
    }

    fun setPadding(start: Int, top: Int, end: Int, bottom: Int): TextStyle {
        paddings = intArrayOf(start, top, end, bottom)
        return this
    }

    fun setPadding(p: Int): TextStyle {
        return setPadding(p, p, p, p)
    }

    fun setMaxLines(maxLines: Int): TextStyle {
        this.maxLines = maxLines
        return this
    }

    fun setEnableHorizontalScroll(enableHorizontalScroll: Boolean): TextStyle {
        isEnableHorizontalScroll = enableHorizontalScroll
        return this
    }

    fun setEnableVerticalScroll(enableVerticalScroll: Boolean): TextStyle {
        isEnableVerticalScroll = enableVerticalScroll
        return this
    }

    fun setOnClickListener(l: View.OnClickListener?): TextStyle {
        onClickListener = l
        return this
    }

    fun setOnLongClickListener(onLongClickListener: OnLongClickListener?): TextStyle {
        this.onLongClickListener = onLongClickListener
        return this
    }

    fun getTextView(context: Context, text: String?): TextView {
        return getTextView(context, TextView(context), text)
    }

    fun getTextView(context: Context, v: TextView, text: String?): TextView {
        v.text = text
        v.setTextSize(textUnit, textSize)
        v.setTextColor(
            if (textColorInt == -1) context.resources.getColor(textColor) else textColorInt
        )

        //Background
        if (backgroundRes != 0) {
            v.setBackgroundResource(backgroundRes) //背景资源
        } else if (backgroundColor != 0) {
            v.setBackgroundColor(backgroundColor) //背景颜色
        } else if (background != null) {
            //背景图
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                v.background = background
            } else {
                v.setBackgroundDrawable(background)
            }
        } else {
            v.setBackgroundColor(Color.TRANSPARENT) //默认透明
        }

        //TypefaceStyle
        if (textTypeface != null) {
            if (textStyle == 0) {
                v.typeface = textTypeface
            } else {
                v.setTypeface(textTypeface, textStyle)
            }
        }
        v.gravity = gravity
        if (maxLines != -1) v.maxLines = maxLines
        if (paddings != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                v.setPaddingRelative(paddings!![0], paddings!![1], paddings!![2], paddings!![3])
            } else {
                v.setPadding(paddings!![0], paddings!![1], paddings!![2], paddings!![3])
            }
        }
        if (isEnableHorizontalScroll || isEnableVerticalScroll) {
            v.movementMethod = ScrollingMovementMethod.getInstance()
            v.isHorizontalScrollBarEnabled = isEnableHorizontalScroll
            v.isVerticalScrollBarEnabled = isEnableVerticalScroll
        }

        //点击/长按事件
        if (onClickListener != null || onLongClickListener != null) {
            v.isFocusable = true
            if (onClickListener != null) {
                v.isClickable = true
                v.setOnClickListener(onClickListener)
            }
            if (onLongClickListener != null) {
                v.isLongClickable = true
                v.setOnLongClickListener(onLongClickListener)
            }
        }
        //显示
        if (v.visibility != View.VISIBLE) v.visibility = View.VISIBLE
        return v
    }

    fun createView(context: Context, text: String?): TextView {
        val v = getTextView(context, text)
        v.layoutParams = ViewGroup.LayoutParams(lpWidth, lpHeight)
        if (width != -1) v.width = width
        if (height != -1) v.height = height
        return v
    }

    companion object {
        fun get(): TextStyle {
            return TextStyle()
        }
    }
}