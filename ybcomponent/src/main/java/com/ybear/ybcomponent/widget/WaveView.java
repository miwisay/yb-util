package com.ybear.ybcomponent.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 水波纹特效
 * Created by zeda on 2018/11/06.
 */
public class WaveView extends View {
    private String TAG = "WaveView";
    private float mInitialRadius;   // 初始波纹半径
    private float mMaxRadiusRate = 1f;   // 如果没有设置mMaxRadius，可mMaxRadius = 最小长度 * mMaxRadiusRate;
    private float mMaxRadius;   // 最大波纹半径
    private long mDuration = 2500; // 一个波纹从创建到消失的持续时间
    private int mSpeed = 700;   // 波纹的创建速度，每500ms创建一个
    private float mInitialAlpha = 0f;   // 初始波纹透明度
    private Interpolator mInterpolator = new LinearInterpolator();

    private final List<Circle> mCircleList = new ArrayList<>();
    private final Queue<Circle> mRecycleCircleQueue = new LinkedBlockingQueue<>();
    private boolean mIsRunning;

    private boolean mMaxRadiusSet;

    private final Paint mWavePaint;
    private final Paint mCircleStrokePaint;
    private final Paint mWaveStrokePaint;
    private int mWavePaintColor;
    private int mCircleStrokePaintColor;
    private int mWaveStrokePaintColor;
    private long mLastCreateTime;
    private int mAvatarSize;
    private float mCircleStrokeWidth;
    private float mCircleStrokePadding;
    private float mWaveStrokeWidth;

    private final Runnable mCreateCircle = new Runnable() {
        @Override
        public void run() {
            if (mIsRunning) {
                newCircle();
                postDelayed( mCreateCircle, mSpeed );
            }
        }
    };

    public WaveView(Context context) {
        this(context, null);
    }

    public WaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mWavePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCircleStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mWaveStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        measure( 0, 0 );
    }

    public void setWaveStrokeWidth(float width) { mWaveStrokeWidth = width; }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (!mMaxRadiusSet) {
            mMaxRadius = Math.min(w, h) * mMaxRadiusRate / 2.0f;
        }
    }

    public void setMaxRadiusRate(float maxRadiusRate) {
        this.mMaxRadiusRate = maxRadiusRate;
    }

    public void setWaveColor(int color) { mWavePaintColor = color; }

    public void setCircleStrokePaintColor(int color) {
        mCircleStrokePaintColor = color;
    }

    public void setWaveStrokePaintColor(int color) {
        mWaveStrokePaintColor = color;
    }

    public void setAvatarSize(int size) {
        mAvatarSize = size;
    }

    public void setCircleStrokeWidth(float strokeWidth) {
        mCircleStrokeWidth = strokeWidth;
    }

    public void setCircleStrokePadding(float padding) {
        mCircleStrokePadding = padding;
    }

    /**
     * 开始
     *
     *
     */
    public void start() {
        if (!mIsRunning) {
            mIsRunning = true;
            post(mCreateCircle);
            this.setVisibility(VISIBLE);
        }
    }

    /**
     * 停止
     */
    public void stop() {
        mIsRunning = false;
        this.setVisibility(GONE);
    }

    protected void onDraw(Canvas canvas) {
        if( !mIsRunning ) return;
        float widthHalf = getWidth() / 2F;
        float heightHalf = getHeight() / 2F;
        float halfAvatarSize = mAvatarSize == 0 ? 0 : mAvatarSize / 2F;
        mWavePaint.setColor( mWavePaintColor );
        mWavePaint.setStyle( Paint.Style.FILL );
//        mWavePaint.setStrokeWidth( ReplaceSkinUtils.dp2Px( getContext(), 5 ) );

        if( hasWaveStroke() ) {
            mWaveStrokePaint.setColor( mWaveStrokePaintColor );
            mWaveStrokePaint.setStyle( Paint.Style.STROKE );
            mWaveStrokePaint.setStrokeWidth( mWaveStrokeWidth );
        }
        //绘制音浪
        Iterator<Circle> iterator = mCircleList.iterator();
        while( iterator.hasNext() ) {
            Circle circle = iterator.next();
            if( System.currentTimeMillis() - circle.mCreateTime < mDuration ) {
                float radius = circle.getCurrentRadius();
                //绘制波纹
                mWavePaint.setAlpha( Math.min(
                        circle.getAlpha() + (int) ( 255 * 0.3F ),
                        255
                ));
                canvas.drawCircle( widthHalf, heightHalf, radius, mWavePaint );
                //绘制波纹轮廓
                if( hasWaveStroke() ) {
                    canvas.drawCircle( widthHalf, heightHalf, radius, mWaveStrokePaint );
                }
            } else {
                mRecycleCircleQueue.add( circle );
                iterator.remove();
            }
        }
        //绘制头像轮廓
        if( hasCircleStroke() ) {
            mCircleStrokePaint.setAlpha( 1 );
            mCircleStrokePaint.setColor( mCircleStrokePaintColor );
            mCircleStrokePaint.setStyle( Paint.Style.STROKE );
            mCircleStrokePaint.setStrokeWidth( mCircleStrokeWidth );
            canvas.drawCircle(
                    widthHalf, heightHalf,
                    halfAvatarSize + mCircleStrokeWidth + mCircleStrokePadding,
                    mCircleStrokePaint
            );
        }
        if( mCircleList.size() > 0 ) postInvalidateDelayed( 10 );
    }

    private boolean hasWaveStroke() {
        return mWaveStrokeWidth > 0 && mWaveStrokePaintColor != 0;
    }
    private boolean hasCircleStroke() {
        return mCircleStrokeWidth > 0 && mCircleStrokePaintColor != 0;
    }

    public void setInitialRadius(float radius) {
        mInitialRadius = radius;
    }

    public void setInitialAlpha(float alpha) {
        mInitialAlpha = alpha;
    }

    public void setDuration(long duration) {
        this.mDuration = duration;
    }

    public void setMaxRadius(float maxRadius) {
        this.mMaxRadius = maxRadius;
        mMaxRadiusSet = true;
    }

    public void setSpeed(int speed) {
        mSpeed = speed;
    }

    private void newCircle() {
        long currentTime = System.currentTimeMillis();
        if( currentTime - mLastCreateTime < mSpeed ) return;
        Circle circle = mRecycleCircleQueue.poll();
        if( circle == null ) circle = new Circle();
        mCircleList.add( circle.init() );
        postInvalidate();
        mLastCreateTime = currentTime;
    }

    public void setInterpolator(Interpolator interpolator) {
        mInterpolator = interpolator;
        if (mInterpolator == null) {
            mInterpolator = new LinearInterpolator();
        }
    }

    private class Circle {
        private long mCreateTime;

        public Circle() {}

        private Circle init() {
            this.mCreateTime = System.currentTimeMillis();
            return this;
        }

        public int getAlpha() {
            float percent = (System.currentTimeMillis() - mCreateTime) * 1.0f / mDuration;
            return (int) (((1.0f - mInterpolator.getInterpolation(percent)) * mInitialAlpha) * 255);
        }

        public float getCurrentRadius() {
            float percent = (System.currentTimeMillis() - mCreateTime) * 1.0f / mDuration;
            return mInitialRadius + mInterpolator.getInterpolation(percent) * (mMaxRadius - mInitialRadius);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(mCreateCircle);
        mIsRunning = false;
    }
}

//package com.ybear.ybcomponent.widget
//
//import android.content.Context
//import android.graphics.Canvas
//import android.graphics.Color
//import android.graphics.LinearGradient
//import android.graphics.Paint
//import android.graphics.Shader
//import com.ybear.ybcomponent.R
//import android.util.AttributeSet
//import android.view.View
//import android.view.ViewGroup
//import android.view.animation.Interpolator
//import android.view.animation.LinearInterpolator
//import androidx.annotation.ColorInt
//import androidx.core.content.withStyledAttributes
//import androidx.core.view.isVisible
//import java.util.concurrent.LinkedBlockingQueue
//import kotlin.math.max
//import kotlin.math.min
//
///**
// * 水波纹特效
// *
// * @author MiWi
// * @date 2024年8月2日
// *
// * @demo
// * 1.XML
// *
// * ```xml
// * <?xml version="1.0" encoding="utf-8"?>
// * <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
// *     xmlns:app="http://schemas.android.com/apk/res-auto"
// *     android:layout_width="match_parent"
// *     android:layout_height="match_parent"
// *     android:gravity="center"
// *     android:orientation="vertical">
// *
// *     <com.talk.live.widget.WaveView
// *         android:id="@+id/wave_view"
// *         android:layout_width="120dp"
// *         android:layout_height="120dp"
// *         app:wv_waveGradientStartColor="@color/red"
// *         app:wv_waveGradientCenterColor="@color/yellow"
// *         app:wv_waveGradientEndColor="@color/green"
// *         app:wv_waveGradientOrientation="horizontal"
// *         app:wv_circleStrokeGradientStartColor="@color/blue"
// *         app:wv_circleStrokeGradientEndColor="@color/white"
// *         app:wv_circleStrokeGradientOrientation="vertical"
// *         app:wv_circleStrokeColor="@color/purple_200"
// *         app:wv_circleStrokeWidth="4dp"
// *         app:wv_initialRadius="60dp"
// *         app:wv_maxRadiusRate="0.8"
// *         app:wv_autoPlay="true"
// *         app:wv_waveType="stroke"
// *         app:wv_waveStrokeWidth="2dp"
// *         app:wv_autoPlayDelayMillis="1000">
// *
// *         <ImageView
// *             android:layout_width="60dp"
// *             android:layout_height="60dp"
// *             android:src="@drawable/ic_launcher_background" />
// *     </com.talk.live.widget.WaveView>
// *
// * </LinearLayout>
// * ```
// *
// * 2.Kotlin
// * ```kotlin
// * class MainActivity : AppCompatActivity() {
// *     private lateinit var waveView: WaveView
// *
// *     override fun onCreate(savedInstanceState: Bundle?) {
// *         super.onCreate(savedInstanceState)
// *         setContentView(R.layout.activity_main)
// *
// *         waveView = findViewById(R.id.wave_view)
// *
// *         // 设置属性
// *         waveView.waveColor = ContextCompat.getColor(this, R.color.wave_color) // 设置波纹颜色（纯色）
// *         // 使用渐变色时，优先级高于 waveColor
// *         waveView.waveGradientStartColor = Color.RED // 设置波纹渐变色开始颜色
// *         waveView.waveGradientCenterColor = Color.YELLOW // 设置波纹渐变色中间颜色
// *         waveView.waveGradientEndColor = Color.GREEN // 设置波纹渐变色结束颜色
// *         waveView.waveGradientOrientation = WaveView.GradientOrientation.HORIZONTAL // 设置波纹渐变方向
// *         // 设置头像轮廓颜色（纯色）
// *         waveView.circleStrokeColor = ContextCompat.getColor(this, R.color.circle_stroke_color)
// *         // 使用渐变色时，优先级高于 circleStrokeColor
// *         waveView.circleStrokeGradientStartColor = Color.BLUE // 设置头像轮廓渐变色开始颜色
// *         waveView.circleStrokeGradientEndColor = Color.WHITE // 设置头像轮廓渐变色结束颜色
// *         waveView.circleStrokeGradientOrientation = WaveView.GradientOrientation.VERTICAL // 设置头像轮廓渐变方向
// *         // ... 其他属性设置 ...
// *     }
// * }
// * ```
// *
// */
//class WaveView @JvmOverloads constructor(
//    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
//) : ViewGroup(context, attrs, defStyleAttr) {
//
//    // region #枚举
//    /**
//     * 波纹类型枚举
//     */
//    enum class WaveTypeEm(val index: Int) {
//        /**
//         * 填充波纹
//         */
//        WAVE(0),
//
//        /**
//         * 轮廓波纹
//         */
//        STROKE(1)
//    }
//
//    /**
//     * 渐变方向枚举
//     */
//    enum class GradientOrientation {
//        HORIZONTAL,
//        VERTICAL
//    }
//    // endregion
//
//    companion object {
//        private const val DEFAULT_MAX_RADIUS_RATE = 1F      // 如果没有设置最大半径，则最大半径 = 最小长度 * 最大半径比例
//        private const val DEFAULT_DURATION = 3000L          // 一个波纹从创建到消失的持续时间
//        private const val DEFAULT_SPEED = 500L              // 波纹的创建速度，每500ms创建一个
//        private const val DEFAULT_INITIAL_ALPHA = 0F        // 初始波纹透明度
//        private const val DEFAULT_WAVE_COUNT = -1           // 默认波纹数量，-1 表示无限
//        private const val DEFAULT_FADE_DURATION = 1000L     // 默认波纹消失时间
//        private const val DEFAULT_INITIAL_WAVE_COUNT = 5    // 默认初始波纹数量
//        private const val DEFAULT_WAVE_STROKE_WIDTH = 2F    // 默认轮廓波纹宽度
//    }
//
//    /**
//     * 最大波纹半径比例，相对于 WaveView 最短边的比例，取值范围为 0 - 1，默认值为 1
//     */
//    var maxRadiusRate: Float = DEFAULT_MAX_RADIUS_RATE
//        set(value) {
//            field = value
//            updateMaxRadius()
//        }
//
//    /**
//     * 波纹颜色，默认值为透明
//     */
//    @ColorInt
//    var waveColor: Int = 0
//        set(value) {
//            field = value
//            mWavePaint.color = value
//            mWavePaint.shader = null // 清除渐变
//        }
//
//    /**
//     * 波纹渐变色颜色数组，至少需要两个颜色值
//     */
//    var waveGradientColors: IntArray? = null
//        set(value) {
//            field = value
//            updateWavePaintShader()
//        }
//    /**
//     * 波纹渐变色开始颜色
//     */
//    @ColorInt
//    private var waveGradientStartColor: Int = 0
//
//    /**
//     * 波纹渐变色中间颜色
//     */
//    @ColorInt
//    private var waveGradientCenterColor: Int = 0
//
//    /**
//     * 波纹渐变色结束颜色
//     */
//    @ColorInt
//    private var waveGradientEndColor: Int = 0
//
//    /**
//     * 波纹渐变方向，默认值为 [GradientOrientation.HORIZONTAL]
//     */
//    var waveGradientOrientation: GradientOrientation = GradientOrientation.HORIZONTAL
//        set(value) {
//            field = value
//            updateWavePaintShader()
//        }
//
//    /**
//     * 头像轮廓颜色，默认值为透明
//     */
//    @ColorInt
//    var circleStrokeColor: Int = 0
//        set(value) {
//            field = value
//            mCircleStrokePaint.color = value
//            mCircleStrokePaint.shader = null // 清除渐变
//        }
//
//    /**
//     * 头像轮廓渐变色颜色数组，至少需要两个颜色值
//     */
//    var circleStrokeGradientColors: IntArray? = null
//        set(value) {
//            field = value
//            updateCircleStrokePaintShader()
//        }
//
//    /**
//     * 头像轮廓渐变色开始颜色
//     */
//    @ColorInt
//    private var circleStrokeGradientStartColor: Int = 0
//
//    /**
//     * 头像轮廓渐变色中间颜色
//     */
//    @ColorInt
//    private var circleStrokeGradientCenterColor: Int = 0
//
//    /**
//     * 头像轮廓渐变色结束颜色
//     */
//    @ColorInt
//    private var circleStrokeGradientEndColor: Int = 0
//
//    /**
//     * 头像轮廓渐变方向，默认值为 [GradientOrientation.HORIZONTAL]
//     */
//    var circleStrokeGradientOrientation: GradientOrientation = GradientOrientation.HORIZONTAL
//        set(value) {
//            field = value
//            updateCircleStrokePaintShader()
//        }
//
//    /**
//     *  波纹轮廓颜色，默认值为白色
//     */
//    @ColorInt
//    var waveStrokeColor: Int = Color.WHITE
//        set(value) {
//            field = value
//            mWaveStrokePaint.color = value
//        }
//
//    /**
//     * 头像轮廓宽度，默认值为 0dp
//     */
//    var circleStrokeWidth: Float = 0F
//        set(value) {
//            field = value
//            mCircleStrokePaint.strokeWidth = value
//        }
//
//    /**
//     * 头像轮廓和波纹之间的间距，默认值为 0dp
//     */
//    var circleStrokePadding: Float = 0F
//
//    /**
//     *  波纹轮廓宽度，默认值为 2dp
//     */
//    var waveStrokeWidth: Float = DEFAULT_WAVE_STROKE_WIDTH
//        set(value) {
//            field = value
//            mWaveStrokePaint.strokeWidth = value
//        }
//
//    /**
//     * 初始波纹半径，默认值为 0dp
//     */
//    var initialRadius: Float = 0F
//
//    /**
//     * 初始波纹透明度，取值范围为 0 - 1，默认值为 0
//     */
//    var initialAlpha: Float = DEFAULT_INITIAL_ALPHA
//
//    /**
//     * 初始波纹数量，默认值为 0
//     */
//    var initialWaveCount: Int = DEFAULT_INITIAL_WAVE_COUNT
//
//    /**
//     *  动画持续时间，单位为毫秒，默认值为 3000 毫秒
//     */
//    var duration: Long = DEFAULT_DURATION
//
//    /**
//     * 最大波纹半径，默认值为 0dp，表示根据 [maxRadiusRate] 计算
//     */
//    var maxRadius: Float = 0F
//        set(value) {
//            field = value
//            mMaxRadiusSet = true
//        }
//
//    /**
//     * 波纹创建速度，单位为毫秒，默认值为 DEFAULT_SPEED 毫秒
//     */
//    var speed: Long = DEFAULT_SPEED
//
//    /**
//     * 动画插值器
//     */
//    var interpolator: Interpolator = LinearInterpolator()
//
//    /**
//     * 自动播放
//     */
//    var autoPlay: Boolean = false
//
//    /**
//     * 自动播放波纹延迟时间，单位为毫秒，默认值为 0 毫秒
//     */
//    var autoPlayDelayMillis: Long = 0L
//
//    /**
//     *  波纹数量，默认值为 -1，表示无限
//     */
//    var waveCount: Int = DEFAULT_WAVE_COUNT
//
//    /**
//     * 波纹消失时间，单位为毫秒，默认值为 DEFAULT_FADE_DURATION 毫秒
//     */
//    var fadeDuration: Long = DEFAULT_FADE_DURATION
//
//    /**
//     *  波纹类型，默认值为 [WaveTypeEm.WAVE]
//     */
//    var waveType: WaveTypeEm = WaveTypeEm.WAVE
//
//
//    // region #私有属性
//    private var isRunning = false
//    private var mMaxRadiusSet = false
//    private var mLastCreateTime = 0L
//    private var childView: View? = null
//    private var mAvatarSize = 0 // 子控件的尺寸，取子控件的长宽最大值
//
//    private val mWavePaint = Paint(Paint.ANTI_ALIAS_FLAG)
//    private val mCircleStrokePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
//        style = Paint.Style.STROKE
//    }
//    private val mWaveStrokePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
//        style = Paint.Style.STROKE
//    }
//
//    private val mCircleList = ArrayList<Circle>()
//    private val mRecycleCircleQueue = LinkedBlockingQueue<Circle>()
//
//    private val mCreateCircle = object : Runnable {
//        override fun run() {
//            if( !isRunning ) return
//            newCircle()
//            postDelayed( this, speed )
//        }
//
//    }
//    // endregion
//
//
//    init {
//        initAttrs(attrs, defStyleAttr)
//        if (background == null) setBackgroundColor(Color.TRANSPARENT)
//    }
//
//    private fun initAttrs(attrs: AttributeSet?, defStyleAttr: Int = 0) {
//        // 使用 withStyledAttributes 方法读取 XML 属性
//        context.withStyledAttributes(attrs, R.styleable.WaveView, defStyleAttr) {
//            // 读取最大波纹半径比例
//            maxRadiusRate =
//                getFloat(R.styleable.WaveView_wv_maxRadiusRate, DEFAULT_MAX_RADIUS_RATE)
//
//            // 读取波纹颜色
//            waveColor = getColor(R.styleable.WaveView_wv_waveColor, 0)
//            // 读取波纹渐变色开始颜色
//            waveGradientStartColor =
//                getColor(R.styleable.WaveView_wv_waveGradientStartColor, Int.MAX_VALUE)
//            // 读取波纹渐变色中间颜色
//            waveGradientCenterColor =
//                getColor(R.styleable.WaveView_wv_waveGradientCenterColor, Int.MAX_VALUE)
//            // 读取波纹渐变色结束颜色
//            waveGradientEndColor =
//                getColor(R.styleable.WaveView_wv_waveGradientEndColor, Int.MAX_VALUE)
//            // 读取波纹渐变色
//            waveGradientColors = getResourceId(
//                R.styleable.WaveView_wv_waveGradientColors, 0
//            ).takeIf { it != 0 }?.let { context.resources.getIntArray(it) }
//            // 如果 wv_waveGradientColors 没有设置，则使用 start, center, end 颜色构建渐变色数组
//            if (waveGradientColors == null) { //  修正: 判断 waveGradientColors 是否为 null
//                waveGradientColors = mutableListOf<Int>().apply {
//                    if (waveGradientStartColor != Int.MAX_VALUE)
//                        add(waveGradientStartColor)
//                    if (waveGradientCenterColor != Int.MAX_VALUE)
//                        add(waveGradientCenterColor)
//                    if (waveGradientEndColor != Int.MAX_VALUE)
//                        add(waveGradientEndColor)
//                }.toIntArray()
//            }
//            // 读取波纹渐变方向
//            waveGradientOrientation = GradientOrientation.values()[
//                getInt(
//                    R.styleable.WaveView_wv_waveGradientOrientation,
//                    waveGradientOrientation.ordinal
//                )
//            ]
//
//            // 读取圆形边框颜色
//            circleStrokeColor =
//                getColor(R.styleable.WaveView_wv_circleStrokeColor, 0)
//            // 读取头像轮廓渐变色开始颜色
//            circleStrokeGradientStartColor = getColor(
//                R.styleable.WaveView_wv_circleStrokeGradientStartColor,
//                Int.MAX_VALUE
//            )
//            // 读取头像轮廓渐变色中间颜色
//            circleStrokeGradientCenterColor = getColor(
//                R.styleable.WaveView_wv_circleStrokeGradientCenterColor,
//                Int.MAX_VALUE
//            )
//            // 读取头像轮廓渐变色结束颜色
//            circleStrokeGradientEndColor = getColor(
//                R.styleable.WaveView_wv_circleStrokeGradientEndColor,
//                Int.MAX_VALUE
//            )
//            // 读取头像轮廓渐变色
//            circleStrokeGradientColors =
//                getResourceId(R.styleable.WaveView_wv_circleStrokeGradientColors, 0)
//                    .takeIf { it != 0 }?.let { context.resources.getIntArray(it) }
//            // 如果 wv_circleStrokeGradientColors 没有设置，则使用 start, center, end 颜色构建渐变色数组
//            if (circleStrokeGradientColors == null) { //  修正: 判断 circleStrokeGradientColors 是否为 null
//                circleStrokeGradientColors = mutableListOf<Int>().apply {
//                    if (circleStrokeGradientStartColor != Int.MAX_VALUE)
//                        add(circleStrokeGradientStartColor)
//                    if (circleStrokeGradientCenterColor != Int.MAX_VALUE)
//                        add(circleStrokeGradientCenterColor)
//                    if (circleStrokeGradientEndColor != Int.MAX_VALUE)
//                        add(circleStrokeGradientEndColor)
//                }.toIntArray()
//            }
//            // 读取头像轮廓渐变方向
//            circleStrokeGradientOrientation = GradientOrientation.values()[
//                getInt(
//                    R.styleable.WaveView_wv_circleStrokeGradientOrientation,
//                    circleStrokeGradientOrientation.ordinal
//                )
//            ]
//            // 读取波纹边框颜色
//            waveStrokeColor =
//                getColor(R.styleable.WaveView_wv_waveStrokeColor, waveStrokeColor)
//            // 读取圆形边框宽度
//            circleStrokeWidth = getDimension(
//                R.styleable.WaveView_wv_circleStrokeWidth,
//                circleStrokeWidth
//            )
//            // 读取圆形边框和波纹之间的间距
//            circleStrokePadding = getDimension(
//                R.styleable.WaveView_wv_circleStrokePadding,
//                circleStrokePadding
//            )
//            // 读取波纹边框宽度
//            waveStrokeWidth =
//                getDimension(R.styleable.WaveView_wv_waveStrokeWidth, waveStrokeWidth)
//            // 读取初始波纹半径
//            initialRadius =
//                getDimension(R.styleable.WaveView_wv_initialRadius, initialRadius)
//            // 读取初始波纹透明度
//            initialAlpha = getFloat(R.styleable.WaveView_wv_initialAlpha, initialAlpha)
//            // 读取初始波纹数量
//            initialWaveCount =
//                getInt(R.styleable.WaveView_wv_initialWaveCount, initialWaveCount)
//            // 读取动画持续时间
//            duration = getInt(
//                R.styleable.WaveView_wv_duration,
//                duration.toInt()
//            ).toLong()
//            // 读取最大波纹半径
//            maxRadius = getDimension(R.styleable.WaveView_wv_maxRadius, maxRadius)
//            // 读取波纹创建速度
//            speed = getInt(R.styleable.WaveView_wv_speed, DEFAULT_SPEED.toInt()).toLong()
//            // 读取波纹数量
//            waveCount = getInt(R.styleable.WaveView_wv_waveCount, waveCount)
//            // 读取波纹消失时间
//            fadeDuration =
//                getInt(
//                    R.styleable.WaveView_wv_fadeDuration,
//                    DEFAULT_FADE_DURATION.toInt()
//                ).toLong()
//            // 读取自动播放标志
//            autoPlay = getBoolean(R.styleable.WaveView_wv_autoPlay, false)
//            // 读取自动播放延迟时间
//            autoPlayDelayMillis =
//                getInt(R.styleable.WaveView_wv_autoPlayDelayMillis, 0).toLong()
//
//            // 读取波纹类型
//            val waveTypeIndex = getInt(R.styleable.WaveView_wv_waveType, WaveTypeEm.WAVE.index)
//            waveType = WaveTypeEm.values()[waveTypeIndex]
//        }
//    }
//
//    // region #公开方法
//
//    /**
//     * 开始动画
//     */
//    @JvmOverloads
//    fun start(delayMillis: Long = 0) {
//        if (isRunning) return
//        isRunning = true
//        isVisible = true
//        mCircleList.clear()
//        mRecycleCircleQueue.clear()
//
//        // 预先生成一定数量的波纹
//        for (i in 0 until initialWaveCount) {
//            newCircle()
//        }
//
//        if (delayMillis > 0)
//            postDelayed(mCreateCircle, delayMillis)
//        else
//            post(mCreateCircle)
//    }
//
//    /**
//     * 停止动画
//     */
//    @JvmOverloads
//    fun stop(delayMillis: Long = 0) {
//        if (delayMillis > 0) {
//            postDelayed({
//                removeCallbacks(mCreateCircle)
//                isRunning = false
//            }, delayMillis)
//        } else {
//            removeCallbacks(mCreateCircle)
//            isRunning = false
//        }
//    }
//    // endregion
//
//    // region #View内部方法重写
//
//    override fun onAttachedToWindow() {
//        super.onAttachedToWindow()
//        if (autoPlay) start(autoPlayDelayMillis)
//    }
//
//    override fun onDetachedFromWindow() {
//        stop()
//        super.onDetachedFromWindow()
//    }
//
//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        measureChildren(widthMeasureSpec, heightMeasureSpec)
//
//        if (childCount > 0) {
//            childView = getChildAt(0)
//            val childWidth = childView?.measuredWidth ?: 0
//            val childHeight = childView?.measuredHeight ?: 0
//            mAvatarSize = max(childWidth, childHeight)
//        }
//
//        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
//        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
//        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
//        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
//
//        val desiredWidth = (2 * (mAvatarSize / 2f + maxRadius)).toInt()
//        val desiredHeight = (2 * (mAvatarSize / 2f + maxRadius)).toInt()
//
//        val measuredWidth = when (widthMode) {
//            MeasureSpec.EXACTLY -> widthSize
//            MeasureSpec.AT_MOST -> min(widthSize, desiredWidth)
//            else -> desiredWidth
//        }
//
//        // 根据是否有子控件和高度模式来确定最终高度
//        val measuredHeight = when {
//            childView != null -> {
//                // 有子控件时，最终高度至少为子控件高度与波纹直径之和
//                max(heightSize, max(childView!!.measuredHeight, desiredHeight))
//            }
//            heightMode == MeasureSpec.EXACTLY -> heightSize // 精确模式，使用指定高度
//            heightMode == MeasureSpec.AT_MOST -> min(heightSize, desiredHeight)  // 最大模式，取最小值
//            else -> desiredHeight // 默认使用计算出的高度
//        }
//        setMeasuredDimension(measuredWidth, measuredHeight)
//    }
//
//    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
//        if (childCount > 0) {
//            val childLeft = ((width - (childView?.measuredWidth ?: 0)) / 2f).toInt()
//            val childTop = ((height - (childView?.measuredHeight ?: 0)) / 2f).toInt()
//            childView?.layout(
//                childLeft,
//                childTop,
//                childLeft + (childView?.measuredWidth ?: 0),
//                childTop + (childView?.measuredHeight ?: 0)
//            )
//        }
//    }
//
//    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
//        super.onSizeChanged(w, h, oldw, oldh)
//        if (!mMaxRadiusSet) {
//            updateMaxRadius()
//        }
//        updateWavePaintShader()
//        updateCircleStrokePaintShader()
//    }
//
//    override fun onDraw(canvas: Canvas) {
//        super.onDraw(canvas)
//        // 如果动画未运行，则不进行绘制
//        if (!isRunning) return
//
//        // 计算视图中心点的坐标
//        val widthHalf = max(width, canvas.width).toFloat() / 2f
//        val heightHalf = max(height, canvas.height).toFloat() / 2f
//        val halfAvatarSize = if (mAvatarSize == 0) 0F else mAvatarSize / 2f
//
//        // 绘制波纹
//        drawWaves(canvas, widthHalf, heightHalf)
//
//        // 绘制头像轮廓
//        drawAvatarStroke(canvas, widthHalf, heightHalf, halfAvatarSize)
//
//        // 如果还有波纹，则延迟 10 毫秒后重绘
//        if (mCircleList.size > 0) postInvalidateDelayed(10)
//    }
//
//    // 提取绘制波纹的逻辑
//    private fun drawWaves(canvas: Canvas, widthHalf: Float, heightHalf: Float) {
//        val iterator = mCircleList.iterator()
//        while (iterator.hasNext()) {
//            val circle = iterator.next()
//            // 检查波纹是否还在持续时间内
//            if (System.currentTimeMillis() - circle.mCreateTime < duration) {
//                val radius = circle.getCurrentRadius() // 获取当前波纹的半径
//                val alpha = minOf(
//                    circle.getAlpha() + (255 * 0.3F).toInt(), // 计算淡入淡出时的透明度
//                    255 // 透明度最大值
//                )
//
//                when (waveType) {
//                    WaveTypeEm.WAVE -> drawWave(canvas, widthHalf, heightHalf, radius, alpha)
//                    WaveTypeEm.STROKE -> drawStrokeWave(
//                        canvas,
//                        widthHalf,
//                        heightHalf,
//                        radius,
//                        alpha
//                    )
//                }
//            } else {
//                // 回收过期的波纹对象
//                mRecycleCircleQueue.add(circle)
//                iterator.remove()
//            }
//        }
//    }
//
//    /**
//     * 绘制填充波纹
//     */
//    private fun drawWave(canvas: Canvas, widthHalf: Float, heightHalf: Float, radius: Float, alpha: Int) {
//        mWavePaint.alpha = alpha
//        if (waveGradientColors != null && waveGradientColors!!.isNotEmpty()) { // 检查是否设置了渐变色数组
//            // 使用渐变色绘制
//            canvas.drawCircle(widthHalf, heightHalf, radius, mWavePaint)
//        } else {
//            // 使用纯色绘制
//            val tempPaint = Paint(mWavePaint).apply {
//                color = waveColor // 设置画笔颜色为 waveColor
//                shader = null // 清除临时画笔的 shader
//            }
//            canvas.drawCircle(widthHalf, heightHalf, radius, tempPaint) // 使用临时画笔绘制纯色波纹
//        }
//    }
//
//    // 绘制轮廓波纹
//    private fun drawStrokeWave(canvas: Canvas, widthHalf: Float, heightHalf: Float, radius: Float, alpha: Int) {
//        mWaveStrokePaint.color = waveStrokeColor
//        mWaveStrokePaint.strokeWidth = waveStrokeWidth
//        mWaveStrokePaint.alpha = alpha
//        canvas.drawCircle(widthHalf, heightHalf, radius, mWaveStrokePaint)
//    }
//
//
//    // 提取绘制头像轮廓的逻辑
//    private fun drawAvatarStroke(canvas: Canvas, centerX: Float, centerY: Float, halfAvatarSize: Float) {
//        if (hasCircleStroke()) {
//            mCircleStrokePaint.alpha = 255
//            if (circleStrokeGradientColors != null && circleStrokeGradientColors!!.isNotEmpty()) { // 检查是否设置了渐变色数组
//                // 使用渐变色绘制
//                canvas.drawCircle(
//                    centerX, centerY,
//                    halfAvatarSize + circleStrokeWidth + circleStrokePadding,
//                    mCircleStrokePaint
//                )
//            } else {
//                // 使用纯色绘制
//                val tempPaint = Paint(mCircleStrokePaint).apply {
//                    color = circleStrokeColor // 设置画笔颜色为 circleStrokeColor
//                    shader = null // 清除临时画笔的 shader
//                }
//                canvas.drawCircle(
//                    centerX, centerY,
//                    halfAvatarSize + circleStrokeWidth + circleStrokePadding,
//                    tempPaint
//                )
//            }
//        }
//    }
//
//    // endregion
//
//    // region #私有方法
//
//    private fun updateMaxRadius() {
//        maxRadius = (min(width, height) / 2f * maxRadiusRate).coerceAtLeast(0F)
//    }
//
//    private fun newCircle() {
//        val currentTime = System.currentTimeMillis()
//        if (currentTime - mLastCreateTime < speed) return
//        val circle = mRecycleCircleQueue.poll() ?: Circle()
//        mCircleList.add(circle.init())
//        postInvalidate()
//        mLastCreateTime = currentTime
//    }
//
//    private fun hasCircleStroke(): Boolean {
//        return circleStrokeWidth > 0 &&
//                ( circleStrokeColor != 0 || circleStrokeGradientColors?.isNotEmpty() == true )
//    }
//
//    private fun updateWavePaintShader() {
//        val colors = mutableListOf<Int>().apply {
//            if (waveGradientStartColor != Int.MAX_VALUE) add(waveGradientStartColor)
//            if (waveGradientCenterColor != Int.MAX_VALUE) add(waveGradientCenterColor)
//            if (waveGradientEndColor != Int.MAX_VALUE) add(waveGradientEndColor)
//        }.toIntArray()
//        // 确保至少有两个颜色才能创建渐变
//        if (colors.size >= 2) {
//            val widthHalf = (width / 2f)
//            val heightHalf = (height / 2f)
//            val shader = when (waveGradientOrientation) {
//                GradientOrientation.VERTICAL -> LinearGradient(
//                    widthHalf,
//                    heightHalf, // 从中心点开始
//                    widthHalf,
//                    height.toFloat(), // 到底部结束
//                    colors,
//                    null,
//                    Shader.TileMode.CLAMP
//                )
//                else -> LinearGradient(
//                    widthHalf,  // 从中心点开始
//                    heightHalf,
//                    width.toFloat(), // 到右边结束
//                    heightHalf,
//                    colors,
//                    null,
//                    Shader.TileMode.CLAMP
//                )
//            }
//
//            mWavePaint.shader = shader
//        } else {
//            mWavePaint.shader = null // 如果颜色不足两个，清除渐变
//        }
//    }
//
//    private fun updateCircleStrokePaintShader() {
//        val colors = mutableListOf<Int>().apply {
//            if (circleStrokeGradientStartColor != Int.MAX_VALUE) add(circleStrokeGradientStartColor)
//            if (circleStrokeGradientCenterColor != Int.MAX_VALUE) add(circleStrokeGradientCenterColor)
//            if (circleStrokeGradientEndColor != Int.MAX_VALUE) add(circleStrokeGradientEndColor)
//        }.toIntArray()
//        // 确保至少有两个颜色才能创建渐变
//        if (colors.size >= 2) {
//            val widthHalf = (width / 2f)
//            val heightHalf = (height / 2f)
//            val shader = when (circleStrokeGradientOrientation) {
//                GradientOrientation.VERTICAL -> LinearGradient(
//                    widthHalf,
//                    heightHalf, // 从中心点开始
//                    widthHalf,
//                    height.toFloat(), // 到底部结束
//                    colors,
//                    null,
//                    Shader.TileMode.CLAMP
//                )
//                else -> LinearGradient(
//                    widthHalf, // 从中心点开始
//                    heightHalf,
//                    width.toFloat(), // 到右边结束
//                    heightHalf,
//                    colors,
//                    null,
//                    Shader.TileMode.CLAMP
//                )
//            }
//            mCircleStrokePaint.shader = shader
//        } else {
//            mCircleStrokePaint.shader = null // 如果颜色不足两个，清除渐变
//        }
//    }
//
//    // endregion
//
//    // region #内部类
//
//    private inner class Circle {
//        var mCreateTime: Long = 0L
//
//        fun init(): Circle {
//            this.mCreateTime = System.currentTimeMillis()
//            return this
//        }
//
//        fun getAlpha(): Int {
//            val percent = (System.currentTimeMillis() - mCreateTime) * 1.0F / duration
//            return (((1.0F - interpolator.getInterpolation(percent)) * initialAlpha) * 255).toInt()
//        }
//
//        fun getCurrentRadius(): Float {
//            val percent = (System.currentTimeMillis() - mCreateTime) * 1.0F / duration
//            return initialRadius + interpolator.getInterpolation(percent) * (maxRadius - initialRadius)
//        }
//    }
//
//    // endregion
//}