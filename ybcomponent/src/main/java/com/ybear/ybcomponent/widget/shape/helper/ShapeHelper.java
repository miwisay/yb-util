//package com.ybear.ybcomponent.widget.shape;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.PaintFlagsDrawFilter;
//import android.graphics.PorterDuff;
//import android.graphics.PorterDuffXfermode;
//import android.graphics.RectF;
//import android.view.View;
//
//import androidx.annotation.ColorInt;
//import androidx.annotation.NonNull;
//
//import com.ybear.ybcomponent.R;
//import com.ybear.ybcomponent.Utils;
//
///**
// * 圆角帮助类
// */
//public class ShapeHelper implements IShape {
//    private Paint mPaint;
////    private GradientDrawable mGradientDrawable;
//    private PaintFlagsDrawFilter mDrawFilter;
//    private PorterDuffXfermode mXfermodeIn, mXfermodeOut;
//    private Canvas mShapeCanvas;
//    private RectF mRectF;
//
//    private int mShape;
//    private float mRadius/*, ltRadius, rtRadius, lbRadius, rbRadius*/;
//    private int mBorderSize;
//    @ColorInt
//    private int mBorderColor;
//    private int mShadowRadius, mShadowOffsetX, mShadowOffsetY;
//    @ColorInt
//    private int mShadowColor;
//
//    public static ShapeHelper create() { return new ShapeHelper(); }
//    public static ShapeHelper getInstance() { return H.I; }
//    private static final class H { private static final ShapeHelper I = new ShapeHelper(); }
//
//    public <T extends View> void init(@NonNull T v) {
////        if( mPaint != null ) return;
//        mPaint = new Paint();
//        //画布抗锯齿
//        mDrawFilter = new PaintFlagsDrawFilter(
//                0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG
//        );
//        //        //用于绘制圆角
////        mGradientDrawable = new GradientDrawable();
//        mRectF = new RectF();
//        mShapeCanvas = new Canvas();
//        /* 保留上层交集图像 */
//        mXfermodeIn = new PorterDuffXfermode( PorterDuff.Mode.DST_IN );
//        /* 消除上层图像 */
//        mXfermodeOut = new PorterDuffXfermode( PorterDuff.Mode.DST_OUT );
//        //启用抗锯齿
//        mPaint.setAntiAlias( true );
//        //消除画布的抗锯齿
//        mShapeCanvas.setDrawFilter( mDrawFilter );
//
//        v.setFocusable( false );
//    }
//
//    /**
//     * 刷新硬件加速类型
//     * @param v     View
//     */
//    private void updateLayerTypeHardware(@NonNull View v) {
//        //启用硬件加速。设计器和模拟器可能依旧会存在黑边
//        int hwStatus = ShapeConfig.get().getHardwareStatus();
//        if( hwStatus != -1 ) {
//            setEnableLayerTypeHardware( v, hwStatus == 1 );
//        }
//    }
//
//    @Override
//    public void setEnableLayerTypeHardware(@NonNull View view, boolean enable) {
//        int curType = view.getLayerType();
//        if( enable ) {
//            if( curType != View.LAYER_TYPE_HARDWARE ) {
//                view.setLayerType( View.LAYER_TYPE_HARDWARE, null );
//            }
//            return;
//        }
//        if( curType != View.LAYER_TYPE_SOFTWARE ) {
//            view.setLayerType( View.LAYER_TYPE_SOFTWARE, null );
//        }
//    }
//
//    @Override
//    public void setShape(@Shape int shape) { mShape = shape; }
//
//    @Override
//    public void setRadius(int radius) { mRadius = radius; }
////    void setRadius(int lt, int rt, int lb, int rb) {
////        ltRadius = lt;
////        rtRadius = rt;
////        lbRadius = lb;
////        rbRadius = rb;
////    }
//
//    @Override
//    public void setBorderSize(int borderSize) { mBorderSize = borderSize; }
//    @Override
//    public void setBorderColor(@ColorInt int color) { mBorderColor = color; }
//    @Override
//    public void setShadowRadius(int radius) { mShadowRadius = radius; }
//    @Override
//    public void setShadowColor(@ColorInt int color) { mShadowColor = color; }
//    @Override
//    public void setShadowOffsetX(int offsetX) { mShadowOffsetX = offsetX; }
//    @Override
//    public void setShadowOffsetY(int offsetY) { mShadowOffsetY = offsetY; }
//
//    public void dispatchDraw(@NonNull Canvas canvas, @NonNull View v) {
//        //刷新硬件加速
//        updateLayerTypeHardware( v );
//        int width = v.getWidth();
//        int height = v.getHeight();
////        if( width <= 0 || height <= 0 ) {
////            v.measure( 0, 0 );
////            width = v.getMeasuredWidth();
////            height = v.getMeasuredHeight();
////        }
//        if( width <= 0 || height <= 0 ) return;
//        //消除画布的抗锯齿
//        canvas.setDrawFilter( mDrawFilter );
//
//        //获取绘制圆角部分的图片
//        drawShape( canvas, width, height );
//        //绘制阴影
//        drawShadow( v.getContext(), canvas, width, height );
//        //绘制描边
//        drawStroke( canvas, width, height );
//    }
//
//    /**
//     * 绘制圆角部分的图片。
//     * @param canvas    画布
//     * @param width     高度
//     * @param height    宽度
//     */
//    private void drawShape(Canvas canvas, int width, int height) {
//        //建立一个空白图片，在里边绘制圆角
////        Bitmap shapeBmp = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );
//        Canvas shapeCanvas = getCanvas();
//        Bitmap shapeBmp = getShapeBmp( width, height );
//        //初始化矩形
//        initRectF( mBorderSize, mBorderSize, width - mBorderSize, height - mBorderSize );
//        //设置图片
//        shapeCanvas.setBitmap( shapeBmp );
//        //绘制形状
//        drawShape( shapeCanvas, mRectF );
//        //绘制时消除绘制的圆角(或者注释掉看效果就明白了)
//        mPaint.setXfermode( mXfermodeIn );
//        //绘制最终的结果
//        canvas.drawBitmap( shapeBmp, 0, 0, mPaint );
//        mPaint.setXfermode( null );
//        shapeBmp.recycle();
//    }
//
//    /**
//     * 绘制描边
//     * @param canvas    画布
//     */
//    private void drawStroke(Canvas canvas, int width, int height) {
//        if( mBorderSize == 0 ) return;
//        Canvas strokeCanvas = getCanvas();
//        Bitmap shapeBmp = getShapeBmp( width, height );
//        //初始化矩形
//        initRectF( 0, 0, width, height );
//        //描边颜色
//        mPaint.setColor( mBorderColor );
//        //设置图片
//        strokeCanvas.setBitmap( shapeBmp );
//        //绘制描边
//        drawShape( strokeCanvas, mRectF );
//
//        /* 绘制空心效果 */
//        mRectF.left += mBorderSize;
//        mRectF.top += mBorderSize;
//        mRectF.right -= mBorderSize;
//        mRectF.bottom -= mBorderSize;
//        mPaint.setXfermode( mXfermodeOut );
//        //绘制空心
//        drawShape( strokeCanvas, mRectF );
//        mPaint.setXfermode( null );
//        //提交最终结果
//        canvas.drawBitmap( shapeBmp, 0, 0, mPaint );
//        shapeBmp.recycle();
//    }
//
//    /**
//     * 绘制阴影
//     * @param canvas    画布
//     */
//    private void drawShadow(@NonNull Context context, Canvas canvas, int width, int height) {
//        if( mShadowRadius == 0 ) return;
//        int color = getShadowColor( context );
//        Canvas shadowCanvas = getCanvas();
//        Bitmap shapeBmp = getShapeBmp( width, height );
//        //初始化矩形
//        initRectF( 0, 0, width, height );
//        //设置图片
//        shadowCanvas.setBitmap( shapeBmp );
//        //创建阴影
//        mPaint.setShadowLayer( mShadowRadius, mShadowOffsetX, mShadowOffsetY, color );
//        drawShape( shadowCanvas, mRectF );
//        //关闭阴影
//        mPaint.clearShadowLayer();
//
//        float dp = Utils.dp2Px( context, 0.1F );
//        //初始化矩形
//        initRectF( -dp, -dp, width + dp, height + dp );
//        /* 绘制空心效果 */
//        mPaint.setXfermode( mXfermodeOut );
//        //绘制空心
//        drawShape( shadowCanvas, mRectF );
//        mPaint.setXfermode( null );
//        //提交最终结果
//        canvas.drawBitmap( shapeBmp, 0, 0, mPaint );
//        shapeBmp.recycle();
//    }
//
//    /**
//     * 绘制形状
//     * @param canvas    画布
//     * @param rectF     形状大小
//     */
//    private void drawShape(Canvas canvas, RectF rectF) {
//        switch ( mShape ) {
//            case Shape.NONE:            //矩形
//                canvas.drawRect( rectF, mPaint );
//                break;
//            case Shape.ROUND_RECT:      //圆角
//                canvas.drawRoundRect( rectF, mRadius, mRadius, mPaint );
//                break;
//            case Shape.OVAL:            //圆形
//                canvas.drawOval( rectF, mPaint );
//                break;
//        }
//    }
//
//    private void initRectF(float left, float top, float right, float bottom) {
//        mRectF.set(
//                left + mShadowRadius * 2,
//                top + mShadowRadius * 2,
//                right - mShadowRadius * 2,
//                bottom - mShadowRadius * 2
//        );
//    }
//
//    private Canvas getCanvas() {
//        mShapeCanvas.drawColor( Color.TRANSPARENT );
//        return mShapeCanvas;
//    }
//
//    @NonNull
//    private Bitmap getShapeBmp(int width, int height) {
//        return Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );
//    }
//
//    private int getShadowColor(Context context) {
//        if( context == null ) return Color.parseColor( "#8FA1A0A1" );
//        return mShadowColor != 0 ?
//                mShadowColor : context.getResources().getColor( R.color.colorShadow );
//    }
//
////    private int ltRadius, lbRadius, rtRadius, rbRadius;
////    private GradientDrawable mGradientDrawable = new GradientDrawable();
////    private void drawRound(Canvas canvas, RectF rectF) {
////        ltRadius = 100;
////        lbRadius = 100;
////        rtRadius = 100;
////        rbRadius = 100;
////        if( ltRadius == 0 && rtRadius == 0 && rbRadius == 0 && lbRadius == 0 ) return;
////        mGradientDrawable.setShape( GradientDrawable.RECTANGLE );
////        mGradientDrawable.setSize(
////                (int)(rectF.right - rectF.left), (int)( rectF.bottom - rectF.top )
////        );
////        mGradientDrawable.setColor( mPaint.getColor() );
////        //这里返回的一个浮点型的数组，一定要有8个元素，不然会报错
////        mGradientDrawable.setCornerRadii( new float[] {
////                ltRadius, ltRadius,
////                rtRadius, rtRadius,
////                rbRadius, rbRadius,
////                lbRadius, lbRadius
////        } );
//////        mGradientDrawable.setColor( color );
////        mGradientDrawable.draw( canvas );
////    }
//}
package com.ybear.ybcomponent.widget.shape.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Region;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.ybear.ybcomponent.R;

import java.lang.ref.WeakReference;

/**
 * 圆角帮助类
 */
public class ShapeHelper implements IShape {
    private WeakReference<View> mWrView;
    private Paint mPaint;
    private PaintFlagsDrawFilter mDrawFilter;
    private PorterDuffXfermode mXfermodeShape, mXfermodeShadow, mXfermodeStroke;
    private Canvas mShapeCanvas;
    private Path mShapePath;
    private RectF mRectF;
    private boolean isRtlLayout;

    private int mShape;
    private float[] mRadii;
    private int mBorderSize;
    @ColorInt
    private int mBorderColor;
    private int mShadowRadius, mShadowOffsetX, mShadowOffsetY;
    @ColorInt
    private int mShadowColor;

    public <T extends View> void init(@NonNull T v) {
        mWrView = new WeakReference<>( v );
        ShapeConfig config = ShapeConfig.get();
        mPaint = new Paint();
        //画布抗锯齿
        mDrawFilter = new PaintFlagsDrawFilter(
                0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG
        );
        //创建绘制的画布
        mShapeCanvas = new Canvas();
        //创建绘制的路径
        mShapePath = new Path();
        //创建绘制图像的矩阵
        mRectF = new RectF();
        /* 保留上层交集图像 */ //PorterDuff.Mode.DST_IN
        mXfermodeShape = new PorterDuffXfermode( PorterDuff.Mode.DST_IN );
        //启用抗锯齿
        mPaint.setAntiAlias( true );
        //消除画布的抗锯齿
        mShapeCanvas.setDrawFilter( mDrawFilter );
        //启用硬件加速。设计器和模拟器可能依旧会存在黑边
        int hwStatus = config.getHardwareStatus();
        if( hwStatus != -1 ) setEnableLayerTypeHardware( v, hwStatus == 1 );
        //是否启用支持RTL布局
        if( config.isEnableRtlLayout() ) {
            isRtlLayout = TextUtils.getLayoutDirectionFromLocale(
                    config.getLocale()
            ) == View.LAYOUT_DIRECTION_RTL;
        }
        v.setFocusable( false );
    }

    @Override
    public void setEnableLayerTypeHardware(@NonNull View view, boolean enable) {
        int curType = view.getLayerType();
        if( enable ) {
            if( curType != View.LAYER_TYPE_HARDWARE ) {
                view.setLayerType( View.LAYER_TYPE_HARDWARE, null );
            }
            return;
        }
        if( curType != View.LAYER_TYPE_SOFTWARE ) {
            view.setLayerType( View.LAYER_TYPE_SOFTWARE, null );
        }
    }

    @Override
    public void setShape(@Shape int shape) { mShape = shape; }

    @Override
    public void setRadius(float lt, float rt, float lb, float rb) {
        setRadii( isRtlLayout ?
                new float[] { rt, rt, lt, lt, lb, lb, rb, rb } :
                new float[] { lt, lt, rt, rt, rb, rb, lb, lb }
        );
    }

    @Override
    public void setRadius(float r) { setRadius( r, r, r, r ); }

    @Override
    public void setRadiusLRT(float r) { setRadius( r, r, 0, 0 ); }

    @Override
    public void setRadiusLRB(float r) { setRadius( 0, 0, r, r ); }

    @Override
    public void setRadiusLTB(float r) { setRadius( r, 0, r, 0 ); }

    @Override
    public void setRadiusRTB(float r) { setRadius( 0, r, 0, r ); }

    @Override
    public void setRadiusLTRB(float r) { setRadius( r, 0, 0, r ); }

    @Override
    public void setRadiusLBRT(float r) { setRadius( 0, r, r, 0 ); }

    @Override
    public void setRadii(float[] radii) { mRadii = radii; }

    @Override
    public float[] getRadii() { return mRadii; }

    @Override
    public void setBorderSize(int borderSize) { mBorderSize = borderSize; }
    @Override
    public void setBorderColor(@ColorInt int color) { mBorderColor = color; }
    @Override
    public void setShadowRadius(int radius) { mShadowRadius = radius; }
    @Override
    public void setShadowColor(@ColorInt int color) { mShadowColor = color; }
    @Override
    public void setShadowOffsetX(int offsetX) { mShadowOffsetX = offsetX; }
    @Override
    public void setShadowOffsetY(int offsetY) { mShadowOffsetY = offsetY; }

    public void dispatchDraw(@NonNull Canvas canvas, @NonNull View v) {
        int width = v.getWidth();
        int height = v.getHeight();
        if( width <= 0 || height <= 0 ) return;
        //更新Padding
        if( paddingHash != getPaddingHash() ) updatePadding();
        //消除画布的抗锯齿
        canvas.setDrawFilter( mDrawFilter );
        //如果控件允许滑动，则平移后在绘制
        canvas.translate( v.getScrollX(), v.getScrollY() );
        //获取位图
        Bitmap shapeBmp = getShapeBmp( width, height );

        //绘制描边
        drawStroke( canvas, shapeBmp );
        //获取绘制圆角部分的图片
        drawShape( canvas, shapeBmp );
        //绘制阴影
        drawShadow( v.getContext(), canvas, shapeBmp );
        //回收位图
        shapeBmp.recycle();
    }

    /**
     绘制圆角部分的图片
     @param canvas          主画布
     @param shapeBmp        绘制的图片
     */
    private void drawShape(Canvas canvas, Bitmap shapeBmp) {
        Canvas shapeCanvas = getCanvas();
        //设置图片
        shapeCanvas.setBitmap( shapeBmp );
        //初始化矩形
        initRectFOfDefault( shapeBmp, mBorderSize );
        //初始化画笔
        initPaint();
        //绘制形状（传入的颜色不能是透明，任意颜色都可以，主要用于裁剪）
        drawShape( shapeCanvas, Color.BLACK );
        //绘制时消除绘制的圆角(或者注释掉看效果就明白了)
        mPaint.setXfermode( mXfermodeShape );
        //绘制最终的结果
        drawBitmap( canvas, shapeBmp );
        mPaint.setXfermode( null );
    }

    /**
     * 绘制阴影
     * @param canvas    画布
     */
    private void drawShadow(@NonNull Context context, Canvas canvas, Bitmap shapeBmp) {
        if( mShadowRadius == 0 ) return;
        int shadowColor = getShadowColor( context );
        //初始化矩形
        initRectFOfDefault( shapeBmp, 0 );
        //初始化画笔
        initPaint();
        //创建阴影
        mPaint.setShadowLayer(
                mShadowRadius, mShadowOffsetX, mShadowOffsetY, shadowColor
        );
        //绘制阴影
        drawPath( canvas );
        //关闭阴影
        mPaint.clearShadowLayer();
    }

    /**
     * 绘制描边
     * @param canvas    画布
     */
    private void drawStroke(Canvas canvas, Bitmap shapeBmp) {
        if( mBorderSize == 0 ) return;
        Canvas strokeCanvas = getCanvas();
        //设置图片
        strokeCanvas.setBitmap( shapeBmp );
        //初始化矩形
        initRectFOfDefault( shapeBmp, 0 );
        //初始化画笔
        initPaint();
        mPaint.setStyle( Paint.Style.STROKE );
        mPaint.setStrokeWidth( mBorderSize * 2 );
        drawShape( canvas, mBorderColor );
    }

    /**
     * 绘制形状/路径
     * @param canvas        画布
     * @param paintColor    画笔颜色
     */
    private void drawShape(Canvas canvas, @ColorInt int paintColor) {
        //设置画笔颜色
        mPaint.setColor( paintColor );
        //绘制形状
        switch ( mShape ) {
            case Shape.NONE:            //矩形
                canvas.drawRect( mRectF, mPaint );
                break;
            case Shape.ROUND_RECT:      //圆角
                float[] radii = getRadii();
                if( radii == null ) break;
                mShapePath.reset();
                mShapePath.addRoundRect( mRectF, radii, Path.Direction.CW );
                canvas.drawPath( mShapePath, mPaint );
                drawClipPath( canvas, Region.Op.DIFFERENCE );
                break;
            case Shape.OVAL:            //圆形
                canvas.drawOval( mRectF, mPaint );
                break;
        }
    }

    private void drawPath(Canvas canvas) {
        //清空之前绘制的路径
        mShapePath.reset();
        //绘制形状
        switch ( mShape ) {
            case Shape.NONE:            //矩形
                mShapePath.addRect( mRectF, Path.Direction.CW );
                break;
            case Shape.ROUND_RECT:      //圆角
                float[] radii = getRadii();
                if( radii == null ) break;
                mShapePath.addRoundRect( mRectF, radii, Path.Direction.CW );
                break;
            case Shape.OVAL:            //圆形
                mShapePath.addOval( mRectF, Path.Direction.CW );
                break;
        }
        drawClipPath( canvas, Region.Op.DIFFERENCE );
    }

    private void drawClipPath(Canvas canvas, Region.Op op) {
        //裁剪路径
        canvas.clipPath( mShapePath, op );
        //绘制路径
        canvas.drawPath( mShapePath, mPaint );
        mShapePath.reset();
    }

    private void drawBitmap(Canvas canvas, Bitmap bitmap) {
        canvas.drawBitmap( bitmap, (float) 0, (float) 0, mPaint );
    }

    private int getRectWidth(int size) {
        return size == 0 ? getShadowSizeX() : size - getShadowSizeX();
    }

    private int getRectHeight(int size) {
        return size == 0 ? getShadowSizeY() : size - getShadowSizeY();
    }

    private int getShadowSizeX() { return mShadowRadius + mShadowOffsetX; }
    private int getShadowSizeY() { return mShadowRadius + mShadowOffsetY; }

    private Canvas getCanvas() {
        mShapeCanvas.drawColor( Color.TRANSPARENT );
        return mShapeCanvas;
    }

    private void initPaint() {
        mPaint.setColor( Color.TRANSPARENT );
        mPaint.setStrokeWidth( 0 );
        mPaint.setStyle( Paint.Style.FILL );
    }

    private void initRectFOfDefault(Bitmap shapeBmp, int offset) {
        float left = getRectWidth( 0 ) + offset;
        float top = getRectHeight( 0 ) + offset;
        float right = getRectWidth( shapeBmp.getWidth() ) - offset;
        float bottom = getRectHeight( shapeBmp.getHeight() ) - offset;
        mRectF.set( isRtlLayout ? right : left, top, isRtlLayout ? left : right, bottom );
    }

    @NonNull
    private Bitmap getShapeBmp(int width, int height) {
        return Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );
    }

    private int getShadowColor(Context context) {
        if( context == null ) return Color.parseColor( "#8FA1A0A1" );
        return mShadowColor != 0 ?
                mShadowColor : context.getResources().getColor( R.color.colorShadow );
    }

    public void updateRadius(TypedArray typedArray,
                             int r, int lt, int rt, int lb, int rb,
                             int lrt, int lrb, int ltb, int rtb, int ltrb, int lbrt ) {
        int rSize = typedArray.getDimensionPixelSize( r, 0 );
        /* 圆角 - 半径 */
        if( rSize > 0 ) {
            setRadius( rSize );
            return;
        }
        /* 圆角 - 水平半径 */
        rSize = typedArray.getDimensionPixelSize( lrt, 0 );
        if( rSize > 0 ) {
            setRadiusLRT( rSize );
            return;
        }
        /* 圆角 - 垂直半径 */
        rSize = typedArray.getDimensionPixelSize( lrb, 0 );
        if( rSize > 0 ) {
            setRadiusLRB( rSize );
            return;
        }
        /* 圆角 - 左半边半径 */
        rSize = typedArray.getDimensionPixelSize( ltb, 0 );
        if( rSize > 0 ) {
            setRadiusLTB( rSize );
            return;
        }
        /* 圆角 - 右半边半径 */
        rSize = typedArray.getDimensionPixelSize( rtb, 0 );
        if( rSize > 0 ) {
            setRadiusRTB( rSize );
            return;
        }
        /* 圆角 - 左上右下半径 */
        rSize = typedArray.getDimensionPixelSize( ltrb, 0 );
        if( rSize > 0 ) {
            setRadiusLTRB( rSize );
            return;
        }
        /* 圆角 - 左下右上半径 */
        rSize = typedArray.getDimensionPixelSize( lbrt, 0 );
        if( rSize > 0 ) {
            setRadiusLBRT( rSize );
            return;
        }
        //圆角 - 左上角，右上角，左下角，右下角半径
        setRadius(
                typedArray.getDimensionPixelSize( lt, 0 ),
                typedArray.getDimensionPixelSize( rt, 0 ),
                typedArray.getDimensionPixelSize( lb, 0 ),
                typedArray.getDimensionPixelSize( rb, 0 )
        );
    }

    private int paddingHash = 0;
    public synchronized void updatePadding() {
        View v = mWrView.get();
        if( v == null || v instanceof ImageView ) return;
        int se = getShadowSizeX() + mBorderSize;
        int tb = getShadowSizeY() + mBorderSize;
        v.setPaddingRelative(
                v.getPaddingStart() + se,
                v.getPaddingTop() + tb,
                v.getPaddingEnd() + se,
                v.getPaddingBottom() + tb
        );
        paddingHash = getPaddingHash();
    }

    private int getPaddingHash() {
        View v = mWrView.get();
        return v == null ? 0 : ( "" +
                v.getPaddingStart() + v.getPaddingTop() +
                v.getPaddingEnd() + v.getPaddingBottom()
        ).hashCode();
    }

//    public void initTypeArray(Context context, AttributeSet set, @StyleableRes int[] attrs,
//                              @NonNull View view) {
//        TypedArray typedArray = context.obtainStyledAttributes( set, attrs );
//        IShape iShape;
//        if( view instanceof IShape ) {
//            iShape = (IShape) view;
//        }else {
//            throw new ClassCastException();
//        }
//
//        //形状类型（圆角、圆形）
//        iShape.setShape(typedArray.getInt(
//                getStyleableId( context, view, "shape" ), Shape.ROUND_RECT
//        ));
//        //圆角
//        updateRadius(
//                typedArray,
//                getStyleableId( context, view, "shapeRadius" ),
//                getStyleableId( context, view, "shapeRadiusLT" ),
//                getStyleableId( context, view, "shapeRadiusRT" ),
//                getStyleableId( context, view, "shapeRadiusLB" ),
//                getStyleableId( context, view, "shapeRadiusRB" ),
//                getStyleableId( context, view, "shapeRadiusLRT" ),
//                getStyleableId( context, view, "shapeRadiusLRB" ),
//                getStyleableId( context, view, "shapeRadiusLTB" ),
//                getStyleableId( context, view, "shapeRadiusRTB" ),
//                getStyleableId( context, view, "shapeRadiusLTRB" ),
//                getStyleableId( context, view, "shapeRadiusLBRT" )
//        );
//        //描边大小
//        iShape.setBorderSize(typedArray.getDimensionPixelSize(
//                getStyleableId( context, view, "shapeBorderSize" ), 0
//        ));
//        //描边颜色
//        iShape.setBorderColor(typedArray.getColor(
//                getStyleableId( context, view, "shapeBorderColor" ), Color.WHITE
//        ));
//        //阴影半径
//        iShape.setShadowRadius(typedArray.getDimensionPixelSize(
//                getStyleableId( context, view, "shapeShadowRadius" ), 0
//        ));
//        //阴影颜色
//        iShape.setShadowColor(typedArray.getColor(
//                getStyleableId( context, view, "shapeShadowColor" ), 0
//        ));
//        //阴影X偏移
//        iShape.setShadowOffsetX(typedArray.getDimensionPixelSize(
//                getStyleableId( context, view, "shapeShadowOffsetX" ), 0
//        ));
//        //阴影Y偏移
//        iShape.setShadowOffsetY(typedArray.getDimensionPixelSize(
//                getStyleableId( context, view, "shapeShadowOffsetY" ), 0
//        ));
//        //复用
//        typedArray.recycle();
//
//        //防止父控件内部的子控件被遮挡
//        updatePadding( view );
//    }
//
//    private int getStyleableId(Context context, View view, String attrName) {
//        try {
//            String name = String.format("%s_%s", view.getClass().getSimpleName(), attrName );
//            Log.e("TAG", "getStyleableId -> " + name );
//            return context.getResources().getIdentifier( name, "styleable", context.getPackageName() );
//        }catch(Exception e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
}
