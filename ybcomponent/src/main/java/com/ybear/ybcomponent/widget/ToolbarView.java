package com.ybear.ybcomponent.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import com.ybear.ybcomponent.R;


public class ToolbarView extends RelativeLayout {
    private String titleText = null, backBtnText = null, otherBtnText = null;
    private int titleResId = -1;
    private Drawable backBtnSrc = null, otherBtnSrc = null;

    private View toolbar;
    private ImageView ivBackBtn, ivOtherBtn;
    private TextView tvTitle, tvBackBtn, tvOtherBtn;

    private Drawable background = null;

    public ToolbarView(Context context) {
        this(context, null);
    }

    public ToolbarView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToolbarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTypeArray(context, attrs);
        initView();
        initData();
    }

    private void initTypeArray(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ToolbarView);

        try {
            titleText = typedArray.getString(R.styleable.ToolbarView_toolbarTitle);
        } catch (Exception ignored) { }

        try {
            titleResId = typedArray.getInt(R.styleable.ToolbarView_toolbarTitle, -1);
        } catch (Exception ignored) { }

        try {
            backBtnText = typedArray.getString(R.styleable.ToolbarView_toolbarBackBtn);
        } catch (Exception ignored) { }

        try {
            backBtnSrc = typedArray.getDrawable(R.styleable.ToolbarView_toolbarBackBtnSrc);
        } catch (Exception ignored) { }

        try {
            otherBtnText = typedArray.getString(R.styleable.ToolbarView_toolbarOtherBtn);
        } catch (Exception ignored) { }

        try {
            otherBtnSrc = typedArray.getDrawable(R.styleable.ToolbarView_toolbarOtherBtnSrc);
        } catch (Exception ignored) { }

        typedArray.recycle();
    }

    private void initView() {
        toolbar = LayoutInflater.from( getContext() )
                .inflate(R.layout.toolbar, this, false);

        tvTitle = toolbar.findViewById(R.id.tb_content_tv_title);
        ivBackBtn = toolbar.findViewById(R.id.tb_content_iv_back_btn);
        tvBackBtn = toolbar.findViewById(R.id.tb_content_tv_back_btn);
        ivOtherBtn = toolbar.findViewById(R.id.tb_content_iv_other_btn);
        tvOtherBtn = toolbar.findViewById(R.id.tb_content_tv_other_btn);

        addView( toolbar );
    }

    private void initData() {
        if( titleText != null ) setTitle( titleText );
        if( titleResId != -1 ) setTitle( titleResId );

        if( backBtnText != null ) setBackBtnOfText( backBtnText );
        if( backBtnSrc != null ) setBackBtnOfImg( backBtnSrc );

        if( otherBtnText != null ) setOtherBtnOfText( otherBtnText );
        if( otherBtnSrc != null ) setOtherBtnOfImg( otherBtnSrc );


        setBackground( ContextCompat.getDrawable( getContext(), R.drawable.layer_btm_line ) );
        if( background != null ) setBackground( background );

        post(() -> {
            ViewGroup.LayoutParams lp;
            int size = getMeasuredHeight();

            lp = ivBackBtn.getLayoutParams();
            lp.width = size;
            ivBackBtn.setLayoutParams( lp );

            lp = ivOtherBtn.getLayoutParams();
            lp.width = size;
            ivOtherBtn.setLayoutParams( lp );
        });
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        ViewGroup.LayoutParams lp;
//        int size = getMeasuredHeight();
//
//        lp = ivBackBtn.getLayoutParams();
//        lp.width = size;
//        ivBackBtn.setLayoutParams( lp );
//
//        lp = ivOtherBtn.getLayoutParams();
//        lp.width = size;
//        ivOtherBtn.setLayoutParams( lp );
//    }

    public ToolbarView setTitleTextSize(float size) {
        tvTitle.setTextSize( size );
        return this;
    }
    public ToolbarView setTitleColor(@ColorRes int resId) {
        tvTitle.setTextColor( getResources().getColor( resId ) );
        return this;
    }
    public ToolbarView setTitleTypeface(Typeface tf) {
        tvTitle.setTypeface( tf );
        return this;
    }
    public ToolbarView setTitleBackgroundRes(@DrawableRes int resId) {
        tvTitle.setBackgroundResource( resId );
        return this;
    }
    public ToolbarView setTitleBackgroundColor(@ColorRes int resId) {
        tvTitle.setBackgroundColor( getResources().getColor( resId ) );
        return this;
    }

    public ToolbarView setBackBtnTextSize(float size) {
        tvBackBtn.setTextSize( size );
        return this;
    }
    public ToolbarView setBackBtnColor(@ColorRes int resId) {
        tvBackBtn.setTextColor( getResources().getColor( resId ) );
        return this;
    }
    public ToolbarView setBackBtnTypeface(Typeface tf) {
        tvBackBtn.setTypeface( tf );
        return this;
    }

    public ToolbarView setOtherBtnTextSize(float size) {
        tvOtherBtn.setTextSize( size );
        return this;
    }
    public ToolbarView setOtherBtnColor(@ColorRes int resId) {
        tvOtherBtn.setTextColor( getResources().getColor( resId ) );
        return this;
    }
    public ToolbarView setOtherBtnTypeface(Typeface tf) {
        tvOtherBtn.setTypeface( tf );
        return this;
    }

    public ToolbarView setTitle(String title) {
        tvTitle.setText( title );
        tvTitle.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setTitle(@StringRes int resId) {
        tvTitle.setText( resId );
        tvTitle.setVisibility( VISIBLE );
        return this;
    }

    public ToolbarView setBackBtnOfImg(@DrawableRes int resId) {
        ivBackBtn.setImageResource( resId );
        ivBackBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setBackBtnOfImg(@NonNull Drawable drawable) {
        ivBackBtn.setImageDrawable( drawable );
        ivBackBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setBackBtnOfText(@StringRes int resId) {
        tvBackBtn.setText( resId );
        tvBackBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setBackBtnOfText(@NonNull String s) {
        tvBackBtn.setText( s );
        tvBackBtn.setVisibility( VISIBLE );
        return this;
    }

    public ToolbarView setOtherBtnOfImg(@DrawableRes int resId) {
        ivOtherBtn.setImageResource( resId );
        ivOtherBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setOtherBtnOfImg(@NonNull Drawable drawable) {
        ivOtherBtn.setImageDrawable( drawable );
        ivOtherBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setOtherBtnOfText(@StringRes int resId) {
        tvOtherBtn.setText( resId );
        tvOtherBtn.setVisibility( VISIBLE );
        return this;
    }
    public ToolbarView setOtherBtnOfText(@NonNull String s) {
        tvOtherBtn.setText( s );
        tvOtherBtn.setVisibility( VISIBLE );
        return this;
    }

    public ToolbarView showTitle(boolean isShow) {
        tvTitle.setVisibility( isShow ? VISIBLE : INVISIBLE );
        return this;
    }
    public ToolbarView showBackBtnOfImg(boolean isShow) {
        ivBackBtn.setVisibility( isShow ? VISIBLE : INVISIBLE );
        return this;
    }
    public ToolbarView showBackBtnOfText(boolean isShow) {
        tvBackBtn.setVisibility( isShow ? VISIBLE : INVISIBLE );
        return this;
    }
    public ToolbarView showOtherBtnOfImg(boolean isShow) {
        ivOtherBtn.setVisibility( isShow ? VISIBLE : INVISIBLE );
        return this;
    }
    public ToolbarView showOtherBtnOfText(boolean isShow) {
        tvOtherBtn.setVisibility( isShow ? VISIBLE : INVISIBLE );
        return this;
    }

   public View getToolbarView() {
        return toolbar;
    }
    public TextView getTitleView() {
        return tvTitle;
    }
    public ImageView getBackBtnOfImg() {
        return ivBackBtn;
    }
    public TextView getBackBtnOfText() {
        return tvBackBtn;
    }
    public ImageView getOtherBtnOfImg() {
        return ivOtherBtn;
    }
    public TextView getOtherBtnOfText() {
        return tvOtherBtn;
    }

    public ToolbarView setOnClickTitleListener(OnClickListener listener) {
        tvTitle.setOnClickListener( listener );
        return this;
    }
    public ToolbarView setOnClickBackBtnListener(OnClickListener listener) {
        ivBackBtn.setOnClickListener( listener );
        tvBackBtn.setOnClickListener( listener );
        return this;
    }
    public ToolbarView setOnClickOtherBtnListener(OnClickListener listener) {
        ivOtherBtn.setOnClickListener( listener );
        tvOtherBtn.setOnClickListener( listener );
        return this;
    }
}
