package com.ybear.ybcomponent.widget.dialog

/**
 * 监听Dialog的生命周期
 */
interface OnDialogLifecycleListener {
    fun onResume()
    fun onPause()
}