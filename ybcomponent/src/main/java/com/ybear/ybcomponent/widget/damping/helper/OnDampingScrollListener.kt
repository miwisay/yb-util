package com.ybear.ybcomponent.widget.damping.helper

/**
 * 滑动监听接口
 */
interface OnDampingScrollListener {
    /**
     * 当阻尼效果开始时回调
     */
    fun onStart()

    /**
     * 当阻尼效果滚动时回调
     * @param x         x坐标
     * @param y         y坐标
     * @param distance 阻尼滚动的距离
     */
    fun onScroll(x: Float, y: Float, distance: Float)

    /**
     * 当阻尼效果结束时回调
     */
    fun onEnd()
}