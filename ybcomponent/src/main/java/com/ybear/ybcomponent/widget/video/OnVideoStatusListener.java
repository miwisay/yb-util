package com.ybear.ybcomponent.widget.video;

public interface OnVideoStatusListener {
    void onVideoReady();
    void onVideoPlay();
    void onVideoPause();
    void onVideoStop();
    void onVideoReset();
    void onVideoRelease();
    void onVideoCompletion(int currentPlayNum, int playTotal, boolean isCompletion);
    void onBufferingUpdate(int percent);
    void onVideoError( int what, int extra);
}