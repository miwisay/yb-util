package com.ybear.ybcomponent.widget.dialog

import android.graphics.Typeface

annotation class TypefaceStyle {
    companion object {
        const val NORMAL = Typeface.NORMAL
        const val BOLD = Typeface.BOLD
        const val ITALIC = Typeface.ITALIC
        const val BOLD_ITALIC = Typeface.BOLD_ITALIC
    }
}
