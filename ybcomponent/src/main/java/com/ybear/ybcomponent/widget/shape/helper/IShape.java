package com.ybear.ybcomponent.widget.shape.helper;

import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface IShape {
    @IntDef({ Shape.NONE, Shape.ROUND_RECT, Shape.OVAL })
    @Retention(RetentionPolicy.SOURCE)
    @interface Shape {
        int NONE = 0;
        int ROUND_RECT = 1;
        int OVAL = 2;
    }

    void setShape(@Shape int shape);
    void setRadius(float r);
    void setRadius(float lt, float rt, float lb, float rb);
    void setRadiusLRT(float r);
    void setRadiusLRB(float r);
    void setRadiusLTB(float r);
    void setRadiusRTB(float r);
    void setRadiusLTRB(float r);
    void setRadiusLBRT(float r);

    void setRadii(float[] radii);
    float[] getRadii();
    void setBorderSize(int size);
    void setBorderColor(@ColorInt int color);

    void setShadowRadius(int radius);
    void setShadowColor(@ColorInt int color);
    void setShadowOffsetX(int offsetX);
    void setShadowOffsetY(int offsetY);
    void setEnableLayerTypeHardware(@NonNull View view, boolean enable);
}
