package com.ybear.ybcomponent.widget.dialog

import android.content.DialogInterface
import androidx.annotation.StringRes

interface ButtonListener {
    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText  按钮文本内容
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(
        positiveText: String?,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(
        negativeText: String?,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText  按钮文本内容
     * @param style         按钮样式
     * @return              this
     */
    fun setOnPositiveListener(positiveText: String?, style: TextStyle?): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param style         按钮样式
     * @return              this
     */
    fun setOnNegativeListener(negativeText: String?, style: TextStyle?): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText   文本资源
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(
        positiveText: String?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(
        negativeText: String?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText   文本资源
     * @return              this
     */
    fun setOnPositiveListener(positiveText: String?): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @return              this
     */
    fun setOnNegativeListener(negativeText: String?): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(
        @StringRes positiveRes: Int, style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(
        @StringRes negativeRes: Int,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param style         按钮样式
     * @return              this
     */
    fun setOnPositiveListener(@StringRes positiveRes: Int, style: TextStyle?): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param style         按钮样式
     * @return              this
     */
    fun setOnNegativeListener(@StringRes negativeRes: Int, style: TextStyle?): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(
        @StringRes positiveRes: Int,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(
        @StringRes negativeRes: Int,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @return              this
     */
    fun setOnPositiveListener(@StringRes positiveRes: Int): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @return              this
     */
    fun setOnNegativeListener(@StringRes negativeRes: Int): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton?

    /**
     * 设置确定按钮点击事件监听器
     * @param l             事件监听器
     * @return              this
     */
    fun setOnPositiveListener(l: DialogInterface.OnClickListener?): DialogButton?

    /**
     * 设置取消按钮点击事件监听器
     * @param l             事件监听器
     * @return              this
     */
    fun setOnNegativeListener(l: DialogInterface.OnClickListener?): DialogButton?
}