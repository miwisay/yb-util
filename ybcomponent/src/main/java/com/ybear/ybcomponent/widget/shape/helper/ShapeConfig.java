package com.ybear.ybcomponent.widget.shape.helper;

import java.util.Locale;

public class ShapeConfig {
    private int mEnableHardware = 0;
    private Locale mLocale = Locale.getDefault();
    private boolean mEnableRtlLayout = false;

    private ShapeConfig() {
        enableHardware();
    }
    public static ShapeConfig get() { return HANDLER.I; }
    public static final class HANDLER { private static final ShapeConfig I = new ShapeConfig(); }

    public ShapeConfig setLocale(Locale locale) {
        mLocale = locale;
        return this;
    }
    public Locale getLocale() { return mLocale; }

    /**
     * 启用支持RTL布局
     */
    public void enableSupportRtlLayout() { mEnableRtlLayout = true; }

    /**
     * 禁用支持RTL布局
     */
    public void disabledSupportRtlLayout() { mEnableRtlLayout = false; }

    public boolean isEnableRtlLayout() { return mEnableRtlLayout; }

    /**
     * 启用硬件加速
     * 全部Shape控件都会启用
     * 默认选项
     * @return      this
     */
    public ShapeConfig enableHardware() {
        mEnableHardware = 1;
        return this;
    }

    /**
     * 关闭硬件加速
     * 全部Shape控件都会关闭
     * @return      this
     */
    public ShapeConfig displayHardware() {
        mEnableHardware = 0;
        return this;
    }

    /**
     * 跳过硬件加速
     * 全部Shape控件会忽略
     * 开发者自行设置硬件加速
     * @return      this
     */
    public ShapeConfig skipHardware() {
        mEnableHardware = -1;
        return this;
    }

    public int getHardwareStatus() { return mEnableHardware; }
}
