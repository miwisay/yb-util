package com.ybear.ybcomponent.widget.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Space
import androidx.annotation.StringRes
import com.ybear.ybcomponent.R
import com.ybear.ybcomponent.Utils

open class DialogButton internal constructor(context: Context, dialog: Dialog) : ButtonListener {
    private val mContext: Context?
    private val mDialog: Dialog
    private var positiveText: String? = null
    private var negativeText: String? = null
    private var positiveStyle = TextStyle()
    private var negativeStyle = TextStyle()
    private var positiveClickListener: DialogInterface.OnClickListener? = null
    private var negativeClickListener: DialogInterface.OnClickListener? = null

    init {
        mContext = context
        mDialog = dialog
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText  按钮文本内容
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(
        positiveText: String?,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        this.positiveText = positiveText ?: oKText
        if (style != null) positiveStyle = style
        positiveClickListener = l
        return this
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(
        negativeText: String?,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        this.negativeText = negativeText ?: cancelText
        if (style != null) negativeStyle = style
        negativeClickListener = l
        return this
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText  按钮文本内容
     * @param style         按钮样式
     * @return              this
     */
    override fun setOnPositiveListener(
        positiveText: String?,
        style: TextStyle?
    ): DialogButton {
        this.positiveText = positiveText ?: oKText
        if (style != null) positiveStyle = style
        positiveClickListener =
            DialogInterface.OnClickListener { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        return this
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param style         按钮样式
     * @return              this
     */
    override fun setOnNegativeListener(
        negativeText: String?,
        style: TextStyle?
    ): DialogButton {
        this.negativeText = negativeText ?: cancelText
        if (style != null) negativeStyle = style
        negativeClickListener =
            DialogInterface.OnClickListener { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        return this
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText   文本资源
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(
        positiveText: String?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnPositiveListener(positiveText, null, l)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(
        negativeText: String?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnNegativeListener(negativeText, null, l)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveText   文本资源
     * @return              this
     */
    override fun setOnPositiveListener(positiveText: String?): DialogButton {
        return setOnPositiveListener(positiveText, null as TextStyle?)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeText  按钮文本内容
     * @return              this
     */
    override fun setOnNegativeListener(negativeText: String?): DialogButton {
        return setOnNegativeListener(negativeText, null as TextStyle?)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(
        @StringRes positiveRes: Int,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnPositiveListener(
            if (mContext != null) getString(positiveRes) else null,
            style,
            l
        )
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(
        @StringRes negativeRes: Int,
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnNegativeListener(
            if (mContext != null) getString(negativeRes) else null,
            style,
            l
        )
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param style         按钮样式
     * @return              this
     */
    override fun setOnPositiveListener(
        @StringRes positiveRes: Int,
        style: TextStyle?
    ): DialogButton {
        return setOnPositiveListener(if (mContext != null) getString(positiveRes) else null, style)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param style         按钮样式
     * @return              this
     */
    override fun setOnNegativeListener(
        @StringRes negativeRes: Int,
        style: TextStyle?
    ): DialogButton {
        return setOnNegativeListener(if (mContext != null) getString(negativeRes) else null, style)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(
        @StringRes positiveRes: Int,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnPositiveListener(positiveRes, null, l)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(
        @StringRes negativeRes: Int,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnNegativeListener(negativeRes, null, l)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param positiveRes   文本资源
     * @return              this
     */
    override fun setOnPositiveListener(@StringRes positiveRes: Int): DialogButton {
        return setOnPositiveListener(positiveRes, null as TextStyle?)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param negativeRes   文本资源
     * @return              this
     */
    override fun setOnNegativeListener(@StringRes negativeRes: Int): DialogButton {
        return setOnNegativeListener(negativeRes, null as TextStyle?)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnPositiveListener(null, style, l)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param style         按钮样式
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(
        style: TextStyle?,
        l: DialogInterface.OnClickListener?
    ): DialogButton {
        return setOnNegativeListener(null, style, l)
    }

    /**
     * 设置确定按钮点击事件监听器
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnPositiveListener(l: DialogInterface.OnClickListener?): DialogButton {
        return setOnPositiveListener(null, null, l)
    }

    /**
     * 设置取消按钮点击事件监听器
     * @param l             事件监听器
     * @return              this
     */
    override fun setOnNegativeListener(l: DialogInterface.OnClickListener?): DialogButton {
        return setOnNegativeListener(null, null, l)
    }

    open fun createButton(): View? {
        if (mContext == null) return null
        val layout = LinearLayout(mContext)
        val lp = LinearLayout.LayoutParams(
            ( mContext.resources.displayMetrics.widthPixels / 1.4F ).toInt(),
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        lp.gravity = Gravity.TOP or Gravity.END
        layout.layoutParams = lp
        layout.setPadding(
            Utils.dp2Px(mContext, DialogInit.get().buttonLayoutLeftPadding),
            Utils.dp2Px(mContext, DialogInit.get().buttonLayoutTopPadding),
            Utils.dp2Px(mContext, DialogInit.get().buttonLayoutRightPadding),
            Utils.dp2Px(mContext, DialogInit.get().buttonLayoutBottomPadding)
        )
        layout.gravity = Gravity.TOP or Gravity.END
        //按钮的布局参数
        val btnLP = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
//        //占位布局
//        val space = Space( mContext )
//        btnLP.weight = 1F
//        space.layoutParams = btnLP
//        layout.addView( space )

        if (positiveText != null) {
            val v = positiveStyle
                .setPadding(Utils.dp2Px(mContext, DialogInit.get().buttonPadding))
                .setOnClickListener { _: View? ->
                    //回调确定按钮事件监听器
                    if (positiveClickListener == null) return@setOnClickListener
                    positiveClickListener!!.onClick(mDialog, DialogInterface.BUTTON_POSITIVE)
                }
                .createView(mContext, positiveText)
            btnLP.rightMargin = Utils.dp2Px(mContext, DialogInit.get().buttonRightMargin)
            btnLP.weight = 0F
            v.layoutParams = btnLP
            v.setBackgroundResource(R.drawable.selector_def_btn)
            v.isFocusable = true
            v.isClickable = true
            layout.addView(v)
        }
        if (negativeText != null) {
            val v = negativeStyle
                .setPadding(Utils.dp2Px(mContext, DialogInit.get().buttonPadding))
                .setOnClickListener { _: View? ->
                    //回调取消按钮事件监听器
                    if (negativeClickListener == null) return@setOnClickListener
                    negativeClickListener!!.onClick(mDialog, DialogInterface.BUTTON_NEGATIVE)
                }
                .createView(mContext, negativeText)
            btnLP.rightMargin = Utils.dp2Px(mContext, DialogInit.get().buttonRightMargin)
            btnLP.weight = 0F
            v.layoutParams = btnLP
            v.setBackgroundResource(R.drawable.selector_def_btn)
            v.isFocusable = true
            v.isClickable = true
            layout.addView(v)
        }
        return layout
    }

    private fun getString(resId: Int): String? {
        return mContext?.getString(resId)
    }

    private val oKText: String? get() = getString(R.string.stringOk)
    private val cancelText: String? get() = getString(R.string.stringCancel)
}