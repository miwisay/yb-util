package com.ybear.ybcomponent.widget.dialog

import android.view.View
import android.view.ViewGroup

open class Create<V : View?> {
    var width = 0
        private set
    var height = 0
        private set
    var layout: V
        private set
    var layoutParams: ViewGroup.LayoutParams? = null
        private set
    var isFree = false
        private set

    @JvmOverloads
    constructor(view: V, lp: ViewGroup.LayoutParams? = null) {
        layout = view
        layoutParams = lp
    }

    constructor(view: V, width: Int, height: Int) {
        layout = view
        this.width = width
        this.height = height
    }

    open fun setWidth(width: Int): Create<*> {
        this.width = width
        return this
    }

    open fun setHeight(height: Int): Create<*> {
        this.height = height
        return this
    }

    fun setLayout(layout: V): Create<*> {
        this.layout = layout
        return this
    }

    open fun setLayoutParams(lp: ViewGroup.LayoutParams?): Create<*> {
        layoutParams = lp
        return this
    }

    open fun setFree(free: Boolean): Create<V> {
        isFree = free
        return this
    }
}