package com.ybear.ybcomponent.widget.damping.helper

import android.widget.LinearLayout

@Retention(AnnotationRetention.SOURCE)
annotation class Orientation {
    companion object {
        const val HORIZONTAL: Int = LinearLayout.HORIZONTAL
        const val VERTICAL: Int = LinearLayout.VERTICAL
    }
}
