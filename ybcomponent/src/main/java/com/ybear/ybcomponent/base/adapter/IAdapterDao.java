package com.ybear.ybcomponent.base.adapter;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAdapterDao<E extends IItemData> extends IItemDataDao<E> {
    int getItemDataHashCode(int position);

    int getDataListHashCode();

    boolean isEmpty();

    void sort(Comparator<? super E> c);

    List<E> subList(int fromPotion, int toPotion);

    boolean containsAll(Collection<E> c);
}