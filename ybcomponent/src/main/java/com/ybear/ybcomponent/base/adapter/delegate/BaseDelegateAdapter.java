package com.ybear.ybcomponent.base.adapter.delegate;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.BaseRecyclerViewAdapter;
import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public abstract class BaseDelegateAdapter<E extends IItemData, H extends BaseViewHolder> {
    private BaseRecyclerViewAdapter<E, H> mAdapter;

    public void initAdapter(@NonNull BaseRecyclerViewAdapter<E, H> adapter) { mAdapter = adapter; }

    public BaseRecyclerViewAdapter<E, H> getAdapter() { return mAdapter; }

    public abstract void onBindViewHolder(@NonNull H viewHolder, int position);
}