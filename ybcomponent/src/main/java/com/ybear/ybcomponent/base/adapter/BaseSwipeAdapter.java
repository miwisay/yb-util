package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.delegate.DelegateSwipe;
import com.ybear.ybcomponent.base.adapter.delegate.IDelegateSwipe;
import com.ybear.ybcomponent.base.adapter.listener.OnCreateItemSwipeLayoutListener;
import com.ybear.ybcomponent.base.adapter.listener.OnSwipeItemClickListener;
import com.ybear.ybcomponent.widget.ItemSwipeLayout;

import java.util.List;

/**
 * 适配器的基础上增加侧滑布局
 * @param <E>
 */
public abstract class BaseSwipeAdapter<E extends IItemData, H extends BaseViewHolder>
        extends BaseRecyclerViewAdapter<E, H> implements IDelegateSwipe<E, H>,
        OnCreateItemSwipeLayoutListener<H> {

    private DelegateSwipe<E, H> mDelegate;

    public BaseSwipeAdapter(@NonNull List<E> list, boolean enableSwipeDrag) {
        super(list);
        getDelegate().init( enableSwipeDrag, this );
        getDelegate().initAdapter( this );
    }

    private DelegateSwipe<E, H> getDelegate() {
        return mDelegate == null ? mDelegate = DelegateSwipe.create() : mDelegate;
    }

    @Override
    public void onBindViewHolder(@NonNull H viewHolder, int position) {
        getDelegate().onBindViewHolder( viewHolder, position );
        super.onBindViewHolder( viewHolder, position );
    }

    @Override
    public void setOnSwipeItemClickListener(OnSwipeItemClickListener<E, H> l) {
        getDelegate().setOnSwipeItemClickListener( l );
    }

    @Override
    public void setOnDragStateChangedListener(ItemSwipeLayout.OnDragStateChangedListener l) {
        getDelegate().setOnDragStateChangedListener( l );
    }

    @NonNull
    @Override
    public abstract ItemSwipeLayout onCreateItemSwipeLayout(@NonNull H viewHolder, int position);
}