package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public interface IItemDataDao<E extends IItemData> {
    @Nullable
    E getItemData(int position);

    @Nullable
    E getItemDataOfLast();

    @Nullable
    E getItemDataOfFirst();

    @NonNull
    List<E> getDataList();

    boolean addItemData(int position, E data);

    boolean addItemData(int position, List<E> data);

    boolean addItemData(E data);

    boolean addItemData(List<E> data);

    void setItemData(List<E> data);

    E setItemData(int position, E data);

    E removeItemData(int position);

    E removeItemData(E data);

    void clearItemData();
}
