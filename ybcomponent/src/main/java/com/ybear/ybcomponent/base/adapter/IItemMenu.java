package com.ybear.ybcomponent.base.adapter;

import android.widget.PopupMenu;

/**
 * Item 长按弹出的菜单
 * @param <E>
 */
public interface IItemMenu<E extends IItemData> {
    void callMenuAttr(PopupMenu menu, E itemData);
}
