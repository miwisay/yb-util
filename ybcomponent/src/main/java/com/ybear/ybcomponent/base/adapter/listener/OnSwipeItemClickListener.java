package com.ybear.ybcomponent.base.adapter.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.widget.ItemSwipeLayout;

public interface OnSwipeItemClickListener<E extends IItemData, H extends BaseViewHolder>  {
    void onClick(RecyclerView.Adapter<H> adapter, ItemSwipeLayout view, View childView,
                 int swipePosition, E data, int itemPosition);
}
