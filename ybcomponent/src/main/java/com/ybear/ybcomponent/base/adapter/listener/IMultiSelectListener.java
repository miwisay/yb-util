package com.ybear.ybcomponent.base.adapter.listener;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.listener.OnMultiSelectChangeListener;

public interface IMultiSelectListener<H extends BaseViewHolder> extends
        OnMultiSelectChangeListener<H> {

    /**
     * 绑定Holder
     * @param holder        绑定的Holder
     * @param position      当前位置
     * @param isSelected    是否选中
     */
    void onBindViewHolder(@NonNull H holder, int position, boolean isSelected);
}
