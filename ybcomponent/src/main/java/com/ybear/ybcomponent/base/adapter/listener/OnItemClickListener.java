package com.ybear.ybcomponent.base.adapter.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public interface OnItemClickListener<E extends IItemData, H extends BaseViewHolder> {
    void onItemClick(RecyclerView.Adapter<H> adapter, View v, E data, int position);
}
