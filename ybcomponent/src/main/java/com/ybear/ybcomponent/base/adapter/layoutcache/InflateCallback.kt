package com.ybear.ybcomponent.base.adapter.layoutcache

import android.view.View

/**
 * @Author MiWi
 * @Date 2024年1月24日
 * @Description 布局优化-回调
 */
interface InflateCallback {
    fun onInflateFinished(layoutId: Int, view: View?)
}