package com.ybear.ybcomponent.base.adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ybear.ybcomponent.base.adapter.listener.IScrollStuckCallback;

import java.util.List;

/**
 * 防止滑动时卡顿的RecyclerView适配器
 * @param <E>
 * @param <H>
 */
public abstract class BaseScrollStuckRecyclerViewAdapter<E extends IItemData, H extends BaseViewHolder>
        extends BaseRecyclerViewAdapter<E, H> implements IScrollStuckCallback<E, H> {
    private boolean isInitRecyclerView = false;

    public BaseScrollStuckRecyclerViewAdapter(@Nullable RecyclerView rv) {
        initRecyclerView( rv );
    }

    public BaseScrollStuckRecyclerViewAdapter(@Nullable RecyclerView rv, @NonNull List<E> list) {
        super(list);
        initRecyclerView( rv );
    }

    private void initRecyclerView(RecyclerView rv) {
        if( !( isInitRecyclerView = rv != null ) ) return;
        //关闭改变动画
        RecyclerView.ItemAnimator animator = rv.getItemAnimator();
        if( animator != null ) animator.setChangeDuration( 0 );
        //
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                bindOnPageScrollStateChanged( newState );
            }
        });
    }

    @Override
    public void bindViewPager(ViewPager viewPager) {
        if( viewPager == null ) return;
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) { }

            @Override
            public void onPageScrollStateChanged(int state) {
                bindOnPageScrollStateChanged( state );
            }
        });
    }

    @Override
    public void bindOnPageScrollStateChanged(int state) {
        //state == ViewPager.SCROLL_STATE_IDLE
        if( state == 0 ) {
            resumeViewHolder();
        }else {
            pauseViewHolder();
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull H holder) {
        super.onViewAttachedToWindow(holder);
        designateViewHolder( holder, true );
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull H holder) {
        super.onViewDetachedFromWindow(holder);
        designateViewHolder( holder, false );
    }

    @Override
    public void resumeViewHolder() {
        forEachViewHolder( true );
    }

    @Override
    public void pauseViewHolder() {
        forEachViewHolder( false );
    }

    @SuppressLint("NotifyDataSetChanged")
    private void forEachViewHolder(boolean isResume) {
        List<H> holderList = getHolderList();
        if( holderList == null ) return;
        for (H h : holderList) designateViewHolder( h, isResume );
        notifyDataSetChanged();
//        int minPos = 0;
//        int maxPos = 0;
//        for (H h : holderList) {
//            int pos = h.getLayoutPosition();
//            designateViewHolder( h, isResume );
//            if( isInitRecyclerView ) {
//                minPos = Math.min( minPos, pos );
//                maxPos = Math.max( maxPos, pos );
//            }
//        }
//        if( isInitRecyclerView ) {
//            if( minPos == 0 && maxPos == 0 ) return;
//            notifyItemRangeChanged( minPos, maxPos + 1 );
//        }else {
//            notifyDataSetChanged();
//        }
    }

    private void designateViewHolder(H h, boolean isResume) {
        int pos = h.getLayoutPosition();
        E data = getItemData( pos );
        if( isResume ) {
            onBindViewHolder( h, data, pos );
        }else {
            onUnBindViewHolder( h, data, pos );
        }
    }


    @Deprecated
    @Override
    public void onBindViewHolder(@NonNull H holder, int position) {
        super.onBindViewHolder(holder, position);
    }

    @Deprecated
    @Override
    public void onBindViewHolder(@NonNull H holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }
}
