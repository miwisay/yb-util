package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({ ItemClickType.SWITCH, ItemClickType.ENABLE, ItemClickType.DISABLE })
@Retention(RetentionPolicy.SOURCE)
public @interface ItemClickType {
    int SWITCH = 0;
    int ENABLE = 1;
    int DISABLE = 2;
}