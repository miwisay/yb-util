package com.ybear.ybcomponent.base.adapter.listener;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;

public class OnMultiSelectChangeAdapter<H extends BaseViewHolder>
        implements OnMultiSelectChangeListener<H> {

    /**
     * 初始化多选状态
     * @param position      初始化的位置
     * @return              是否选中
     */
    @Override
    public boolean onInitMultiSelect(int position) { return false; }

    /**
     * 多选发生改变时
     * @param adapter       this
     * @param holder        Holder
     * @param position      选中的位置
     * @param isChecked     是否选中
     * @param fromUser      是否为用户发起
     */
    @Override
    public void onMultiSelectChange(RecyclerView.Adapter<H> adapter, @Nullable H holder,
                                    int position, boolean isChecked, boolean fromUser) { }

    /**
     * 全选状态
     * @param adapter       适配器
     * @param isSelectAll   是否为全选状态
     * @param fromUser      是否为用户发起
     */
    @Override
    public void onMultiSelectAll(RecyclerView.Adapter<H> adapter, boolean isSelectAll,
                                 boolean fromUser) { }
}
