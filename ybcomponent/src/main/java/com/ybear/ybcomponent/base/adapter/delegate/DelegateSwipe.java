package com.ybear.ybcomponent.base.adapter.delegate;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.listener.OnCreateItemSwipeLayoutListener;
import com.ybear.ybcomponent.base.adapter.listener.OnSwipeItemClickListener;
import com.ybear.ybcomponent.widget.ItemSwipeLayout;

public class DelegateSwipe<E extends IItemData, H extends BaseViewHolder>
        extends BaseDelegateAdapter<E, H>
        implements IDelegateSwipe<E, H> {

    public  static <E extends IItemData, H extends BaseViewHolder> DelegateSwipe<E, H> create() {
        return new DelegateSwipe<>();
    }

    private OnCreateItemSwipeLayoutListener<H> mOnCreateItemSwipeLayoutListener;
    private OnSwipeItemClickListener<E, H> mOnSwipeItemClickListener;
    private ItemSwipeLayout.OnDragStateChangedListener mOnDragStateChangedListener;
    //启用/禁用侧滑
    private boolean enableSwipeDrag;

    public void init(boolean enable, OnCreateItemSwipeLayoutListener<H> l) {
        enableSwipeDrag = enable;
        mOnCreateItemSwipeLayoutListener = l;
    }

    public ItemSwipeLayout getItemSwipeLayout(@NonNull H viewHolder, int position) {
        return mOnCreateItemSwipeLayoutListener.onCreateItemSwipeLayout( viewHolder, position );
    }

    /**
     * 委托处理BindViewHolder
     * @param viewHolder    holder
     * @param position      位置
     */
    @Override
    public void onBindViewHolder(@NonNull H viewHolder, int position) {
        ItemSwipeLayout mItemSwipeLayout = getItemSwipeLayout( viewHolder, position );
        E data = getAdapter().getDataList().get( position );

        //设置点击效果的Item
        viewHolder.setTouchStyleView( mItemSwipeLayout.getItemView() );
        //拖拽状态发生改变事件监听器
        mItemSwipeLayout.setOnDragStateChangedListener((itemSwipeLayout, state) -> {
            if( mOnDragStateChangedListener != null ) {
                mOnDragStateChangedListener.onChanged(itemSwipeLayout, state);
            }
        });
        //侧滑Item点击事件监听器
        mItemSwipeLayout.setOnSwipeItemClickListener((itemSwipeLayout, view, i) -> {
            if( mOnSwipeItemClickListener != null ) {
                mOnSwipeItemClickListener.onClick(
                        getAdapter(), itemSwipeLayout, view, i, data, position
                );
            }
        });
        //侧滑控件点击事件监听器
        mItemSwipeLayout.setOnClickListener(
                v -> getAdapter().onItemClick( getAdapter(), v, data, position ) );
        //是否允许启用侧滑
        mItemSwipeLayout.setEnableSwipeDrag( enableSwipeDrag );
    }

    /**
     * 设置侧滑Item点击事件监听器
     * @param l  监听器
     */
    @Override
    public void setOnSwipeItemClickListener(OnSwipeItemClickListener<E, H> l) {
        mOnSwipeItemClickListener = l;
    }

    /**
     * 拖拽状态发生改变时事件监听器
     * @param l  监听器
     */
    @Override
    public void setOnDragStateChangedListener(ItemSwipeLayout.OnDragStateChangedListener l) {
        mOnDragStateChangedListener = l;
    }
}
