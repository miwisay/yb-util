package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.DrawableRes;

/**
 * 点击样式接口
 */
public interface IItemTouchStyle {
    @DrawableRes
    int onTouchStyle();
}
