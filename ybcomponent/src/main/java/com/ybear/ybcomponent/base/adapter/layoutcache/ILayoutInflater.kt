package com.ybear.ybcomponent.base.adapter.layoutcache

import android.view.View
import android.view.ViewGroup

/**
 * @Author MiWi
 * @Date 2024年1月24日
 * @Description 布局优化-接口
 */
interface ILayoutInflater {
    /**
     * 异步加载View
     *
     * @param parent   父布局
     * @param layoutId 布局资源id
     * @param callback 加载回调
     */
    fun asyncInflateView(parent: ViewGroup, layoutId: Int, callback: InflateCallback?)

    /**
     * 同步加载View
     *
     * @param parent   父布局
     * @param layoutId 布局资源id
     * @return 加载的View
     */
    fun inflateView(parent: ViewGroup, layoutId: Int): View?
}