package com.ybear.ybcomponent.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.OnTouchListener;
import com.ybear.ybcomponent.Utils;

/**
 * 为{@link BaseRecyclerViewAdapter}使用的Holder
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {
    private final Context mContext;

    /*
    * touchStyleView 允许点击样式可以不是同一个View，
    * 因为Item可能会通过FrameLayout做层叠，点击效果是顶层，此时，底层的点击就不可见了。
    * */
    @NonNull
    private View mTouchStyleView;
    private final ViewGroup.LayoutParams mLayoutParams;

    private boolean isShow = true;

    private View.OnClickListener mClickListener;
    private View.OnLongClickListener mLongClickListener;

    public BaseViewHolder(@NonNull ViewGroup parent, @LayoutRes int itemRes) {
        this(LayoutInflater.from( parent.getContext() )
                .inflate(itemRes, parent, false)
        );
    }
    public BaseViewHolder(@NonNull ViewGroup parent, @LayoutRes int itemRes,
                          @LayoutRes int touchRes) {
        this(LayoutInflater.from( parent.getContext() )
                        .inflate(itemRes, parent, false),
                LayoutInflater.from( parent.getContext() )
                        .inflate(touchRes, parent, false)
        );
    }
    public BaseViewHolder(@NonNull View itemView) {
        this(itemView, itemView);
    }
    public BaseViewHolder(@NonNull View itemView, @NonNull View touchStyleView) {
        super(itemView);
        //用于按下的样式View默认是ItemView
        mTouchStyleView = touchStyleView;
        mContext = itemView.getContext();
        mLayoutParams = itemView.getLayoutParams();

        touchStyleView.setOnClickListener(v -> {
            if( mClickListener != null ) mClickListener.onClick( v );
        });
        touchStyleView.setOnLongClickListener(v -> {
            if( mLongClickListener != null ) return mLongClickListener.onLongClick( v );
            return false;
        });
    }
    
    public Context getContext() { return mContext; }

    /**
     * 显示View
     */
    public void showItemView() {
        View v = getItemView();
        v.setLayoutParams( mLayoutParams );
        v.setVisibility( View.VISIBLE );
        isShow = true;
    }

    /**
     * 隐藏View
     * @param isGone    是否完全隐藏
     */
    public void hideItemView(boolean isGone) {
        View v = getItemView();
        if( isGone ) {
            ViewGroup.LayoutParams lp = v.getLayoutParams();
            lp.width = lp.height = 0;
        }
        v.setVisibility( isGone ? View.GONE : View.INVISIBLE );
        isShow = false;
    }

    /**
     * 完全隐藏View
     */
    public void hideItemView() { hideItemView( true ); }

    /**
     * View是否处于显示状态
     * @return      状态
     */
    public boolean isShow() { return isShow; }

    /**
     * 获取View中的子View
     * @param id    子View id资源
     * @param <T>   子View泛型
     * @return      子View
     */
    public final <T extends View> T findViewById(@IdRes int id) {
        return itemView.findViewById( id );
    }

    @NonNull
    public View getItemView() { return itemView; }

    @NonNull
    public View getTouchStyleView() { return mTouchStyleView; }

    /**
     * 设置按下的样式View
     * @param v     默认：{@link #itemView}
     */
    public void setTouchStyleView(@NonNull View v) { mTouchStyleView = v; }

    /**
     * 优化版Touch
     * @param l     监听器
     */
    public void setOnSuperTouchListener(OnTouchListener l) {
        Utils.setOnSuperTouchListener( itemView, l );
    }

    /**
     * 原版Touch
     * @param l     监听器
     */
    public void setOnTouchListener(View.OnTouchListener l) {
        itemView.setOnTouchListener( l );
    }

    /**
     * 设置点击事件监听器
     * @param l     监听器
     */
    public void setOnClickListener(View.OnClickListener l) {
        mClickListener = l;
    }

    /**
     * 设置长按事件监听器
     * @param l     监听器
     */
    public void setOnLongClickListener(View.OnLongClickListener l) {
        mLongClickListener = l;
    }
}
