package com.ybear.ybcomponent.base.adapter.delegate;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.listener.OnSwipeItemClickListener;
import com.ybear.ybcomponent.widget.ItemSwipeLayout;

public interface IDelegateSwipe<E extends IItemData, H extends BaseViewHolder> {
    /**
     * 设置侧滑Item点击事件监听器
     * @param l  监听器
     */
    void setOnSwipeItemClickListener(OnSwipeItemClickListener<E, H> l);

    /**
     * 拖拽状态发生改变时事件监听器
     * @param l  监听器
     */
    void setOnDragStateChangedListener(ItemSwipeLayout.OnDragStateChangedListener l);
}
