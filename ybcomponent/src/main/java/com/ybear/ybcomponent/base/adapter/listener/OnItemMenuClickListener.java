package com.ybear.ybcomponent.base.adapter.listener;

import android.view.MenuItem;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public interface OnItemMenuClickListener<E extends IItemData, H extends BaseViewHolder> {
    boolean onItemMenuClick(
            RecyclerView.Adapter<H> adapter, View view, MenuItem menuItem, E itemData, int position
    );
}
