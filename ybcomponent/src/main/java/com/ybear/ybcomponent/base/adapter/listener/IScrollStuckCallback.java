package com.ybear.ybcomponent.base.adapter.listener;

import androidx.viewpager.widget.ViewPager;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public interface IScrollStuckCallback<E extends IItemData, H extends BaseViewHolder> {
    void onBindViewHolder(H holder, E data, int pos);
    void onUnBindViewHolder(H holder, E data, int pos);
    void resumeViewHolder();
    void pauseViewHolder();

    void bindViewPager(ViewPager viewPager);
    void bindOnPageScrollStateChanged(int state);
}