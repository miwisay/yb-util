package com.ybear.ybcomponent.base.adapter.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public interface OnItemDownListener<E extends IItemData, H extends BaseViewHolder> {
    void onItemDown(RecyclerView.Adapter<H> adapter, View view, E itemData, int position);
}
