package com.ybear.ybcomponent.base.adapter.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;

public interface OnItemLongClickListener<E extends IItemData, H extends BaseViewHolder> {
    boolean onItemLongClick(RecyclerView.Adapter<H> adapter, View v, E data, int position);
}
