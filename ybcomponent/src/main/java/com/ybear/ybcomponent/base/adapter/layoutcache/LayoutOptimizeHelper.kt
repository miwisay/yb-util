package com.ybear.ybcomponent.base.adapter.layoutcache

import android.os.Build
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * @Author MiWi
 * @Date 2024年1月24日
 * @Description 布局优化-外部调用类
 */
class LayoutOptimizeHelper {
    constructor(rv: RecyclerView, layoutId: Int) : this( rv, layoutId, 11 )
    constructor(rv: RecyclerView, layoutId: Int, maxCount: Int) {
        mRecyclerView = rv
        mLayoutId = layoutId
        mMaxCount = maxCount
    }

    private var mRecyclerView: RecyclerView? = null
    private var mLayoutId: Int = 0
    private val mOptimize = LayoutInflaterOptimize()
    private var mMaxCount = 11

    init {
        mRecyclerView?.apply {
            setItemViewCacheSize(
                if( Build.VERSION.SDK_INT >= 33 ) 4 else 2
            )
            mOptimize.preload( this, mLayoutId, mMaxCount )
        }
    }

    fun getView(parent: ViewGroup, layoutId: Int): View? {
        mOptimize.getView( layoutId )?.apply {
            mOptimize.preload( parent, layoutId, mMaxCount )
            return this
        }
        return mOptimize.inflateView( parent, layoutId )
    }
}