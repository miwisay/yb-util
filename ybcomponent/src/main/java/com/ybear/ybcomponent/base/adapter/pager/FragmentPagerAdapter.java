package com.ybear.ybcomponent.base.adapter.pager;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;

import com.ybear.ybcomponent.base.adapter.OnViewPagerAdapterListener;

import java.util.List;

/**
 * Pager适配器
 *
 * 需要用到适配器的组件及方法
 * @see androidx.viewpager.widget.ViewPager
 * @see androidx.viewpager.widget.ViewPager#setAdapter(PagerAdapter)
 * 传入一个集合View，设置适配器后以实现多个View之间的滑动切换
 */
public class FragmentPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {
    @NonNull
    private FragmentManager mFM;
    private List<Fragment> mDataList;
    private OnViewPagerAdapterListener mOnViewPagerAdapterListener;

    public FragmentPagerAdapter(@NonNull FragmentManager fm, List<Fragment> list) {
        super(fm);
        mFM = fm;
        mDataList = list;
    }

    public void setOnViewPagerAdapterListener(OnViewPagerAdapterListener l) {
        mOnViewPagerAdapterListener = l;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mDataList.get( position );
    }

    /**
     * 页面数量
     * @return          页面数量
     */
    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return mDataList.get( position ).hashCode();
    }

    //    @Override
//    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
//    }
//
//    @Override
//    public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
//    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Object ret;
        if( mOnViewPagerAdapterListener != null ) {
            ret = mOnViewPagerAdapterListener.instantiateItem( container, position );
            if( ret != null ) return ret;
        }
        ret =  super.instantiateItem(container, position);
        mFM.beginTransaction().show( (Fragment) ret ).commitAllowingStateLoss();
        return ret;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        boolean ret = false;
        if( mOnViewPagerAdapterListener != null ) {
            ret = mOnViewPagerAdapterListener.destroyItem( container, position, object );
        }
        if( ret ) return;
        try {
            mFM.beginTransaction()
                    .hide( mDataList.get( position ) )
                    .commitAllowingStateLoss();
            return;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        super.destroyItem(container, position, object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
