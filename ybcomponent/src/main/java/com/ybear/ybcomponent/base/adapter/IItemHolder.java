package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.NonNull;

public interface IItemHolder<H extends BaseViewHolder> {
    void onHolderChange(@NonNull H holder, int position, @HolderStatus int holderStatus);
}
