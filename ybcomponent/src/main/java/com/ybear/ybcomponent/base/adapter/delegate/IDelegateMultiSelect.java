package com.ybear.ybcomponent.base.adapter.delegate;

import android.annotation.SuppressLint;
import android.view.View;

import androidx.annotation.IntRange;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.IItemDataDao;
import com.ybear.ybcomponent.base.adapter.IItemHolder;
import com.ybear.ybcomponent.base.adapter.ItemClickType;
import com.ybear.ybcomponent.base.adapter.listener.OnMultiSelectChangeListener;

import java.util.List;

public interface IDelegateMultiSelect<E extends IItemData, H extends BaseViewHolder>
        extends IItemDataDao<E>, IItemHolder<H> {
    @SuppressLint("ClickableViewAccessibility")
    void delegateMultiView(View v, int position);

    void delegateMultiViewClick(View v, int position);

    void setItemClickType(@ItemClickType int type);

    void setOnMultiSelectChangeListener(OnMultiSelectChangeListener<H> l);

    void setEnableMultiSelect(boolean enable);

    void setEnableMultiSelectAndInit(boolean enable);

    void setAutoSelectHideItem(boolean autoSelectHideItem);

    boolean isEnableMultiSelect();

    int getSelectedCount();

    boolean isSelectedAll();

    void setMaxMultiSelectCount(@IntRange(from = -1) int count);

    int getMaxMultiSelectCount();

    void setMinMultiSelectCount(@IntRange(from = 0) int count,
                                @IntRange(from = 0) int startPosition, boolean isFirst);

    void setMinMultiSelectCount(@IntRange(from = 0) int count, boolean isFirst);

    void setMinMultiSelectCount(@IntRange(from = 0) int count,
                                @IntRange(from = 0) int startPosition);

    void setMinMultiSelectCount(@IntRange(from = 0) int count);

    int getMinMultiSelectCount();

    boolean isFirstSelectOfMinCount();

    int getStartPositionOfMinCount();

    void setReplaceLastSelect(boolean select);

    boolean isReplaceLastSelect();

    void setMultiSelectStatus(int position, boolean enable);

    boolean setMultiSelectStatusOfFirst(int startPosition, boolean enable);

    boolean setMultiSelectStatusOfFirst(boolean enable);

    boolean setMultiSelectStatusOfLast(int endPosition, boolean enable);

    boolean setMultiSelectStatusOfLast(boolean enable);

    void setMultiSelectStatusAll(boolean enable);

    void switchMultiSelectStatus(int position);

    void switchMultiSelectStatusAll();

    boolean getMultiSelectStatus(int position);

    List<Boolean> getMultiSelectStatusAll();

    List<E> getMultiSelectDataList();

    boolean isExistSelect();

    void addMultiSelectStatus(int position, boolean enable);

    void addMultiSelectStatus(int position);

    boolean addMultiSelectStatus(boolean enable);

    boolean addMultiSelectStatus();

    boolean removeMultiSelectStatus(int position);

    boolean removeMultiSelectStatus(E data);

    void clearMultiSelectStatus();

    boolean addItemData(int position, boolean enable, E data);

    boolean addItemData(int position, boolean enable, List<E> data);

    boolean addItemData(boolean enable, E data);

    boolean addItemData(boolean enable, List<E> data);

    E setItemData(int position, boolean enable, E data);
}
