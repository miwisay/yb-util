package com.ybear.ybcomponent.base.adapter.pager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ybear.ybcomponent.TextHighlight;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.OnViewPagerAdapterListener;
import com.ybear.ybcomponent.highlight.ITextHighlight;

import java.util.ArrayList;
import java.util.List;

/**
 * 复用View Pager适配器
 *
 * 需要用到适配器的组件及方法
 * @see ViewPager
 * @see ViewPager#setAdapter(PagerAdapter)
 *
 * 传入一个集合View，设置适配器后以实现多个View之间的滑动切换
 * @param <E>
 */
public abstract class BaseRecyclerPagerAdapter
        <E extends IItemData, H extends BaseRecyclerPagerAdapter.PagerHolder>
        extends PagerAdapter
        implements ITextHighlight {
    private final List<E> mDataList;
    private final List<H> mRecyclerList = new ArrayList<>();
    private final List<H> mUseList = new ArrayList<>();

    private final TextHighlight mTextHighlight = new TextHighlight();

    private OnViewPagerAdapterListener mOnViewPagerAdapterListener;

    public BaseRecyclerPagerAdapter(List<E> list) {
        this.mDataList = list;
    }

    public E getItemData(int position) { return mDataList.get( position ); }

    public void setOnViewPagerAdapterListener(OnViewPagerAdapterListener l) {
        mOnViewPagerAdapterListener = l;
    }

    /**
     * 添加页面
     * @param container     {@link ViewGroup}
     * @param position      下标
     * @return              当前下标的View
     */
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        H h;
        int viewType = getItemViewType( position );

        if( mOnViewPagerAdapterListener != null ) {
            mOnViewPagerAdapterListener.instantiateItem( container, position );
        }

        if( mRecyclerList.isEmpty() ) {
            /* 创建Holder */
            h = onCreatePagerHolder( container, viewType );
        }else {
            /* 复用Holder */
            h = mRecyclerList.remove( 0 );
        }

        try {
            //添加到布局
            container.addView( h.getItemView() );
            h.setPosition( position );
            h.setViewType( viewType );
            mUseList.add( h );
            //回调Holder用于绑定
            onBindPagerHolder( h, position );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return h;
    }

    /**
     * 删除页面
     * @param container     {@link ViewGroup}
     * @param position      下标
     * @param object        {@link Object}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        H h = ((H) object);

        if( mOnViewPagerAdapterListener != null ) {
            mOnViewPagerAdapterListener.destroyItem( container, position, object );
        }

        mRecyclerList.add( h );
        container.removeView( h.getItemView() );
        //回调Holder解除绑定
        onDestroyPagerHolder( h, position );
        mUseList.remove( h );
    }

    /**
     * 页面数量
     * @return          页面数量
     */
    @Override
    public int getCount() { return mDataList.size(); }

//    @SuppressWarnings("unchecked")
//    @Override
//    public int getItemPosition(@NonNull Object object) {
//        H h = (H) object;
//        return getCount() == 0 || h.getPosition() >= getCount() ?
//                POSITION_NONE : POSITION_UNCHANGED;
//    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == ((H) o).getItemView();
    }

    public int getItemViewType(int position) { return 0; }

    @Nullable
    public H getUseHandler(int position) {
        for( H h : mUseList ) if( h != null && h.getPosition() == position ) return  h;
        return null;
    }

    public List<H> getUseHandlerList() { return new ArrayList<>( mUseList ); }

    public int getUseHandlerCount() { return mUseList.size(); }

    @NonNull
    public abstract H onCreatePagerHolder(@NonNull ViewGroup container, int viewType);
    public abstract void onBindPagerHolder(@NonNull H holder, int position);
    public void onDestroyPagerHolder(@NonNull H holder, int position) {}

    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color, int highlightCount) {
        mTextHighlight.setTextAndHighlight( tv, text, highlight, color, highlightCount );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight, int color) {
        mTextHighlight.setTextAndHighlight( tv, text, highlight, color );
    }

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlight(TextView tv, String text, String highlight) {
        mTextHighlight.setTextAndHighlight( tv, text, highlight );
    }

    /**
     * 设置高亮文本背景
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight,
                                                int color, int highlightCount) {
        mTextHighlight.setTextAndHighlightByBackground( tv, text, highlight, color, highlightCount );
    }

    /**
     * 设置高亮文本背景
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight, int color) {
        mTextHighlight.setTextAndHighlightByBackground( tv, text, highlight, color );
    }

    /**
     * 设置高亮文本背景
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlightByBackground(TextView tv, String text, String highlight) {
        mTextHighlight.setTextAndHighlightByBackground( tv, text, highlight );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight,
                                                 int foreColor, int backColor,
                                                 int highlightCount) {
        mTextHighlight.setTextAndHighlightByForeAndBack(
                tv, text, highlight, foreColor, backColor, highlightCount
        );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight,
                                                 int foreColor, int backColor) {
        mTextHighlight.setTextAndHighlightByForeAndBack( tv, text, highlight, foreColor, backColor );
    }

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    @Override
    public void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight) {
        mTextHighlight.setTextAndHighlightByForeAndBack( tv, text, highlight );
    }

    public static class PagerHolder {
        private View itemView;
        private int position;
        private int viewType;

        public PagerHolder(@NonNull ViewGroup container, @LayoutRes int layout) {
            this( LayoutInflater.from( container.getContext() )
                    .inflate( layout, container, false)
            );
        }

        public PagerHolder(View itemView) { this.itemView = itemView; }

        @NonNull
        @Override
        public String toString() {
            return "PagerHolder{" +
                    "itemView=" + itemView +
                    ", position=" + position +
                    ", viewType=" + viewType +
                    '}';
        }

        void setItemView(View itemView) { this.itemView = itemView; }
        void setPosition(int position) { this.position = position; }
        void setViewType(int viewType) { this.viewType = viewType; }

        public View getItemView() { return itemView; }
        public int getPosition() { return position; }
        public int getViewType() { return viewType; }
    }
}
