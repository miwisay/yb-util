package com.ybear.ybcomponent.base.adapter;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({ HolderStatus.ATTACHED, HolderStatus.RECYCLED, HolderStatus.DETACHED })
@Retention(RetentionPolicy.SOURCE)
public @interface HolderStatus {
    int ATTACHED = 1;
    int RECYCLED = 0;
    int DETACHED = -1;
}