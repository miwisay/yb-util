package com.ybear.ybcomponent.base.adapter.listener;

import androidx.annotation.NonNull;

import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.widget.ItemSwipeLayout;

public interface OnCreateItemSwipeLayoutListener<H extends BaseViewHolder> {
    @NonNull
    ItemSwipeLayout onCreateItemSwipeLayout(@NonNull H viewHolder, int position);
}
