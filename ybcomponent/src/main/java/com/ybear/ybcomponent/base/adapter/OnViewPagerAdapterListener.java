package com.ybear.ybcomponent.base.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface OnViewPagerAdapterListener {

    @Nullable
    Object instantiateItem(@NonNull ViewGroup container, int position);
    boolean destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object);
}
