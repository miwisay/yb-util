package com.ybear.ybcomponent.base.adapter.delegate;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.IntDef;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ybear.ybcomponent.base.adapter.BaseRecyclerViewAdapter;
import com.ybear.ybcomponent.base.adapter.BaseViewHolder;
import com.ybear.ybcomponent.base.adapter.HolderStatus;
import com.ybear.ybcomponent.base.adapter.IItemData;
import com.ybear.ybcomponent.base.adapter.ItemClickType;
import com.ybear.ybcomponent.base.adapter.listener.IMultiSelectListener;
import com.ybear.ybcomponent.base.adapter.listener.OnMultiSelectChangeListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class DelegateMultiSelect<E extends IItemData, H extends BaseViewHolder>
        extends BaseDelegateAdapter<E, H>
        implements IDelegateMultiSelect<E, H> {

    /**
     * 上下限到达状态
     */
    @IntDef({ ReachStatus.NOT_SET, ReachStatus.NOT_REACH, ReachStatus.REACH })
    @Retention(RetentionPolicy.SOURCE)
    private @interface ReachStatus {
        int NOT_SET = -1;
        int NOT_REACH = 0;
        int REACH = 1;
    }

    private List<Boolean> mStatusList = new ArrayList<>();
    private boolean isEnableMultiSelect = true;
    private boolean isSelectedAll = false;
    private boolean isReplaceLastSelect = true;     //是否替换上一个选项
    private boolean isFirstSelectOfMinCount = true; //多选下限的起始位置是否从顶部开始，否则从底部开始
    private boolean isAutoSelectHideItem = false;   //自动勾选隐藏的Item
    private int mStartPositionOfMinCount = 0;       //多选下限的起始位置，底部为 size() - startPosition
    private int mLastSelectPosition = -1;           //上一个选中的位置
    private int mMaxMultiSelectCount = -1;          //多选上限。-1：没有限制
    private int mMinMultiSelectCount = 0;           //多选下限。 0：没有限制
    private int mSelectedCount = 0;
    private int mItemClickType = ItemClickType.SWITCH;

    private OnMultiSelectChangeListener<H> mOnMultiChangeListener;
    private IMultiSelectListener<H> mIMultiListener;

    public static <E extends IItemData, H extends BaseViewHolder> DelegateMultiSelect<E, H> create() {
        return new DelegateMultiSelect<>();
    }

    public BaseDelegateAdapter<E, H> init(IMultiSelectListener<H> l) {
        mIMultiListener = l;
        return this;
    }

    /**
     * Item被点击
     * @param position  选中的位置
     */
    public void onItemClick(int position) {
        if( !isEnableMultiSelect ) return;
        switch( mItemClickType ) {
            case ItemClickType.SWITCH:
                switchMultiSelectStatus( position );
                break;
            case ItemClickType.ENABLE:
                setMultiSelectStatus( position, true );
                break;
            case ItemClickType.DISABLE:
                setMultiSelectStatus( position, false );
                break;
        }
    }

    /**
     * 绑定ViewHolder
     * @param h             Holder
     * @param position      当前Item的位置
     */
    @Override
    public void onBindViewHolder(@NonNull H h, int position) {
        //检查上限和下限是否合法
        checkSizeOfMaxAndMin();
        //是否自动勾选隐藏的Item
        if( isAutoSelectHideItem && !h.isShow() ) {
            setMultiSelectStatus( position, true, false, false );
        }

        if( mIMultiListener != null ) {
            mIMultiListener.onBindViewHolder( h, position, getMultiSelectStatus( position ) );
        }
    }

    @Override
    public void onHolderChange(@NonNull H h, int position, @HolderStatus int holderStatus) {
        if( holderStatus != HolderStatus.ATTACHED ) return;
        if( !isEnableMultiSelect || getAdapter().getItemCount() <= 0 ) return;
        //填充多选状态
        fillMultiStatus();
        //更新最小下限
        updateMultiStatusReachMin();
        //通知接口
        notifyChange( h, position, getMultiSelectStatus( position ), false );
    }

    /**
     * 委托传入的View切换选中状态 - touch
     * 当View的Touch被抬起时触发
     * @param v             委托的View
     * @param position      选中的位置
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void delegateMultiView(View v, int position) {
        if( v == null ) return;
        //CheckBox，Switch，RadioButton等控件的委托
        if( v instanceof CompoundButton ) {
            ((CompoundButton) v).setOnCheckedChangeListener((buttonView, isChecked) ->
                    setMultiSelectStatus( position, isChecked ));
            return;
        }
        //普通View点击的委托
        v.setOnTouchListener((v1, ev) -> {
            if( ev.getAction() == MotionEvent.ACTION_UP ) switchMultiSelectStatus( position );
            return false;
        });
    }

    /**
     * 委托传入的View切换选中状态 - click
     * @param v             委托的View
     * @param position      选中的位置
     */
    @Override
    public void delegateMultiViewClick(View v, int position) {
        v.setOnClickListener(v1 -> switchMultiSelectStatus( position ));
    }

    /**
     * 设置Item被点击时选项处理的方式
     * @param type  反选：      {@link ItemClickType#SWITCH}
     *              勾选：      {@link ItemClickType#ENABLE}
     *              取消勾选：   {@link ItemClickType#DISABLE}
     */
    @Override
    public void setItemClickType(@ItemClickType int type) {
        mItemClickType = type;
    }

    /**
     * 设置多选发生改变时事件监听器
     * @param l     监听器
     */
    @Override
    public void setOnMultiSelectChangeListener(OnMultiSelectChangeListener<H> l) {
        this.mOnMultiChangeListener = l;
    }

    /**
     * 是否启用多选
     * @param enable    启用多选
     */
    @Override
    public void setEnableMultiSelect(boolean enable) {
        isEnableMultiSelect = enable;
    }

    /**
     * 是否启用多选并且初始化
     * @param enable    启用多选
     */
    @Override
    public void setEnableMultiSelectAndInit(boolean enable) {
        setEnableMultiSelect( enable );
        if( enable ) return;
        //清空选中的值
        for (int i = 0; i < mStatusList.size(); i++)
            try {
                mStatusList.set( i, false );
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    /**
     * 设置自动勾选隐藏的Item
     * @param autoSelectHideItem    是否自动勾选
     */
    @Override
    public void setAutoSelectHideItem(boolean autoSelectHideItem) {
        isAutoSelectHideItem = autoSelectHideItem;
    }

    /**
     * 启用多选的状态
     * @return  是否已启用
     */
    @Override
    public boolean isEnableMultiSelect() { return isEnableMultiSelect; }

    /**
     * 获取当前选中的数量
     * @return  数量
     */
    @Override
    public int getSelectedCount() { return mSelectedCount; }

    /**
     * 是否为全选状态
     * @return  结果
     */
    @Override
    public boolean isSelectedAll() { return isSelectedAll; }

    /**
     * 设置最大选中数量
     * @param count     数量。-1：没有限制
     */
    @Override
    public void setMaxMultiSelectCount(@IntRange(from = -1) int count) {
        mMaxMultiSelectCount = count;
    }

    /**
     * 获取最大选中数量
     * @return  数量
     */
    @Override
    public int getMaxMultiSelectCount() {
        int count = getAdapter().getItemCount();
        return Math.min( mMaxMultiSelectCount, count );
    }

    /**
     * 设置最小选中数量
     * @param count             数量。0：没有限制
     * @param startPosition     起始位置。
     *                          当<code>isFirst</code>为false时，实际位置：size() - startPosition
     * @param isFirst           是否从上往下开始
     */
    @Override
    public void setMinMultiSelectCount(@IntRange(from = 0) int count,
                                       @IntRange(from = 0) int startPosition, boolean isFirst) {
        mMinMultiSelectCount = count;
        mStartPositionOfMinCount = startPosition;
        isFirstSelectOfMinCount = isFirst;
    }

    /**
     * 设置最小选中数量
     * @param count             数量。0：没有限制
     * @param isFirst           是否从上往下开始
     */
    @Override
    public void setMinMultiSelectCount(@IntRange(from = 0) int count, boolean isFirst) {
        setMinMultiSelectCount( count, mStartPositionOfMinCount, isFirst );
    }

    /**
     * 设置最小选中数量
     * @param count             数量。0：没有限制
     * @param startPosition     起始位置。
     *                          当<code>isFirst</code>为false时，实际位置：size() - startPosition
     */
    @Override
    public void setMinMultiSelectCount(@IntRange(from = 0) int count,
                                       @IntRange(from = 0) int startPosition) {
        setMinMultiSelectCount( count, startPosition, true );
    }

    /**
     * 设置最小选中数量
     * @param count             数量。0：没有限制
     */
    @Override
    public void setMinMultiSelectCount(@IntRange(from = 0) int count) {
        setMinMultiSelectCount( count, true );
    }

    /**
     * 获取最小选中数量
     * @return  数量
     */
    @Override
    public int getMinMultiSelectCount() { return mMinMultiSelectCount; }

    /**
     * 多选下限的起始位置是否从顶部开始，否则从底部开始
     * @return  是否为顶部
     */
    @Override
    public boolean isFirstSelectOfMinCount() { return isFirstSelectOfMinCount; }

    /**
     * 获取最小数量的起始位置
     * @return  位置
     */
    @Override
    public int getStartPositionOfMinCount() { return mStartPositionOfMinCount; }

    /**
     * 设置替换上一个多选状态
     * 启用后，当到达{@link DelegateMultiSelect#getMaxMultiSelectCount()}上限时会取消上一个选中的状态
     * @param select    是否设置
     */
    @Override
    public void setReplaceLastSelect(boolean select) { isReplaceLastSelect = select; }

    /**
     * 是否替换上一个选中的状态
     * @return  结果
     */
    @Override
    public boolean isReplaceLastSelect() { return isReplaceLastSelect; }

    /**
     * 选中/反选位置的多选状态
     * @param position  位置
     * @param enable    多选状态
     */
    @Override
    public void setMultiSelectStatus(int position, boolean enable) {
        setMultiSelectStatus( position, enable, true, true );
    }

    /**
     * 从上往下选中/反选多选状态
     * @param startPosition     起始位置
     * @param enable            多选状态
     */
    @Override
    public boolean setMultiSelectStatusOfFirst(int startPosition, boolean enable) {
        return setMultiSelectStatusOfFirst( startPosition, enable, true );
    }

    /**
     * 从上往下选中/反选多选状态
     * @param enable    多选状态
     */
    @Override
    public boolean setMultiSelectStatusOfFirst(boolean enable) {
        return setMultiSelectStatusOfFirst( 0, enable );
    }

    /**
     * 从下往上选中/反选多选状态
     * @param endPosition       起始位置。实际位置：size() - endPosition
     * @param enable            多选状态
     */
    @Override
    public boolean setMultiSelectStatusOfLast(int endPosition, boolean enable) {
        return setMultiSelectStatusOfLast( endPosition, enable, true );
    }

    /**
     * 从下往上选中/反选多选状态
     * @param enable    多选状态
     */
    @Override
    public boolean setMultiSelectStatusOfLast(boolean enable) {
        return setMultiSelectStatusOfLast( 0, enable );
    }

    /**
     * 选中/反选位置的多选状态
     * @param enable    多选状态
     */
    @Override
    public void setMultiSelectStatusAll(boolean enable) {
        //全选时从顶部开始，反选时从底部开始。
        int i = enable ? 0 : mStatusList.size() - 1;
        for( ; enable ? i < mStatusList.size() : i >= 0; i = enable ? i + 1 : i - 1 ) {
            if( enable && isMultiSelectReachMax() == ReachStatus.REACH ) break;
            setMultiSelectStatus( i, enable, false, true );
        }
    }

    /**
     * 选中/反选位置的多选状态
     * @param position  位置
     */
    @Override
    public void switchMultiSelectStatus(int position) {
        setMultiSelectStatus( position, !getMultiSelectStatus( position ) );
    }

    /**
     * 选中/反选所有多选状态
     */
    @Override
    public void switchMultiSelectStatusAll() { setMultiSelectStatusAll( !isSelectedAll ); }

    /**
     * 获取位置的多选状态
     * @param position  位置
     * @return          多选状态
     */
    @Override
    public boolean getMultiSelectStatus(int position) {
        return getAdapter().checkPosition( position ) ? mStatusList.get( position ) : false;
    }

    /**
     * 获取所有数据的多选状态
     * @return  列表（new 主要是防止调用者直接修改）
     */
    @Override
    public List<Boolean> getMultiSelectStatusAll() { return new ArrayList<>( mStatusList ); }

    /**
     * 获取选中的数据列表
     * @return  数据源
     */
    @Override
    public List<E> getMultiSelectDataList() {
        List<E> dataList = new ArrayList<>();
        for (int i = 0; i < mStatusList.size(); i++) {
            if( getMultiSelectStatus( i ) ) dataList.add( getAdapter().getDataList().get( i ) );
        }
        return dataList;
    }

    /**
     * 是否存在选中
     * @return  结果
     */
    @Override
    public boolean isExistSelect() {
        for (int i = 0; i < mStatusList.size(); i++) {
            if( getMultiSelectStatus( i ) ) return true;
        }
        return false;
    }

    /**
     * 指定位置增加一个多选状态
     * @param position  位置
     * @param enable    多选状态
     */
    @Override
    public void addMultiSelectStatus(int position, boolean enable) {
        addMultiStatus( position, getAdapter().getItemCount(), enable, false );
    }

    /**
     * 指定位置增加一个多选状态
     * @param position  位置
     */
    @Override
    public void addMultiSelectStatus(int position) {
        addMultiStatus( position, getAdapter().getItemCount(), false, false );
    }

    /**
     * 增加一个多选状态
     * @param enable    多选状态
     */
    @Override
    public boolean addMultiSelectStatus(boolean enable) { return mStatusList.add( enable ); }

    /**
     * 增加一个多选状态
     * @return  结果
     */
    @Override
    public boolean addMultiSelectStatus() { return addMultiSelectStatus( false ); }

    /**
     * 移除位置的多选状态
     * @param position  移除的位置
     */
    @Override
    public boolean removeMultiSelectStatus(int position) {
        return mStatusList.remove( position );
    }

    /**
     * 移除位置的多选状态
     * @param data      移除的数据
     */
    @Override
    public boolean removeMultiSelectStatus(E data) {
        return mStatusList.remove( getAdapter().getItemDataPosition( data ) );
    }

    /**
     * 清空所有所有多选状态
     */
    @Override
    public void clearMultiSelectStatus() { mStatusList.clear(); }

    @Nullable
    @Override
    public E getItemData(int position) { return getAdapter().getItemData( position ); }

    @Nullable
    @Override
    public E getItemDataOfFirst() { return getAdapter().getItemDataOfFirst(); }

    @Nullable
    @Override
    public E getItemDataOfLast() { return getAdapter().getItemDataOfLast(); }

    @NonNull
    @Override
    public List<E> getDataList() { return getAdapter().getDataList(); }

    /**
     * 增加一条数据
     * @param position  位置
     * @param enable    多选状态
     * @param data      数据源
     */
    @Override
    public boolean addItemData(int position, boolean enable, E data) {
        addMultiSelectStatus( position, enable );
        return true;
    }

    /**
     * 增加一条数据
     * @param position  位置
     * @param enable    多选状态
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(int position, boolean enable, List<E> data) {
        if( data == null || data.size() == 0 ) return false;
        for (int i = 0; i < data.size(); i++) {
            addMultiSelectStatus( position, enable );
        }
        return true;
    }

    /**
     * 增加一条数据
     * @param enable    多选状态
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(boolean enable, E data) {
        addMultiSelectStatus( enable );
        return true;
    }

    /**
     * 增加一条数据
     * @param enable    多选状态
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(boolean enable, List<E> data) {
        if( data == null || data.size() == 0 ) return false;
        for (int i = 0; i < data.size(); i++) addMultiSelectStatus( enable );
        return true;
    }

    /**
     * 增加一条数据
     * @param position  添加的位置
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(int position, E data) {
        return addItemData( position, false, data );
    }

    /**
     * 增加一条数据
     * @param position  添加的位置
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(int position, List<E> data) {
        return addItemData( position, false, data );
    }

    /**
     * 增加一条数据
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(E data) { return addItemData( false, data ); }

    /**
     * 增加一条数据
     * @param data      数据源
     * @return          结果
     */
    @Override
    public boolean addItemData(List<E> data) { return addItemData( false, data ); }

    @Override
    public void setItemData(List<E> data) {
        clearMultiSelectStatus();
    }

    /**
     * 设置一个位置的数据以及多选状态
     * @param position  位置
     * @param enable    多选状态
     * @param data      数据源
     * @return          之前的数据
     */
    @Override
    public E setItemData(int position, boolean enable, E data) {
        setMultiSelectStatus( position, enable );
        return data;
    }

    /**
     * 设置一个位置的数据以及多选状态
     * @param position  位置
     * @param data      数据源
     * @return          之前的数据
     */
    @Override
    public E setItemData(int position, E data) {
        return setItemData( position, false, data );
    }

    /**
     * 移除一个位置的数据源
     * @param position  位置
     * @return          结果
     */
    @Override
    public E removeItemData(int position) {
        removeMultiSelectStatus( position );
        return null;
    }

    /**
     * 移除一个位置的数据源
     * @param data      数据
     * @return          结果
     */
    @Override
    public E removeItemData(E data) {
        removeMultiSelectStatus( data );
        return data;
    }

    /**
     * 清空数据源
     */
    @Override
    public void clearItemData() { clearMultiSelectStatus(); }

    /**
     * 选中/反选位置的多选状态
     * @param position              选中的位置
     * @param enable                多选的状态
     * @param isCancelLastSelect    是否允许取消上一个选择
     */
    private void setMultiSelectStatus(int position, boolean enable, boolean isCancelLastSelect,
                                      boolean isNotify) {
        if( !isEnableMultiSelect || !getAdapter().checkPosition( position ) ) return;

        //相同的状态只通知，不改变数据源
        if( isNotify && getMultiSelectStatus( position ) == enable ) {
            notifyChange( position, enable, true );
            return;
        }
        //限制多选数量。-1：跳过。其他：上个记录的位置
        if( enable ) {
            int isReachMax = isMultiSelectReachMax();
            if( isReachMax != ReachStatus.NOT_SET ) {
                boolean isMax = isReachMax == ReachStatus.REACH;
                //是否允许替换上一个选项
                if( isReplaceLastSelect ) {
                    //取消上一个选项
                    if( isCancelLastSelect && mLastSelectPosition != -1 && isMax ) {
                        changeMulti( mLastSelectPosition, false, false, isNotify );
                    }
                    mLastSelectPosition = position;
                }else if( isMax ) {
                    /* 满足最大限制 */
                    notifyChange( position, true, true );
                    return;
                }
            }
        }else if( isMultiSelectReachMin( 1 ) == ReachStatus.NOT_REACH ) {
            /* 满足最小限制 */
            notifyChange( position, true, true );
            return;
        }
        //更新多选状态
        changeMulti( position, enable, true, isNotify );
    }
    private void changeMulti(int position, boolean enable, boolean fromUser, boolean isNotify) {
        int maxCount = getMaxMultiSelectCount();
        if( enable ) {
            //递减状态，标记全选状态
            if( ++mSelectedCount == mStatusList.size() ||
                    isMultiSelectReachMax() == ReachStatus.REACH ) {
                isSelectedAll = true;
            }
        }else if( mSelectedCount > 0 ) {
            //递减状态，取消全选状态
            --mSelectedCount;
            isSelectedAll = false;
        }
        //向下滑动时，没有选中的item才会勾选，此时虽然为全选状态，但count并不是全选数量，所以需要count为全选状态
        if( isSelectedAll ) mSelectedCount = maxCount == -1 ? getAdapter().getItemCount() : maxCount;

        if( getAdapter().checkPosition( position ) ) {
            //更新值
            try {
                mStatusList.set( position, enable );
            } catch (Exception e) {
                e.printStackTrace();
            }
            //多选状态改变事件监听器
            if( isNotify ) notifyChange( position, enable, fromUser );
        }
    }

    /**
     * 从上往下选中/反选多选状态
     * @param startPosition     选中数量
     * @param enable            多选状态
     * @param isNotify          是否通知监听器
     */
    private boolean setMultiSelectStatusOfFirst(int startPosition,
                                                boolean enable, boolean isNotify) {
        if( isMultiSelectReachMin() != ReachStatus.NOT_REACH ) return false;
        if( !getAdapter().checkPosition( startPosition ) ) return false;
        setMultiSelectStatus( startPosition, enable, true, isNotify );
        setMultiSelectStatusOfFirst( ++startPosition, enable, isNotify );
        return true;
    }

    /**
     * 从下往上选中/反选多选状态
     * @param endPosition       选中数量
     * @param enable            多选状态
     * @param isNotify          是否通知监听器
     */
    private boolean setMultiSelectStatusOfLast(int endPosition, boolean enable, boolean isNotify) {
        if( isMultiSelectReachMin() != ReachStatus.NOT_REACH ) return false;
        if( !getAdapter().checkPosition( endPosition ) ) return false;
        setMultiSelectStatus( endPosition, enable, true, isNotify );
        setMultiSelectStatusOfLast( --endPosition, enable, isNotify );
        return true;
    }

    /**
     * 填充多选状态
     */
    private synchronized void fillMultiStatus() {
        int count = getAdapter().getItemCount();
        //状态列表为空，或者为0并且数据列表至少存在10条数据时重新初始化状态列表
        if( mStatusList == null || ( mStatusList.size() == 0 && count >= 10 ) ) {
            mStatusList = new ArrayList<>();
        }
        if( mStatusList.size() >= count ) return;
        addMultiStatus(
                0,
                count,
                mIMultiListener != null &&
                        mIMultiListener.onInitMultiSelect( mStatusList.size() ),
                false
        );
    }

    /**
     * 添加多选状态
     * @param startPosition     起始下标。如果超过 {@link DelegateMultiSelect#mStatusList} 大小时，则自动填充
     *                          eg: pos=5, size=0, defEnable=true, result:[ true, true, true, true, true ]
     * @param count             最大填充数量
     * @param enable            是否启用
     * @param defEnable         自动填充时是否启用
     */
    private void addMultiStatus(int startPosition, int count, boolean enable, boolean defEnable) {
        try {
            if( startPosition < 0 ) return;
            int statusSize = mStatusList.size();
            boolean isFill = statusSize == count;
            boolean isNext = true;
            if( startPosition == 0 || !isFill && startPosition < statusSize ) {
                mStatusList.add( startPosition, enable );
                statusSize += 1;
                isNext = statusSize < count;
            }else if( startPosition > statusSize || !isFill ) {
                enable = isFill ? defEnable : enable;
                mStatusList.add( enable );
            }else {
                return;
            }
            if( enable ) mSelectedCount++;
            if( isNext ) addMultiStatus( startPosition, count, enable, defEnable );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新最小下限
     */
    private void updateMultiStatusReachMin() {
        if( isMultiSelectReachMin() != ReachStatus.NOT_REACH ) return;
        if( isFirstSelectOfMinCount() ) {
            //从上往下
            setMultiSelectStatusOfFirst( mStartPositionOfMinCount, true, false );
        }else {
            //从下往上
            setMultiSelectStatusOfLast(
                    mStatusList.size() - mStartPositionOfMinCount - 1,
                    true, false
            );
        }
    }

    /**
     * 多选状态改变事件监听器
     * @param h             当前位置的Holder
     * @param position      位置
     * @param isCheck       多选状态
     * @param fromUser      是否为用户点击
     */
    private void notifyChange(H h, int position, boolean isCheck, boolean fromUser) {
        BaseRecyclerViewAdapter<E, H> adapter = getAdapter();
        if( mIMultiListener != null ) {
            //通知选项状态发生改变
            mIMultiListener.onMultiSelectChange( adapter, h, position, isCheck, fromUser );
            //通知全选状态
            mIMultiListener.onMultiSelectAll( adapter, isSelectedAll, fromUser );
        }
        if( mOnMultiChangeListener != null ) {
            //通知选项状态发生改变
            mOnMultiChangeListener.onMultiSelectChange( adapter, h, position, isCheck, fromUser );
            //通知全选状态
            mOnMultiChangeListener.onMultiSelectAll( adapter, isSelectedAll, fromUser );
        }
    }

    /**
     * 多选状态改变事件监听器
     * @param position      位置
     * @param status        多选状态
     * @param fromUser      是否为用户点击
     */
    private void notifyChange(int position, boolean status, boolean fromUser) {
        notifyChange( getAdapter().getHolder( position ), position, status, fromUser );
    }

    /**
     * 是否到达最大上限
     * @return          结果。-1：未设置最大上限，0：未超过，1：超过
     */
    @ReachStatus
    private int isMultiSelectReachMax() {
        if( mMaxMultiSelectCount < 0 ) return ReachStatus.NOT_SET;
        return mSelectedCount >= mMaxMultiSelectCount ? ReachStatus.REACH : ReachStatus.NOT_REACH;
    }

    /**
     * 是否到达最低下限
     * @param step      步进。假设减少step个selectedCount后是否满足最低下限
     * @return          结果。-1：未设置最大上限，0：未超过，1：超过
     */
    @ReachStatus
    private int isMultiSelectReachMin(int step) {
        if( mMinMultiSelectCount <= 0 ) return ReachStatus.NOT_SET;
        return getSelectedCount() - step >= mMinMultiSelectCount ?
                ReachStatus.REACH : ReachStatus.NOT_REACH;
    }

    /**
     * 是否到达最低下限
     * @return          结果
     */
    private int isMultiSelectReachMin() {
        return isMultiSelectReachMin( 0 );
    }

    /**
     * 检查上限和下限是否合法
     */
    private void checkSizeOfMaxAndMin() {
        int max = getMaxMultiSelectCount();
        if( max == -1 || max >= getMinMultiSelectCount() ) return;
        mMinMultiSelectCount = max;
    }
}
