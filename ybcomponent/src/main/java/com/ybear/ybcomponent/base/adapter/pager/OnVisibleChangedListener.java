package com.ybear.ybcomponent.base.adapter.pager;

import android.content.Context;

import androidx.annotation.NonNull;

public interface OnVisibleChangedListener {
    /**
     * Fragment被展示时
     * @param context       上下文
     * @param position      展示下标的Fragment
     */
    void onFragmentShow(@NonNull Context context, int position);

    /**
     * Fragment被隐藏时
     * @param context       上下文
     * @param position      隐藏下标的Fragment
     */
    void onFragmentHidden(@NonNull Context context, int position);
}