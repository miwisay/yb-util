package com.ybear.ybcomponent;

import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;

import java.util.Arrays;

public class ItemStyleBuild {
    private int iconWidth = 45;
    private int iconHeight = 45;
    private int textSizeUnit = TypedValue.COMPLEX_UNIT_SP;
    private float textSize = 14;
    @ColorRes
    private int textColor = R.color.color75;
    @ColorRes
    private int textSelectColor = R.color.color75;
    private int backgroundResource = 0;
    private Typeface typeface = null;
    private int gravity = Gravity.CENTER;
    private int[] padding = null;
    private int iconPadding = 0;
    private boolean textIncludeFontPadding = true;

    public ItemStyleBuild() { }

    public void init(ItemStyleBuild build) {
        if( build == null ) return;
        this.iconWidth = build.iconWidth;
        this.iconHeight = build.iconHeight;
        this.textSizeUnit = build.textSizeUnit;
        this.textSize = build.textSize;
        this.textColor = build.textColor;
        this.textSelectColor = build.textSelectColor;
        this.backgroundResource = build.backgroundResource;
        this.typeface = build.typeface;
        this.gravity = build.gravity;
        this.padding = build.padding;
        this.iconPadding = build.iconPadding;
        this.textIncludeFontPadding = build.textIncludeFontPadding;
    }

    @NonNull
    @Override
    public String toString() {
        return "ItemStyleBuild{" +
                "iconWidth=" + iconWidth +
                ", iconHeight=" + iconHeight +
                ", textSizeUnit=" + textSizeUnit +
                ", textSize=" + textSize +
                ", textColor=" + textColor +
                ", textSelectColor=" + textSelectColor +
                ", backgroundResource=" + backgroundResource +
                ", typeface=" + typeface +
                ", gravity=" + gravity +
                ", padding=" + Arrays.toString(padding) +
                ", iconPadding=" + iconPadding +
                ", textIncludeFontPadding=" + textIncludeFontPadding +
                '}';
    }

    public ItemStyleBuild setIconSize(int size) {
        iconWidth = size;
        iconHeight = size;
        return this;
    }

    public int getIconWidth() {
        return iconWidth;
    }

    public ItemStyleBuild setIconWidth(int iconWidth) {
        this.iconWidth = iconWidth;
        return this;
    }

    public int getIconHeight() {
        return iconHeight;
    }

    public ItemStyleBuild setIconHeight(int iconHeight) {
        this.iconHeight = iconHeight;
        return this;
    }

    public ItemStyleBuild setPadding(int[] padding) {
        this.padding = padding;
        return this;
    }

    public int getTextSizeUnit() {
        return textSizeUnit;
    }

    public ItemStyleBuild setTextSizeUnit(int textSizeUnit) {
        this.textSizeUnit = textSizeUnit;
        return this;
    }

    public float getTextSize() {
        return textSize;
    }

    public ItemStyleBuild setTextSize(float textSize) {
        this.textSize = textSize;
        return this;
    }

    @ColorRes
    public int getTextColor() { return textColor; }

    public ItemStyleBuild setTextColor(@ColorRes int textColor) {
        this.textColor = textColor;
        return this;
    }

    @ColorRes
    public int getTextSelectColor() { return textSelectColor; }

    public ItemStyleBuild setTextSelectColor(@ColorRes int textSelectColor) {
        this.textSelectColor = textSelectColor;
        return this;
    }

    public int getBackgroundResource() {
        return backgroundResource;
    }

    public ItemStyleBuild setBackgroundResource(int backgroundResource) {
        this.backgroundResource = backgroundResource;
        return this;
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public ItemStyleBuild setTypeface(Typeface typeface) {
        this.typeface = typeface;
        return this;
    }

    public int getGravity() {
        return gravity;
    }

    public ItemStyleBuild setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public int[] getPadding() {
        return padding;
    }

    public ItemStyleBuild setPadding(int start, int top, int end, int bottom) {
        this.padding = new int[] { start, top, end, bottom };
        return this;
    }

    public int getIconPadding() {
        return iconPadding;
    }

    public ItemStyleBuild setIconPadding(int iconPadding) {
        this.iconPadding = iconPadding;
        return this;
    }

    public boolean isTextIncludeFontPadding() {
        return textIncludeFontPadding;
    }

    public ItemStyleBuild setTextIncludeFontPadding(boolean textIncludeFontPadding) {
        this.textIncludeFontPadding = textIncludeFontPadding;
        return this;
    }
}
