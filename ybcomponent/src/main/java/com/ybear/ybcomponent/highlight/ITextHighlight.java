package com.ybear.ybcomponent.highlight;

import android.widget.TextView;

public interface ITextHighlight {
    /**
     * 设置高亮文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    void setTextAndHighlight(TextView tv, String text, String highlight, int color,
                             int highlightCount);

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    void setTextAndHighlight(TextView tv, String text, String highlight, int color);

    /**
     * 设置高亮文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    void setTextAndHighlight(TextView tv, String text, String highlight);

    /**
     * 设置高亮背景文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param color             高亮文本色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    void setTextAndHighlightByBackground(TextView tv, String text, String highlight,
                                         int color, int highlightCount);

    /**
     * 设置高亮背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param color         高亮文本色
     */
    void setTextAndHighlightByBackground(TextView tv, String text, String highlight, int color);

    /**
     * 设置高亮背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    void setTextAndHighlightByBackground(TextView tv, String text, String highlight);

    /**
     * 设置高亮文本和背景文本
     * @param tv                设置高亮文本的TextView
     * @param text              文本内容
     * @param highlight         高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     * @param highlightCount    重复文本的高亮次数。-1：无限制
     */
    void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight,
                                          int foreColor, int backColor, int highlightCount);

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     * @param foreColor         高亮文本色
     * @param backColor         高亮背景色
     */
    void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight,
                                          int foreColor, int backColor);

    /**
     * 设置高亮文本和背景文本
     * @param tv            设置高亮文本的TextView
     * @param text          文本内容
     * @param highlight     高亮文本
     */
    void setTextAndHighlightByForeAndBack(TextView tv, String text, String highlight);
}
